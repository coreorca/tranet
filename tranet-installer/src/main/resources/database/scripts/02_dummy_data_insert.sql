USE `tranet`;

INSERT INTO `car_type` VALUES
  (1, '2014-10-25 20:37:47', 'ACTIVE', '2014-10-25 20:37:47', 1, 'Shelby Super Cars', 'CAR1', NULL, 2007,
   'SSC Ultimate Aero', 2, 'SEDAN', 2),
  (2, '2014-10-25 20:41:16', 'ACTIVE', '2014-10-25 20:41:16', 0, 'Bugatti Automobiles SAS', 'CAR2', NULL, 2005,
   'Bugatti Veyron', 2, 'HATCHBACK', 1),
  (3, '2014-10-25 20:41:51', 'ACTIVE', '2014-10-25 20:41:51', 0, 'Koenigsegg', 'CAR3', NULL, 1996, 'Koenigsegg CCX', 4,
   'SEDAN', 2), (4, '2014-10-25 20:42:18', 'ACTIVE', '2014-10-25 20:42:18', 1,
                 'Hidden Creek Industries, Phil Frank Design, and Ray Mallock Ltd.', 'CAR4', NULL, 2000,
                 'Saleen S7 Twin-Turbo', 20, 'MIDI_BUS', 30),
  (5, '2014-10-25 20:42:56', 'ACTIVE', '2014-10-25 20:42:56', 0, 'Ferrari', 'CAR5', NULL, 2003, 'Ferrari Enzo', 2,
   'STATION_WAGON', 2);

INSERT INTO `country` VALUES (1, '2014-10-25 20:49:44', 'ACTIVE', '2014-10-25 20:49:44', 0, 'TUR', 1),
  (2, '2014-10-25 20:53:46', 'ACTIVE', '2014-10-25 20:53:46', 0, 'USA', 2);

INSERT INTO `city` VALUES (1, '2014-10-25 20:54:02', 'ACTIVE', '2014-10-25 20:54:02', 0, 'AYT', 'Antalya', 1),
  (2, '2014-10-25 20:54:12', 'ACTIVE', '2014-10-25 20:54:12', 0, 'IST', 'Istanbul', 1);

INSERT INTO `currency_type`
VALUES (1, '2014-10-25 20:45:19', 'ACTIVE', '2014-10-25 20:45:19', 0, 'TRY', 'Turkish Lira'),
  (2, '2014-10-25 20:46:13', 'ACTIVE', '2014-10-25 20:46:13', 0, 'USD', 'United States Dollar'),
  (3, '2014-10-25 20:46:54', 'ACTIVE', '2014-10-25 20:46:54', 0, 'EUR', 'EURO'),
  (4, '2014-10-25 20:47:14', 'ACTIVE', '2014-10-25 20:47:14', 0, 'RUB', 'Russian Ruble');

INSERT INTO `currency_value`
VALUES (1, "2014-10-29 16:41:57.0", 'ACTIVE', "2014-10-29 16:41:57.0", 0, 2.8408, 3, 1, 1),
  (2, "2014-10-29 16:41:57.0", 'ACTIVE', "2014-10-29 16:41:57.0", 0, 0.05346, 4, 1, 1),
  (3, "2014-10-29 16:41:57.0",'ACTIVE', "2014-10-29 16:41:57.0", 0, 2.2398, 2, 1, 1);

INSERT INTO `destination_point` VALUES
  (1, '2014-10-25 20:54:32', 'ACTIVE', '2014-10-25 20:54:32', 0, 'AYT', '36.90621795243867,30.80429345369339',
   'Antalya Airport', 'AIRPORT', 1),
  (2, '2014-10-25 20:54:55', 'ACTIVE', '2014-10-25 20:54:55', 0, 'MUSEUM', '36.88503778543135,30.680115222930908',
   'Antalya Museum', 'REGION', 1);

INSERT INTO `keyword`
VALUES (1, '2014-10-25 20:50:01', 'ACTIVE', '2014-10-25 20:50:01', 0, 'db77174aa34ded1b6139455a58d0a38b', 'Turkey'),
  (2, '2014-10-25 20:53:58', 'ACTIVE', '2014-10-25 20:53:58', 0, '2c3f5a5391e1647d547133fa5ca119b8',
   'United States of America');

INSERT INTO `language` VALUES (1, '2014-10-25 20:44:01', 'ACTIVE', '2014-10-25 20:44:01', 1, 'TR', 'tr-TR', 'Turkish'),
  (2, '2014-10-25 20:44:18', 'ACTIVE', '2014-10-25 20:44:18', 1, 'EN', 'en-US', 'English');

INSERT INTO `privilege` VALUES (1, NULL, 'ACTIVE', NULL, 0, 'test priv', 'test priv');

INSERT INTO `role` VALUES (1, NULL, 'ACTIVE', NULL, 0, 'The Super User', 'ORCA'),
  (2, NULL, 'ACTIVE', NULL, 0, 'The Agency User', 'AGENCY ROLE');

INSERT INTO `user` VALUES
  (1, NULL, 'ACTIVE', NULL, 0, NULL, 'orca@orcateam.org', 'MALE', 'Orca', '827ccb0eea8a706c4c34a16891f84e7b', 'Team',
   1);

INSERT INTO `role_privilege` VALUES (1, 1);

INSERT INTO `system_user` VALUES (1);

INSERT INTO `transfer`
VALUES (1, '2014-10-25 20:55:37', 'ACTIVE', '2014-10-25 20:55:37', 0, 'AYTMUSE', NULL, 1, 1, 1, 2);

INSERT INTO `transfer_price` VALUES (1, '2014-10-25 20:56:07', 'ACTIVE', '2014-10-25 20:56:07', 0, 100.00, 4, 2, 1),
  (2, '2014-10-25 20:56:18', 'ACTIVE', '2014-10-25 20:56:18', 0, 150.00, 5, 2, 1),
  (3, '2014-10-25 20:55:58', 'ACTIVE', '2014-10-25 20:55:58', 0, 120.00, 1, 2, 1);




