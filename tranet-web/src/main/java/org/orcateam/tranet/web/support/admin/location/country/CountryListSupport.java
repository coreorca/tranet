package org.orcateam.tranet.web.support.admin.location.country;

import org.orcateam.tranet.core.dao.parameter.CountryParameter;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.orcateam.tranet.web.navigation.NavRule;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CountryListSupport extends BaseListSupport<Country, Integer> {

    CountryParameter params = new CountryParameter();

    @Autowired
    transient CountryService countryService;

    @Override
    public void reset() {
        params = new CountryParameter();
        super.reset();
    }

    @Override
    @Secured("admin.location.country.list")
    protected void initResultList() {
        Pagination<Country> pagination = countryService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.location.country.view")
    public String navToView(Country country) {
        NavRule rule = new NavRule("countryView", "id", country.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.location.country.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("countryEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "countryList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public CountryParameter getParams() {
        return params;
    }

    public void setParams(CountryParameter params) {
        this.params = params;
    }
}
