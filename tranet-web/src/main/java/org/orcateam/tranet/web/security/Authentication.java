package org.orcateam.tranet.web.security;

import org.orcateam.tranet.core.model.auth.User;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("session")
public class Authentication {

    public boolean hasRole(String role) {
        //return getAuthorities().contains(new SimpleGrantedAuthority(role));
        return true;
    }

    public User getCurrentUser() {
        AuthUser authUser = (AuthUser) getSecurity().getPrincipal();
        return authUser.getUser();
    }

    public List<GrantedAuthority> getAuthorities() {
        return (List<GrantedAuthority>) getSecurity().getAuthorities();
    }

    private org.springframework.security.core.Authentication getSecurity() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public boolean isAuthenticated() {
        return getSecurity().isAuthenticated();
    }
}
