package org.orcateam.tranet.web.jsf;

import com.sun.faces.renderkit.html_basic.SelectManyCheckboxListRenderer;
import org.orcateam.tranet.core.model.BaseEntity;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.lang.reflect.Array;

public class ExtendedSelectManyCheckboxListRenderer extends SelectManyCheckboxListRenderer {

    @Override
    protected boolean isSelected(FacesContext context, UIComponent component, Object itemValue, Object valueArray, Converter converter) {

        if (itemValue == null && valueArray == null) {
            return true;
        }
        if (null != valueArray) {
            if (!valueArray.getClass().isArray()) {
                logger.warning("valueArray is not an array, the actual type is " + valueArray.getClass());
                return valueArray.equals(itemValue);
            }
            int len = Array.getLength(valueArray);
            for (int i = 0; i < len; i++) {
                Object value = Array.get(valueArray, i);
                if (value == null && itemValue == null) {
                    return true;
                } else {
                    if ((value == null) ^ (itemValue == null)) {
                        continue;
                    }
                    Object compareValue;
                    if (converter == null) {
                        compareValue = coerceToModelType(context, itemValue, value.getClass());
                    } else {
                        compareValue = itemValue;
                        if (compareValue instanceof String && !(value instanceof String)) {
                            // type mismatch between the time and the value
                            // we're
                            // comparing. Invoke the Converter.
                            compareValue = converter.getAsObject(context, component, (String) compareValue);
                        }
                    }

                    /**
                     * If object instance of enderun baseEntity
                     */
                    if (value instanceof BaseEntity && compareValue instanceof BaseEntity) {
                        if (((BaseEntity) value).getId().equals(((BaseEntity) compareValue).getId())) {
                            return (true);
                        }
                    } else if (value.equals(compareValue)) {
                        return (true);
                    }
                }
            }
        }
        return false;

    }

}
