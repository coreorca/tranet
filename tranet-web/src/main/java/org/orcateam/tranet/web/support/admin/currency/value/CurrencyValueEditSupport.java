package org.orcateam.tranet.web.support.admin.currency.value;

import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.core.service.CurrencyValueService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class CurrencyValueEditSupport extends BaseEditSupport<CurrencyValue, Integer> {

    private List<CurrencyType> currencyTypeList = null;

    @Autowired
    transient CurrencyValueService entityService;

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    @Override
    @Secured("admin.currency.value.create")
    public void initCreate() {
        CurrencyValue entity = new CurrencyValue();
        setInstance(entity);
        initCurrencyTypeList();
    }

    @Override
    @Secured("admin.currency.value.update")
    public void initUpdate(Integer instanceId) {
        CurrencyValue entity = entityService.getWithTypes(instanceId);
        setInstance(entity);
        initCurrencyTypeList();
    }

    private void initCurrencyTypeList() {
        if (currencyTypeList == null) {
            currencyTypeList = currencyTypeService.getAll();
        }
    }

    private void validation() throws Exception {
        if (instance.getTo().getId().equals(instance.getFrom().getId())) {
            throw new Exception("admin.currency.value.validation.cannotBeSameCurrencyType");
        }
    }

    @Override
    public String persist() {
        try {
            validation();
            CurrencyValue persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            validation();
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            validation();
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "currencyValueEdit}";
    }

    public String navToList() {
        NavRule rule = new NavRule("currencyValueList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("currencyValueView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("currencyValueList");
        } else {
            NavRule rule = new NavRule("currencyValueView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }
}
