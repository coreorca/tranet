package org.orcateam.tranet.web.support.admin.provider;

import org.orcateam.tranet.core.model.provider.Provider;
import org.orcateam.tranet.core.service.ProviderService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class ProviderViewSupport extends BaseViewSupport<Provider, Integer> {

    @Autowired
    transient ProviderService providerService;

    @Override
    @Secured("admin.provider.view")
    public void initView(Integer instanceId) throws Exception {
        Provider provider = providerService.findEagerlyById(instanceId);
        setInstance(provider);
    }

    @Override
    @Secured("admin.provider.update")
    public String navToEdit() {
        NavRule rule = new NavRule("providerEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "providerView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("providerList");
    }
}
