package org.orcateam.tranet.web.support.admin.currency.type;

import org.orcateam.tranet.core.dao.parameter.CurrencyTypeParameter;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.orcateam.tranet.web.navigation.NavRule;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CurrencyTypeListSupport extends BaseListSupport<CurrencyType, Integer> {

    CurrencyTypeParameter params = new CurrencyTypeParameter();

    @Autowired
    transient CurrencyTypeService entityService;

    @Override
    public void reset() {
        params = new CurrencyTypeParameter();
        super.reset();
    }

    @Override
    @Secured("admin.currency.type.list")
    protected void initResultList() {
        Pagination<CurrencyType> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.currency.type.view")
    public String navToView(CurrencyType entity) {
        NavRule rule = new NavRule("currencyTypeView", "id", entity.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.currency.type.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("currencyTypeEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "currencyTypeList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public CurrencyTypeParameter getParams() {
        return params;
    }

    public void setParams(CurrencyTypeParameter params) {
        this.params = params;
    }

}
