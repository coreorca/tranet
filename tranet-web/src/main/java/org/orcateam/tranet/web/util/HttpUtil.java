package org.orcateam.tranet.web.util;

import org.apache.commons.lang3.StringUtils;
import org.orcateam.tranet.web.navigation.NavUtil;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class HttpUtil {



    public static Map<String, String> getHttpParamMap() {
        Map<String, String> params = new HashMap<String, String>();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        for (Object o : request.getParameterMap().keySet()) {
            String param = (String) o;
            String value = request.getParameter(param);
            params.put(param, value);
        }
        return params;
    }

    public static  Long getNavId() {
        Long navId = null;
        String navIdStr = getHttpParamMap().get(NavUtil.NAV_ID);
        if(StringUtils.isNotBlank(navIdStr)) {
            try {
                navId = new Long(navIdStr);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return navId;
    }



}
