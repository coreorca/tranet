package org.orcateam.tranet.web.support.agency.auth.user;

import org.apache.commons.lang3.StringUtils;
import org.orcateam.tranet.core.enumeration.GenderEnum;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.AgencyService;
import org.orcateam.tranet.core.service.AgencyUserService;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AgencyUserEditSupport extends BaseEditSupport<AgencyUser, Integer> {

    private String newPassword;
    private List<Role> roleList = new ArrayList<Role>();
    private List<Agency> agencyList = new ArrayList<Agency>();

    @Autowired
    transient AgencyUserService agencyUserService;

    @Autowired
    transient RoleService roleService;

    @Autowired
    transient AgencyService agencyService;

    @Override
    @Secured("agency.auth.user.create")
    public void initCreate() throws Exception {

        AgencyUser agencyUser = new AgencyUser();
        setInstance(agencyUser);

        initRoleList();
    }

    @Override
    @Secured("agency.auth.user.update")
    public void initUpdate(Integer instanceId) throws Exception {

        AgencyUser agencyUser = agencyUserService.findEagerlyById(instanceId);

        if (agencyUser == null) {
            throw new NotFoundException();
        }
        setInstance(agencyUser);

        initRoleList();
    }

    private void setAgency() {
        AgencyUser currentUser = (AgencyUser) authentication.getCurrentUser();
        Agency agency = currentUser.getAgency();
        getInstance().setAgency(agency);
    }

    private void initRoleList() {
        roleList = roleService.findAll();
    }

    public GenderEnum[] getGenders() {
        return GenderEnum.values();
    }

    @Override
    public String persist() {
        try {
            if (StringUtils.isEmpty(newPassword)) {
                WebUtil.addErrorMessage("agency.auth.user.error.passwordRequired");
                return null;
            }

            setAgency();

            AgencyUser agencyUser = agencyUserService.saveNewUser(instance, newPassword);
            setInstanceId(agencyUser.getId());

            WebUtil.addInfoMessage("agency.auth.user.success.create");

            return navToBack();
        } catch (ClassCastException cce) {
            WebUtil.addErrorMessage("agency.auth.user.onlyAgencyUseradd", cce.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String update() {
        try {
            agencyUserService.saveNewUser(instance, newPassword);

            WebUtil.addInfoMessage("agency.auth.user.success.update");

            return navToView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String remove() {
        return null;
    }

    @Override
    public boolean isManaged() {
        return instanceId != null;
    }

    @Override
    protected String getViewId() {
        return "agencyUserEdit";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("agencyUserList");
    }

    private String navToView() {
        NavRule navToView = new NavRule("agencyUserView");
        navToView.addParam("instanceId", instanceId.toString());
        return nav.cleanRedirect(navToView, getNavId());
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public List<Agency> getAgencyList() {
        return agencyList;
    }

    public void setAgencyList(List<Agency> agencyList) {
        this.agencyList = agencyList;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
