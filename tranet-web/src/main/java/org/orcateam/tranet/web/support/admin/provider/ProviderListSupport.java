package org.orcateam.tranet.web.support.admin.provider;

import org.orcateam.tranet.core.dao.parameter.ProviderParameter;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.provider.Provider;
import org.orcateam.tranet.core.service.ProviderService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ProviderListSupport extends BaseListSupport<Provider, Integer> {

    ProviderParameter params = new ProviderParameter();

    @Autowired
    transient ProviderService providerService;

    @Override
    @Secured("admin.provider.list")
    protected void initResultList() {
        Pagination<Provider> pagination = providerService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.provider.view")
    public String navToView(Provider provider) {
        NavRule rule = new NavRule("providerView", "id", provider.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }


    @Override
    @Secured("admin.provider.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("providerEdit"), navId.getValue());
    }

    @Override
    public void reset() {
        params = new ProviderParameter();
        super.reset();
    }

    @Override
    protected String getViewId() {
        return "providerList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public ProviderParameter getParams() {
        return params;
    }

    public void setParams(ProviderParameter params) {
        this.params = params;
    }
}
