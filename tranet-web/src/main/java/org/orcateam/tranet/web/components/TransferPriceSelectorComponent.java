package org.orcateam.tranet.web.components;

import org.orcateam.tranet.core.model.embeddable.Money;
import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.model.transfer.TransferPrice;
import org.orcateam.tranet.core.util.SessionUtil;
import org.orcateam.tranet.web.BaseBean;
import org.orcateam.tranet.web.support.Ext;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class TransferPriceSelectorComponent extends BaseBean {

    private Transfer transfer = null;
    private List<Ext<TransferPrice>> priceList = new ArrayList<Ext<TransferPrice>>();

    @Autowired
    SessionUtil sessionUtil;

    public void init(Transfer transfer) {
        try {
            this.transfer = transfer;
            priceList.clear();
            for (TransferPrice price : transfer.getPriceSet()) {
                priceList.add(new Ext<TransferPrice>(price, false));
            }
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
        }
    }

    public List<TransferPrice> getSelectedList() {
        List<TransferPrice> selectedList = new ArrayList<TransferPrice>();
        for (Ext<TransferPrice> ext : priceList) {
            if (ext.isSelected()) {
                selectedList.add(ext.getInstance());
            }
        }
        return selectedList;
    }

    public void priceClickAction(Ext<TransferPrice> priceExt) {
        priceExt.setSelected(!priceExt.isSelected());
    }

    public Money getTotalMoney() {
        Money money = new Money();
        money.setCurrencyType(sessionUtil.getCurrency());
        List<TransferPrice> selectedList = getSelectedList();
        for (TransferPrice price : selectedList) {
            money.add(price.getMoney().getConverted().getValue());
        }
        return money;
    }

    public Integer getTotalPax() {
        Integer totalPax = 0;
        List<TransferPrice> selectedList = getSelectedList();
        for (TransferPrice price : selectedList) {
            totalPax += price.getCarType().getPax();
        }
        return totalPax;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public List<Ext<TransferPrice>> getPriceList() {
        return priceList;
    }
}
