package org.orcateam.tranet.web.support;

public abstract class BaseEditSupport<E, PK> extends BaseSupport<E, PK> {

    public abstract void initCreate() throws Exception;

    public abstract void initUpdate(PK instanceId) throws Exception;

    public abstract String persist();

    public abstract String update();

    public abstract String remove();

    public abstract boolean isManaged();

    @Override
    protected void callInitFunctions() throws Exception {
        if (instanceId != null) {
            checkSecured("initUpdate", instanceId.getClass());
            logger.debug("calling initUpdate(" + instanceId + ") function from support");
            initUpdate(instanceId);
        } else {
            checkSecured("initCreate");
            logger.debug("calling initCreate() function from support.");
            initCreate();
        }
    }
}
