package org.orcateam.tranet.web.support.admin.location.country;

import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CountryEditSupport extends BaseEditSupport<Country, Integer> {

    private String name;

    @Autowired
    transient CountryService countryService;

    @Override
    @Secured("admin.location.country.create")
    public void initCreate() throws Exception {
        Country country = new Country();
        setInstance(country);
    }

    @Override
    @Secured("admin.location.country.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Country country = countryService.get(instanceId);
        setInstance(country);
        if (country == null) {
            throw new NotFoundException();
        }
        name = country.getName().getText();
    }

    @Override
    public String persist() {
        try {
            Country pesisted = countryService.saveWithKeyword(instance, name);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(pesisted.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }


    @Override
    public String update() {
        try {
            countryService.updateWithKeyword(instance, name);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            countryService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "countryEdit";
    }


    public String navToList() {
        NavRule rule = new NavRule("countryList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("countryView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("countryList");
        } else {
            NavRule rule = new NavRule("countryView");
            rule.addParam("countryId", instanceId.toString());
            return rule;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
