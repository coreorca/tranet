package org.orcateam.tranet.web.support.admin.car.carType;

import org.orcateam.tranet.core.dao.parameter.CarTypeParameter;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.service.CarTypeService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CarTypeListSupport extends BaseListSupport<CarType, Integer> {

    CarTypeParameter params = new CarTypeParameter();

    @Autowired
    transient CarTypeService carTypeService;

    @Override
    @Secured("admin.car.carType.list")
    protected void initResultList() {
        Pagination<CarType> pagination = carTypeService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.car.carType.view")
    public String navToView(CarType carType) {
        NavRule rule = new NavRule("carTypeView", "id", carType.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.car.carType.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("carTypeEdit"), navId.getValue());
    }

    @Override
    public void reset() {
        params = new CarTypeParameter();
        super.reset();
    }

    @Override
    protected String getViewId() {
        return "carTypeList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public CarTypeParameter getParams() {
        return params;
    }

    public void setParams(CarTypeParameter params) {
        this.params = params;
    }
}
