package org.orcateam.tranet.web.support.admin.auth.user;

import org.apache.commons.lang3.StringUtils;
import org.orcateam.tranet.core.enumeration.GenderEnum;
import org.orcateam.tranet.core.enumeration.UserTypeEnum;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.model.provider.Provider;
import org.orcateam.tranet.core.service.AgencyService;
import org.orcateam.tranet.core.service.ProviderService;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.core.service.UserService;
import org.orcateam.tranet.core.util.UserUtil;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseWizardSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class UserWizardSupport extends BaseWizardSupport<User, Integer> {

    String[] states = {
            "admin.auth.user.wizard.step.selectType",
            "admin.auth.user.wizard.step.information"
    };

    private UserTypeEnum userType;

    /**
     * Params from url
     */
    private Integer paramUserType;
    private Integer innerId;

    private String newPassword;

    private List<Role> roleList = null;

    /**
     * AgencyUser
     */
    private List<Agency> agencyList = null;

    /**
     * ProviderUser
     */
    private List<Provider> providerList = null;

    @Autowired
    transient UserService entityService;

    @Autowired
    transient RoleService roleService;

    @Autowired
    transient AgencyService agencyService;

    @Autowired
    transient ProviderService providerService;

    @Override
    @Secured("admin.auth.user.create")
    public void initWizard() throws Exception {
        putAll(states);

        if (paramUserType != null && innerId != null) {
            initCustomCreate();
        }

        initRoleList();
    }

    private void initCustomCreate() throws Exception {
        userType = UserTypeEnum.resolveFrom(paramUserType);

        setInstance(UserUtil.getUser(userType));
        if (userType.equals(UserTypeEnum.AGENCY_USER)) {
            initAgencyUserWithParam();
            initAgencyList();
            setIndex(1);
        }
    }

    private void initAgencyUserWithParam() throws Exception {
        Agency agency = agencyService.get(innerId);
        if (agency == null) {
            throw new NotFoundException();
        }
        ((AgencyUser) instance).setAgency(agency);
    }

    @Override
    @Secured("admin.auth.user.update")
    public void initWizard(Integer instanceId) throws Exception {
        putAll(states);

        User user = entityService.findEagerlyById(instanceId);

        if (user == null) {
            throw new NotFoundException();
        }

        setInstance(user);

        userType = UserUtil.getUserType(instance);

        initRoleList();
    }

    private void initRoleList() {
        if (roleList == null) {
            roleList = roleService.findAll();
        }
    }

    private void initAgencyList() {
        if (agencyList == null) {
            agencyList = agencyService.getAll();
        }
    }

    private void initProviderList() {
        if (providerList == null) {
            providerList = providerService.getAll();
        }
    }

    @Override
    public void next() {
        try {
            super.next();
            if (instanceId == null && getIndex().equals(1)) {
                User user = UserUtil.getUser(userType);
                setInstance(user);
            }

            if (getIndex().equals(1)) {
                switch (userType) {
                    case AGENCY_USER:
                        initAgencyList();
                        break;
                    case PROVIDER_USER:
                        initProviderList();
                        break;
                }
            }
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.debug(e);
        }
    }

    @Override
    public String finish() {
        try {
            if (instanceId != null) {
                return update();
            } else {
                return persist();
            }
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String cancel() {
        return nav.cleanRedirect(new NavRule("invoiceList"), getNavId());
    }

    @Override
    public String persist() {
        if (StringUtils.isBlank(newPassword)) {
            WebUtil.addErrorMessage("admin.auth.user.error.passwordRequired");
            return null;
        }

        User entity = entityService.saveNewUser(instance, newPassword);
        setInstanceId(entity.getId());

        WebUtil.addInfoMessage("admin.auth.user.success.create");

        try {
            // why is this throws exception?
            return navToBack();
        } catch (Exception e) {
            return navToView(instanceId);
        }
    }

    @Override
    public String update() {
        entityService.updateWithNewPassword(instance, newPassword);

        WebUtil.addInfoMessage("admin.auth.user.success.update");

        return navToView(instanceId);
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("userView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    public boolean isManaged() {
        return instanceId != null;
    }

    @Override
    protected String getViewId() {
        return "userWizard";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("userList");
        } else {
            NavRule rule = new NavRule("userView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public UserTypeEnum[] getUserTypes() {
        // TODO has to be change according to user permission!
        return UserTypeEnum.values();
    }

    public GenderEnum[] getGenders() {
        return GenderEnum.values();
    }

    public boolean isSystemUser() {
        try {
            return UserUtil.getUserType(instance).equals(UserTypeEnum.SYSTEM_USER);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isAgencyUser() {
        try {
            return UserUtil.getUserType(instance).equals(UserTypeEnum.AGENCY_USER);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isProviderUser() {
        try {
            return UserUtil.getUserType(instance).equals(UserTypeEnum.PROVIDER_USER);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isCustomerUser() {
        try {
            return UserUtil.getUserType(instance).equals(UserTypeEnum.CUSTOMER_USER);
        } catch (Exception e) {
            return false;
        }
    }


    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEnum userType) {
        this.userType = userType;
    }

    public Integer getParamUserType() {
        return paramUserType;
    }

    public void setParamUserType(Integer paramUserType) {
        this.paramUserType = paramUserType;
    }

    public Integer getInnerId() {
        return innerId;
    }

    public void setInnerId(Integer innerId) {
        this.innerId = innerId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public List<Agency> getAgencyList() {
        return agencyList;
    }

    public void setAgencyList(List<Agency> agencyList) {
        this.agencyList = agencyList;
    }

    public List<Provider> getProviderList() {
        return providerList;
    }

    public void setProviderList(List<Provider> providerList) {
        this.providerList = providerList;
    }
}
