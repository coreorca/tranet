package org.orcateam.tranet.web.support;

public abstract class BaseViewSupport<E, PK> extends BaseSupport<E, PK> {

    public abstract void initView(PK instanceId) throws Exception;

    @Override
    protected void callInitFunctions() throws Exception {
        if (instanceId == null) {
            throw new Exception("instanceId parameter cannot be null!");
        }
        checkSecured("initView", instanceId.getClass());
        logger.debug("calling initView(" + instanceId + ") function from support");
        initView(instanceId);
    }

    public abstract String navToEdit();
}
