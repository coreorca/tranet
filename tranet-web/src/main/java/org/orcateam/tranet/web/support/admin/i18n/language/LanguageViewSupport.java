package org.orcateam.tranet.web.support.admin.i18n.language;

import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.service.LanguageService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class LanguageViewSupport extends BaseViewSupport<Language, Integer> {

    @Autowired
    transient LanguageService entityService;

    @Override
    @Secured("admin.i18n.language.view")
    public void initView(Integer instanceId) {
        Language result = entityService.get(instanceId);
        setInstance(result);
    }

    @Secured("admin.i18n.language.update")
    public String navToEdit() {
        NavRule rule = new NavRule("languageEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "languageView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("languageList");
    }

}
