package org.orcateam.tranet.web.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeUtil {


    public static Date getDateWithTime(Date date, Time time) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, time.getHour());
        calendar.set(Calendar.MINUTE, time.getMinute());
        return calendar.getTime();
    }

    public static Time getTime(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String[] timeStr =  df.format(date).split(":");
        return new Time(new Integer(timeStr[0]), new Integer(timeStr[1]));
    }

    public static String toStringTime(Time time) {
        if (time == null) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        if (time.getHour() != null && time.getHour() < 10) {
            buffer.append("0");
        }
        buffer.append(time.getHour());
        buffer.append(":");
        if (time.getMinute() != null && time.getMinute() < 10) {
            buffer.append("0");
        }
        buffer.append(time.getMinute());
        return buffer.toString();
    }

}
