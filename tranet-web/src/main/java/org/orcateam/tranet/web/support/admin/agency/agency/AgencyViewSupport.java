package org.orcateam.tranet.web.support.admin.agency.agency;

import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.model.balance.Balance;
import org.orcateam.tranet.core.service.AgencyService;
import org.orcateam.tranet.core.service.AgencyUserService;
import org.orcateam.tranet.core.service.UserService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AgencyViewSupport extends BaseViewSupport<Agency, Integer> {

    private List<Balance> balanceList = new ArrayList<Balance>();
    private List<AgencyUser> agencyUserList = new ArrayList<AgencyUser>();

    @Autowired
    transient AgencyService entityService;

    @Autowired
    transient UserService userService;

    @Autowired
    transient AgencyUserService agencyUserService;

    @Override
    @Secured("admin.agency.agency.view")
    public void initView(Integer instanceId) throws Exception {
        Agency entity = entityService.findEagerlyById(instanceId);
        if (entity == null) {
            throw new NotFoundException();
        }
        setInstance(entity);

        initBalanceList();
        initAgencyUserList();
    }

    private void initBalanceList() {
        balanceList.clear();
        if (instance.getBalanceSet() != null) {
            balanceList = new ArrayList<Balance>(instance.getBalanceSet());
        }
    }

    private void initAgencyUserList() {
        agencyUserList.clear();
        if (instanceId != null) {
            agencyUserList = agencyUserService.getAllByAgencyId(instanceId);
        }
    }

    @Override
    @Secured("admin.agency.agency.update")
    public String navToEdit() {
        NavRule rule = new NavRule("agencyEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "agencyView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("agencyList");
    }

    public List<Balance> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<Balance> balanceList) {
        this.balanceList = balanceList;
    }

    public List<AgencyUser> getAgencyUserList() {
        return agencyUserList;
    }

    public void setAgencyUserList(List<AgencyUser> agencyUserList) {
        this.agencyUserList = agencyUserList;
    }
}
