package org.orcateam.tranet.web.support.front.reservation;

import org.orcateam.tranet.core.enumeration.ReservationStatusEnum;
import org.orcateam.tranet.core.enumeration.ReservationTypeEnum;
import org.orcateam.tranet.core.enumeration.RouteTypeEnum;
import org.orcateam.tranet.core.model.embeddable.Money;
import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.model.reservation.ReservationCar;
import org.orcateam.tranet.core.model.transfer.TransferPrice;
import org.orcateam.tranet.core.util.SessionUtil;
import org.orcateam.tranet.web.components.RouteSelectorComponent;
import org.orcateam.tranet.web.components.TransferPriceSelectorComponent;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BaseWizardSupport;
import org.orcateam.tranet.web.support.front.cart.FrontEndCart;
import org.orcateam.tranet.web.util.Time;
import org.orcateam.tranet.web.util.TimeUtil;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@ManagedBean
@ViewScoped
public class FrontReservationWizardSupport extends BaseWizardSupport<Reservation, Integer> {

    private Time flightTime = null;
    private Time pickupTime = null;

    private Money totalMoney = null;
    private Integer totalPax = 0;

    private boolean editInCart = false;
    private String editInCartCode = null;

    List<ReservationCar> carList = new ArrayList<ReservationCar>();

    @Autowired
    FrontEndCart cart;

    @Autowired
    SessionUtil sessionUtil;

    @ManagedProperty("#{routeSelectorComponent}")
    RouteSelectorComponent routaSelector;

    @ManagedProperty("#{transferPriceSelectorComponent}")
    TransferPriceSelectorComponent priceSelector;

    String[] states = {
            "front.reservation.wizard.step.selectRoute",
            "front.reservation.wizard.step.inputFlightAndInformation",
            "front.reservation.wizard.step.selectCarTypes",
            "front.reservation.wizard.step.finish"
    };


    @Override
    public void initWizard() {
        putAll(states);
        Reservation reservation = new Reservation();
        reservation.setType(ReservationTypeEnum.INDIVIDUAL);
        reservation.setReservationStatus(ReservationStatusEnum.WAITING);
        reservation.getCode().setValue("" + System.currentTimeMillis());
        setInstance(reservation);
        routaSelector.init();
        totalMoney = new Money(0d, sessionUtil.getCurrency());
    }

    @Override
    public void initWizard(Integer instanceId) throws Exception {
        putAll(states);
        totalMoney = new Money(0d, sessionUtil.getCurrency());
        List<Reservation> reservationList = cart.getReservationList();
        for (Reservation reservation : reservationList) {
            if (reservation.getCode().getValue().equals(editInCartCode)) {
                setInstance(reservation);
                initRoutaSelector();
                flightTime = TimeUtil.getTime(instance.getFlightDate());
                pickupTime = TimeUtil.getTime(instance.getPickupDate());
                carList = new ArrayList<ReservationCar>(instance.getReservationCarSet());
                editInCart = true;
                refreshTotalValues();
            }
        }
        if (instance == null) {
            throw new NoResultException();
        }
    }

    public void initEditInCart() {
        setInstanceId(0);
    }

    private void initRoutaSelector() {
        routaSelector.init(instance.getRouteType(), instance.getCountry(), instance.getCity(), instance.getFrom(), instance.getTo());
    }

    @Override
    public void next() {
        try {
            switch (getIndex()) {
                case 1:
                    validateFlightInformation();
                    break;
                case 2:
                    validateCarSelection();
            }
            super.next();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
        }
    }

    private void validateFlightInformation() throws Exception {
        Date flightDate = TimeUtil.getDateWithTime(getInstance().getFlightDate(), flightTime);
        Date pickupDate = TimeUtil.getDateWithTime(getInstance().getPickupDate(), pickupTime);
        if (pickupDate.before(flightDate)) {
            throw new Exception("front.reservation.wizard.validation.flightDateNotBeforePickupDate");
        }
        getInstance().setFlightDate(flightDate);
        getInstance().setPickupDate(pickupDate);

    }

    private void validateCarSelection() throws Exception {
        if (carList.isEmpty()) {
            throw new Exception("front.reservation.wizard.validation.carListCannotBeEmpty");
        }
        if (getInstance().getPax() > totalPax) {
            String msg = WebUtil.getMessage("front.reservation.wizard.validation.totalPaxCannotlessThanYourPax", totalPax.toString(), getInstance().getPax().toString());
            throw new Exception(msg);
        }
    }

    public void addCarAction() {
        List<TransferPrice> priceList = priceSelector.getSelectedList();
        for (TransferPrice price : priceList) {
            ReservationCar car = new ReservationCar();
            car.setReservation(getInstance());
            car.setCarType(price.getCarType());
            car.setPrice(price.getMoney());
            carList.add(car);
        }
        refreshTotalValues();
    }

    private void refreshTotalValues() {
        totalMoney.setValue(0d);
        totalPax = 0;
        for (ReservationCar car : carList) {
            totalMoney.add(car.getPrice().getConverted().getValue());
            totalPax += car.getCarType().getPax();

        }
    }

    public void removeCardAction(ReservationCar car) {
        carList.remove(car);
        refreshTotalValues();
    }


    @Override
    public String finish() {
        try {
            applyReservationValus();
            if (!editInCart) {
                cart.add(getInstance());
                WebUtil.addInfoMessage("global.messages.add.success");
            } else {
                WebUtil.addInfoMessage("global.messages.update.success");
            }
            return nav.cleanRedirect(new NavRule("frontCartPage"), getNavId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    private void applyReservationValus() throws Exception {
        // setting route selection
        getInstance().setRouteType(routaSelector.getRouteType());
        getInstance().setCountry(routaSelector.getCountry());
        getInstance().setCity(routaSelector.getCity());

        // setting reservation from and to value
        if (RouteTypeEnum.AIRPORT_TO_REGION.equals(getInstance().getRouteType())) {
            getInstance().setFrom(routaSelector.getAirport());
            getInstance().setTo(routaSelector.getRegion());
        } else if (RouteTypeEnum.REGION_TO_AIRPORT.equals(getInstance().getRouteType())) {
            getInstance().setTo(routaSelector.getAirport());
            getInstance().setFrom(routaSelector.getRegion());
        }
        // route selection
        else {
            throw new Exception("front.reservation.wizard.validation.unknownRouteSelection");
        }
        // setting reservation cars
        getInstance().setReservationCarSet(new HashSet<ReservationCar>(carList));

        // setting reservation price
        getInstance().setPrice(getTotalMoney());

        // setting reservation user
        if (authentication.isAuthenticated()) {
            getInstance().setUser(authentication.getCurrentUser());
        }
    }

    @Override
    public String cancel() {
        return nav.cleanRedirect(new NavRule("frontMainPage"), getNavId());
    }

    @Override
    public String persist() {
        return null;
    }

    @Override
    public String update() {
        return null;
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "frontReservationWizard";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public RouteSelectorComponent getRoutaSelector() {
        return routaSelector;
    }

    public void setRoutaSelector(RouteSelectorComponent routaSelector) {
        this.routaSelector = routaSelector;
    }

    public Time getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(Time flightTime) {
        this.flightTime = flightTime;
    }

    public Time getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Time pickupTime) {
        this.pickupTime = pickupTime;
    }

    public TransferPriceSelectorComponent getPriceSelector() {
        return priceSelector;
    }

    public void setPriceSelector(TransferPriceSelectorComponent priceSelector) {
        this.priceSelector = priceSelector;
    }

    public List<ReservationCar> getCarList() {
        return carList;
    }

    public void setCarList(List<ReservationCar> carList) {
        this.carList = carList;
    }

    public Money getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Money totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Integer getTotalPax() {
        return totalPax;
    }

    public void setTotalPax(Integer totalPax) {
        this.totalPax = totalPax;
    }

    public String getEditInCartCode() {
        return editInCartCode;
    }

    public void setEditInCartCode(String editInCartCode) {
        this.editInCartCode = editInCartCode;
    }
}
