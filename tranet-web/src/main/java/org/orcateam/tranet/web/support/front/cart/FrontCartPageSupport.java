package org.orcateam.tranet.web.support.front.cart;

import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BasePageSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class FrontCartPageSupport extends BasePageSupport {


    @Autowired
    transient FrontEndCart cart;


    @Override
    public void initPage() {
    }


    public String navToCheckoutPage() {
        NavRule rule = new NavRule("frontCheckoutPage");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToEdit(Reservation reservation) {
        NavRule rule = new NavRule("frontReservationWizard");
        rule.addParam("code", reservation.getCode().getValue());
        return nav.cleanRedirect(rule, getNavId());
    }

    @Override
    protected String getViewId() {
        return "frontCartPage";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("frontMainPage");
    }

}
