package org.orcateam.tranet.web.support.admin.i18n.keyword;

import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.service.KeywordService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class KeywordViewSupport extends BaseViewSupport<Keyword, Integer> {

    @Autowired
    transient KeywordService entityService;

    @Override
    @Secured("admin.i18n.keyword.view")
    public void initView(Integer instanceId) {
        Keyword result = entityService.get(instanceId);
        setInstance(result);
    }

    @Secured("admin.i18n.keyword.update")
    public String navToEdit() {
        NavRule rule = new NavRule("keywordEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "keywordView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("keywordList");
    }

}
