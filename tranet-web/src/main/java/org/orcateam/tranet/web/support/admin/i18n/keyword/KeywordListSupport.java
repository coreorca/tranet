package org.orcateam.tranet.web.support.admin.i18n.keyword;

import org.orcateam.tranet.core.dao.parameter.KeywordParameter;
import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.service.KeywordService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class KeywordListSupport extends BaseListSupport<Keyword, Integer> {

    KeywordParameter params = new KeywordParameter();

    @Autowired
    transient KeywordService entityService;

    @Override
    public void reset() {
        params = new KeywordParameter();
        super.reset();
    }

    @Override
    @Secured("admin.i18n.keyword.list")
    protected void initResultList() {
        Pagination<Keyword> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.i18n.keyword.view")
    public String navToView(Keyword entity) {
        NavRule rule = new NavRule("keywordView", "id", entity.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.i18n.keyword.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("keywordEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "keywordList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public KeywordParameter getParams() {
        return params;
    }

    public void setParams(KeywordParameter params) {
        this.params = params;
    }

}
