package org.orcateam.tranet.web.support.admin.invoice;

import org.orcateam.tranet.core.enumeration.InvoiceTypeEnum;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.auth.CustomerUser;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.invoice.Invoice;
import org.orcateam.tranet.core.service.AgencyService;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.core.service.InvoiceService;
import org.orcateam.tranet.core.service.UserService;
import org.orcateam.tranet.core.util.InvoiceUtil;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseWizardSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class InvoiceWizardSupport extends BaseWizardSupport<Invoice, Integer> {

    private InvoiceTypeEnum invoiceType = null;

    private List<Agency> agencyList = null;
    private List<CustomerUser> customerUserList = null;

    private List<CurrencyType> currencyTypeList = null;

    String[] states = {
            "admin.invoice.wizard.step.selectType",
            "admin.invoice.wizard.step.information"
    };

    @Autowired
    transient InvoiceService entityService;

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    @Autowired
    transient AgencyService agencyService;

    @Autowired
    transient UserService userService;

    @Override
    @Secured("admin.invoice.create")
    public void initWizard() throws Exception {
        putAll(states);

        initCurrencyTypeList();
        initAgencyList();
        initCustomerUserList();
    }

    @Override
    @Secured("admin.invoice.update")
    public void initWizard(Integer instanceId) throws Exception {
        putAll(states);

        Invoice invoice = entityService.findEagerlyById(instanceId);
        if (invoice == null) {
            throw new NotFoundException();
        }
        setInstance(invoice);

        invoiceType = InvoiceUtil.getInvoiceType(invoice);

        initCurrencyTypeList();
        initAgencyList();
        initCustomerUserList();
    }

    private void initCurrencyTypeList() {
        if (currencyTypeList == null) {
            currencyTypeList = currencyTypeService.getAll();
        }
    }

    private void initAgencyList() {
        if (agencyList == null) {
            agencyList = agencyService.getAll();
        }
    }

    private void initCustomerUserList() {
        if (customerUserList == null) {
            customerUserList = userService.getAllCustomerUsers();
        }
    }

    @Override
    public void next() {
        try {
            super.next();
            if (instanceId == null && getIndex().equals(1)) {
                Invoice invoice = InvoiceUtil.getInvoice(invoiceType);
                setInstance(invoice);
            }
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.debug(e);
        }
    }

    @Override
    public String finish() {
        try {
            if (instanceId != null) {
                return update();
            } else {
                return persist();
            }
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String cancel() {
        return nav.cleanRedirect(new NavRule("invoiceList"), getNavId());
    }

    @Override
    public String persist() {
        Invoice invoice = entityService.save(instance);
        return navToView(invoice.getId());
    }

    @Override
    public String update() {
        entityService.update(instance);
        return navToView(instanceId);
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("invoiceView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    public boolean isManaged() {
        return instanceId != null;
    }

    @Override
    protected String getViewId() {
        return "invoiceWizard";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("invoiceList");
        } else {
            NavRule rule = new NavRule("invoiceView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public boolean isAgency() {
        return invoiceType.equals(InvoiceTypeEnum.AGENCY_INVOICE);
    }

    public boolean isIndividual() {
        return invoiceType.equals(InvoiceTypeEnum.INDIVIDUAL_INVOCE);
    }

    public InvoiceTypeEnum[] getInvoiceTypeList() {
        return InvoiceTypeEnum.values();
    }

    public InvoiceTypeEnum getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceTypeEnum invoiceType) {
        this.invoiceType = invoiceType;
    }

    public List<Agency> getAgencyList() {
        return agencyList;
    }

    public void setAgencyList(List<Agency> agencyList) {
        this.agencyList = agencyList;
    }

    public List<CustomerUser> getCustomerUserList() {
        return customerUserList;
    }

    public void setCustomerUserList(List<CustomerUser> customerUserList) {
        this.customerUserList = customerUserList;
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }
}
