package org.orcateam.tranet.web.util;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.orcateam.tranet.web.components.GlobalMessage;
import org.orcateam.tranet.web.components.GlobalMessageManager;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

public class WebUtil {

    private static Map<String, String> constraintExceptions = new HashMap<String, String>(0);

    static {
        constraintExceptions.put("CURR_TYPE_UNQ", "admin.currency.value.validation.CURR_TYPE_UNQ");
    }

    public static void addInfoMessage(String message, String... params) {
        addFacesMessage(FacesMessage.SEVERITY_INFO, getMessage(message, params));
    }

    public static void addErrorMessage(String message, String... params) {
        if (StringUtils.isBlank(message)) {
            message = "global.error.unexceptedError";
        }
        addFacesMessage(FacesMessage.SEVERITY_ERROR, getMessage(message, params));
    }

    public static void addErrorMessageClient(String clientId, String message, String... params) {
        addFacesMessageClient(clientId, FacesMessage.SEVERITY_ERROR, getMessage(message, params));
    }

    public static void addWarnMessage(String message, String... params) {
        addFacesMessage(FacesMessage.SEVERITY_WARN, getMessage(message, params));
    }

    public static String getMessage(String message, String... params) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle("messages", context.getViewRoot().getLocale());
            String str = bundle.getString(message);
            return MessageFormat.format(str, params);
        } catch (Exception e) {
            return message;
        }
    }

    private static void addFacesMessage(FacesMessage.Severity severity, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        GlobalMessageManager manager = context.getApplication().evaluateExpressionGet(context, "#{globalMessageManager}", GlobalMessageManager.class);
        manager.push(new GlobalMessage(severity, message));

        //FacesMessage msg = new FacesMessage(severity, message, null);
        //context.addMessage(null, msg);
        //context.getExternalContext().getFlash().setKeepMessages(true);
    }

    private static void addFacesMessageClient(String clientId, FacesMessage.Severity severity, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(severity, message, null);
        context.addMessage(clientId, msg);
        context.getExternalContext().getFlash().setKeepMessages(true);
    }

    public static void addExceptionError(Exception exception) {
        String message = handleJPAExceptionMessage(exception);
        addErrorMessage(message);
    }

    public static String handleJPAExceptionMessage(Exception exception) {
        String message = exception.getMessage();
        Throwable parent = exception.getCause();
        try {
            if (parent != null) {
                if (parent instanceof ConstraintViolationException) {
                    Throwable batchUpdate = parent.getCause();
                    if (batchUpdate instanceof BatchUpdateException) {
                        message = batchUpdate.getMessage();
                    } else if (batchUpdate instanceof SQLException) {
                        message = batchUpdate.getMessage();
                    } else {
                        message = parent.getMessage();
                    }
                } else {
                    message = parent.getMessage();
                }
            } else if (exception instanceof org.hibernate.StaleObjectStateException) {
                message = "database.constraint.exceptions.staleObjectException";
            }
        } catch (Exception e) {
        }
        return replaceExceptionMessage(message);
    }

    private static String replaceExceptionMessage(String message) {
        String replace = message;
        try {
            for (String key : constraintExceptions.keySet()) {
                if (message.contains(key)) {
                    replace = constraintExceptions.get(key);
                    break;
                }
            }
        } catch (Exception e) {

        }
        return replace;
    }

    public static void clearMessages() {
        Iterator<FacesMessage> msgIterator = FacesContext.getCurrentInstance().getMessages();
        while (msgIterator.hasNext()) {
            msgIterator.next();
            msgIterator.remove();
        }
    }

}
