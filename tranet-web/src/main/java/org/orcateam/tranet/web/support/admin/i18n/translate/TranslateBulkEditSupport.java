package org.orcateam.tranet.web.support.admin.i18n.translate;

import org.orcateam.tranet.core.dao.parameter.KeywordParameter;
import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.model.i18n.Translate;
import org.orcateam.tranet.core.service.KeywordService;
import org.orcateam.tranet.core.service.LanguageService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class TranslateBulkEditSupport extends BaseListSupport<Keyword, Integer> {

    private Language language = null;
    private String langCode = null;
    private KeywordParameter params = new KeywordParameter();

    @Autowired
    transient KeywordService entityService;

    @Autowired
    transient LanguageService languageService;

    @Override
    public void reset() {
        super.reset();
    }

    @Override
    @Secured("admin.i18n.translate.edit")
    protected void initResultList() {
        setMaxResult(50);
        Pagination<Keyword> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
        initTranslateEntities();
    }

    private void initTranslateEntities() {
        if (language == null) {
            language = languageService.getByCode(langCode);
            if (language == null) {
                WebUtil.addErrorMessage("admin.i18n.translate.validation.languageNotFound");
                return;
            }
        }
        for (Keyword keyword : getResultList()) {
            Translate translate = keyword.getTranslateMap().get(langCode);
            if (translate == null) {
                translate = new Translate();
                translate.setLanguage(language);
                translate.setKeyword(keyword);
                translate.setLangCode(langCode);
                keyword.getTranslateMap().put(language.getCode().getValue(), translate);
            }
        }
    }

    public void bulkUpdate() {
        try {
            entityService.update(getResultList());
            WebUtil.addInfoMessage("global.messages.update.success");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
        }
    }


    @Override
    public String navToView(Keyword entity) {
        return null;
    }

    @Override
    public String navToCreate() {
        return null;
    }

    @Override
    protected String getViewId() {
        return "translateBulkEdit";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public KeywordParameter getParams() {
        return params;
    }

    public void setParams(KeywordParameter params) {
        this.params = params;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
