package org.orcateam.tranet.web.support.admin.agency.agency;

import org.orcateam.tranet.core.enumeration.PaymentTypeEnum;
import org.orcateam.tranet.core.enumeration.UserTypeEnum;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.model.balance.Balance;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.service.*;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AgencyEditSupport extends BaseEditSupport<Agency, Integer> {

    private List<CurrencyType> currencyTypeList = new ArrayList<CurrencyType>();
    private List<DiscountGroup> discountGroupList = new ArrayList<DiscountGroup>();

    private List<Balance> balanceList = new ArrayList<Balance>();
    private List<AgencyUser> agencyUserList = new ArrayList<AgencyUser>();

    @Autowired
    transient AgencyService entityService;

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    @Autowired
    transient DiscountGroupService discountGroupService;

    @Autowired
    transient UserService userService;

    @Autowired
    transient AgencyUserService agencyUserService;

    @Override
    @Secured("admin.agency.agency.create")
    public void initCreate() throws Exception {
        Agency entity = new Agency();
        setInstance(entity);

        initCurrenctTypeList();
        initDiscountGroupList();
    }

    @Override
    @Secured("admin.agency.agency.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Agency entity = entityService.findEagerlyById(instanceId);
        if (entity == null) {
            throw new NotFoundException();
        }
        setInstance(entity);
        initBalanceList();
        initAgencyUserList();
        initCurrenctTypeList();
        initDiscountGroupList();
    }

    private void initBalanceList() {
        balanceList.clear();
        if (instance.getBalanceSet() != null) {
            balanceList = new ArrayList<Balance>(instance.getBalanceSet());
        }
    }

    private void initAgencyUserList() {
        agencyUserList.clear();
        if (instanceId != null) {
            agencyUserList = agencyUserService.getAllByAgencyId(instanceId);
        }
    }

    private void initCurrenctTypeList() {
        currencyTypeList.clear();
        currencyTypeList = currencyTypeService.getAll();
    }

    private void initDiscountGroupList() {
        discountGroupList.clear();
        discountGroupList = discountGroupService.getAll();
    }

    @Override
    public String persist() {
        try {
            Agency persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return instanceId != null;
    }

    @Override
    protected String getViewId() {
        return "agencyEdit";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("agencyList");
        } else {
            NavRule rule = new NavRule("agencyView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("agencyView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToList() {
        NavRule rule = new NavRule("agencyList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Secured("admin.agency.agency.addBalance")
    public String navToBalanceAdd() {
        NavRule rule = new NavRule("balanceEdit");
        rule.addParam("agencyId", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Secured("admin.agency.agency.addUser")
    public String navToUserAdd() {
        NavRule rule = new NavRule("userWizard");
        rule.addParam("type", UserTypeEnum.AGENCY_USER.getLevel().toString());
        rule.addParam("innerId", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    public PaymentTypeEnum[] getPaymentTypeList() {
        return PaymentTypeEnum.values();
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }

    public List<DiscountGroup> getDiscountGroupList() {
        return discountGroupList;
    }

    public void setDiscountGroupList(List<DiscountGroup> discountGroupList) {
        this.discountGroupList = discountGroupList;
    }

    public List<Balance> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<Balance> balanceList) {
        this.balanceList = balanceList;
    }

    public List<AgencyUser> getAgencyUserList() {
        return agencyUserList;
    }

    public void setAgencyUserList(List<AgencyUser> agencyUserList) {
        this.agencyUserList = agencyUserList;
    }
}
