package org.orcateam.tranet.web.support.admin.location.city;

import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.service.CityService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CityViewSupport extends BaseViewSupport<City, Integer> {

    @Autowired
    transient CityService entityService;

    @Override
    @Secured("admin.location.city.view")
    public void initView(Integer instanceId) throws Exception {
        City result = entityService.getWithCountry(instanceId);
        setInstance(result);
        if (result == null) {
            throw new NotFoundException();
        }
    }

    @Secured("admin.location.city.edit")
    public String navToEdit() {
        NavRule rule = new NavRule("cityEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "cityView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("cityList");
    }

}
