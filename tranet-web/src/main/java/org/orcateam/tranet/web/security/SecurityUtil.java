package org.orcateam.tranet.web.security;

import org.orcateam.tranet.core.util.SpringContext;
import org.orcateam.tranet.web.exception.NonSecuredException;

public class SecurityUtil {
    public static void checkSecured(Class clazz, String methodName, Class<?>... parameters) throws NonSecuredException {
        Authentication authentication = SpringContext.getContext().getBean(Authentication.class);
        String[] privileges = getPrivileges(clazz, methodName, parameters);
        if (privileges != null) {
            for (String privilege : privileges) {
                if (!authentication.hasRole(privilege)) {
                    throw new NonSecuredException();
                }
            }
        }
    }

    private static String[] getPrivileges(Class clazz, String methodName, Class<?>... parameters) throws NonSecuredException {
        try {
            Secured secured = null;
            if (parameters == null || parameters.length == 0) {
                secured = clazz.getDeclaredMethod(methodName).getAnnotation(Secured.class);
            } else {
                secured = clazz.getDeclaredMethod(methodName, parameters).getAnnotation(Secured.class);
            }
            if (secured != null) {
                return secured.value();
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new NonSecuredException();
        }
    }
}
