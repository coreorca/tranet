package org.orcateam.tranet.web.support.admin.location.city;

import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.service.CityService;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class CityEditSupport extends BaseEditSupport<City, Integer> {

    private List<Country> countryList = null;

    @Autowired
    transient CityService entityService;

    @Autowired
    transient CountryService countryService;

    @Override
    @Secured("admin.location.city.create")
    public void initCreate() throws Exception {
        City entity = new City();
        setInstance(entity);
        initCountryList();
    }

    @Override
    @Secured("admin.location.city.edit")
    public void initUpdate(Integer instanceId) throws Exception {
        City entity = entityService.getWithCountry(instanceId);
        setInstance(entity);
        if (entity == null) {
            throw new NotFoundException();
        }
        initCountryList();
    }

    private void initCountryList() {
        countryList = countryService.getAll();
    }

    @Override
    public String persist() {
        try {
            City persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToCityView(persisted.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToCityView(instance.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return nav.cleanRedirect(new NavRule("cityList"), navId.getValue());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    public String navToCityView(Integer cityId) {
        NavRule rule = new NavRule("cityView");
        rule.addParam("id", cityId.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "cityEdit}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("cityList");
        } else {
            NavRule rule = new NavRule("cityView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }
}
