package org.orcateam.tranet.web.components;

import javax.faces.application.FacesMessage;
import java.io.Serializable;

public class GlobalMessage implements Serializable {

    FacesMessage.Severity severity;
    private String text;

    public GlobalMessage(FacesMessage.Severity severity, String text) {
        this.severity = severity;
        this.text = text;
    }

    public FacesMessage.Severity getSeverity() {
        return severity;
    }

    public void setSeverity(FacesMessage.Severity severity) {
        this.severity = severity;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMessageStyle() {
        if (FacesMessage.SEVERITY_INFO.equals(severity)) {
            return "alert alert-success";
        } else if (FacesMessage.SEVERITY_WARN.equals(severity)) {
            return "alert alert-warning";
        } else if (FacesMessage.SEVERITY_ERROR.equals(severity)) {
            return "alert alert-danger";
        } else if (FacesMessage.SEVERITY_FATAL.equals(severity)) {
            return "alert alert-danger";
        } else {
            return "alert alert-info";
        }
    }
}
