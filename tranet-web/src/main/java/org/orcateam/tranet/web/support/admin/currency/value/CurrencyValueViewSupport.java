package org.orcateam.tranet.web.support.admin.currency.value;

import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.service.CurrencyValueService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CurrencyValueViewSupport extends BaseViewSupport<CurrencyValue, Integer> {

    @Autowired
    transient CurrencyValueService entityService;

    @Override
    @Secured("admin.currency.value.view")
    public void initView(Integer instanceId) {
        CurrencyValue result = entityService.getWithTypes(instanceId);
        setInstance(result);
    }

    @Secured("admin.currency.value.update")
    public String navToEdit() {
        NavRule rule = new NavRule("currencyValueEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "currencyValueView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("currencyValueList");
    }

}
