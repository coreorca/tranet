package org.orcateam.tranet.web.converter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

@FacesConverter(value = "moneyConverter")
public class MoneyConverter implements Converter {

    Logger logger = Logger.getLogger(MoneyConverter.class);

    private static DecimalFormat decimalFormat;

    static {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(new Locale("tr", "TR"));
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        decimalFormat = new DecimalFormat("#,###,###,##0.00", symbols);
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            Double value = null;
            if (!StringUtils.isEmpty(s)) {
                value = parseToDouble(s);
            }
            return value;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ConverterException(e);
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        try {
            String formatted = formatToString(o);
            return formatted;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ConverterException(e);
        }

    }

    public static String formatToString(Object value) throws Exception {
        if (value instanceof String) {
            return value.toString();
        }
        return decimalFormat.format(value);
    }

    public static Double parseToDouble(String value) throws Exception {
        return decimalFormat.parse(value).doubleValue();
    }

}
