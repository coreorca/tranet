package org.orcateam.tranet.web.support.front.auth;

import org.orcateam.tranet.core.model.auth.CustomerUser;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BaseEditSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class FrontRegisterEditSupport extends BaseEditSupport<CustomerUser, Integer> {

    @Override
    public void initCreate() throws Exception {

    }

    @Override
    public void initUpdate(Integer instanceId) throws Exception {

    }

    @Override
    public String persist() {
        return null;
    }

    @Override
    public String update() {
        return null;
    }

    @Override
    public String remove() {
        return null;
    }

    @Override
    public boolean isManaged() {
        return false;
    }

    @Override
    protected String getViewId() {
        return null;
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }
}
