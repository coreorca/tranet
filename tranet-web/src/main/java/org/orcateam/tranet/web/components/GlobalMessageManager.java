package org.orcateam.tranet.web.components;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@ManagedBean
@SessionScoped
public class GlobalMessageManager implements Serializable {

    private FacesMessage.Severity maximumSeverity = FacesMessage.SEVERITY_INFO;

    private LinkedList<GlobalMessage> messages = new LinkedList<GlobalMessage>();


    public void push(GlobalMessage message) {
        if (message.getSeverity().getOrdinal() > maximumSeverity.getOrdinal()) {
            maximumSeverity = message.severity;
        }
        messages.push(message);
    }

    public GlobalMessage pop() {
        GlobalMessage popped = messages.pop();
        resetMaxSeverity();
        return popped;
    }

    public List<GlobalMessage> toList() {
        List<GlobalMessage> list = new ArrayList<GlobalMessage>(messages);
        messages.clear();
        return list;
    }

    public boolean hashNext() {
        return !messages.isEmpty();
    }

    private void resetMaxSeverity() {
        if (!messages.isEmpty()) {
            for (GlobalMessage message : messages) {
                if (message.getSeverity().getOrdinal() > maximumSeverity.getOrdinal()) {
                    maximumSeverity = message.severity;
                }
            }
        } else {
            maximumSeverity = FacesMessage.SEVERITY_INFO;
        }
    }

    public FacesMessage.Severity getMaximumSeverity() {
        FacesMessage.Severity facesMaxSeverity = FacesContext.getCurrentInstance().getMaximumSeverity();
        if (facesMaxSeverity != null && facesMaxSeverity.getOrdinal() > maximumSeverity.getOrdinal()) {
            return facesMaxSeverity;
        }
        return maximumSeverity;
    }

}
