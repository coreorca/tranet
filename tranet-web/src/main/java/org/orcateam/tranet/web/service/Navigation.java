package org.orcateam.tranet.web.service;


import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.orcateam.tranet.web.exception.NavIdExpiredException;
import org.orcateam.tranet.web.navigation.NavId;
import org.orcateam.tranet.web.navigation.NavParam;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.navigation.NavUtil;
import org.orcateam.tranet.web.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Service
@Scope(value = "session")
public class Navigation implements Serializable {


    private Long nextNavId = 1L;

    @Autowired
    ViewService viewService;

    Logger logger = Logger.getLogger(getClass());

    Map<Long, NavId> history = new HashMap<Long, NavId>();

    public String getNavURL(NavRule rule, Long navId) {
        String path = viewService.getView(rule.getViewId());
        if (StringUtils.isNotBlank(path)) {
            path += "?faces-redirect=true";
            for (NavParam navParam : rule.getParamList()) {
                path = path + "&amp;" + navParam.toString();
            }
            path = path + "&amp;" + NavUtil.NAV_ID + "=" + navId;
        }
        return path;
    }

    /**
     * return to previous view and remove history.
     *
     * @return
     */
    public String back(NavId navId) throws NavIdExpiredException {
        NavRule navRule = navId.pop();
        if (navRule == null) {
            return null;
        }
        logger.debug("navigating to " + navRule.getViewId());
        return getNavURL(navRule, navId.getValue());
    }

    /**
     * redirect to view
     *
     * @param rule
     * @return
     */
    public String redirect(NavRule rule, Long navId) {
        return getNavURL(rule, navId);
    }


    /**
     * redirect to view and clear history
     *
     * @param rule
     * @param navId
     * @return
     */
    public String cleanRedirect(NavRule rule, Long navId) {
        history.get(navId).clean();
        return getNavURL(rule, navId);
    }

    public NavId getActiveNavId() throws NavIdExpiredException {
        Long navIdVal = getNavIdVal();
        NavId navId = history.get(navIdVal);
        if (navId == null) {
            navId = new NavId(navIdVal);
            history.put(navIdVal, navId);
        }
        return navId;
    }

    private synchronized Long getNavIdVal() throws NavIdExpiredException {
        Long navId = HttpUtil.getNavId();
        if (navId == null) {
            navId = nextNavId++;
            if (nextNavId >= Long.MAX_VALUE) {
                nextNavId = 1L;
            }
        } else {
            if (!history.containsKey(navId)) {
                throw new NavIdExpiredException();
            }
        }
        return navId;
    }

    public NavId push(NavRule navRule) throws NavIdExpiredException {
        NavId navId = getActiveNavId();
        navId.push(navRule);
        return navId;
    }

}

