package org.orcateam.tranet.web.support.front.checkout;

import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BasePageSupport;
import org.orcateam.tranet.web.support.front.cart.FrontEndCart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class FrontCheckoutPageSupport extends BasePageSupport {

    @Autowired
    transient FrontEndCart cart;

    @Override
    public void initPage() {
        if (cart.isEmpty()) {

        }
    }

    @Override
    protected String getViewId() {
        return null;
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }
}
