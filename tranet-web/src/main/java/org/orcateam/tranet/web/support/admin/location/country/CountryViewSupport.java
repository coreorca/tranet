package org.orcateam.tranet.web.support.admin.location.country;

import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class CountryViewSupport extends BaseViewSupport<Country, Integer> {

    @Autowired
    transient CountryService countryService;

    @Override
    @Secured("admin.location.country.view")
    public void initView(Integer instanceId) throws Exception {
        Country country = countryService.get(instanceId);
        setInstance(country);
    }

    @Secured("admin.location.country.update")
    public String navToEdit() {
        NavRule rule = new NavRule("countryEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "countryView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("countryList");
    }
}
