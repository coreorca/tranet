package org.orcateam.tranet.web.support.agency.transfer.reservation;

import org.orcateam.tranet.core.dao.parameter.AgencyReservationParameter;
import org.orcateam.tranet.core.enumeration.ReservationStatusEnum;
import org.orcateam.tranet.core.enumeration.RouteTypeEnum;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.service.PointService;
import org.orcateam.tranet.core.service.ReservationService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Arrays;
import java.util.List;

@ManagedBean
@ViewScoped
public class AgencyReservationListSupport extends BaseListSupport<Reservation, Integer> {

    AgencyReservationParameter params = new AgencyReservationParameter();

    @Autowired
    transient ReservationService reservationService;

    @Autowired
    transient PointService pointService;

    @Override
    @Secured("agency.transfer.reservation.list")
    protected void initResultList() {
        Pagination<Reservation> pagination = reservationService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("agency.transfer.reservation.view")
    public String navToView(Reservation reservation) {
        NavRule rule = new NavRule("agencyReservationView", "id", reservation.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("agency.transfer.reservation.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("agencyReservationWizard"), navId.getValue());
    }

    @Override
    public void reset() {
        params = new AgencyReservationParameter();
        super.reset();
    }

    @Override
    protected String getViewId() {
        return "agencyReservationList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public AgencyReservationParameter getParams() {
        return params;
    }

    public void setParams(AgencyReservationParameter params) {
        this.params = params;
    }

    public List<RouteTypeEnum> getRouteTypeList() {
        return Arrays.asList(RouteTypeEnum.values());
    }

    public List<Point> getPointList() {
        return pointService.getAll();
    }

    public List<ReservationStatusEnum> getReservationStatusList() {
        return Arrays.asList(ReservationStatusEnum.values());
    }

}
