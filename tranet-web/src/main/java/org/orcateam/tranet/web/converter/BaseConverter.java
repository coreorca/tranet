package org.orcateam.tranet.web.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public abstract class BaseConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,String value) {
		List<?> selections = getSelectionList(component);
		Object object = null;
		if(selections != null) {
			for(Object selection : selections) {
				String entityId = getEntityIdValue(selection);
				if(value.equals(entityId)) {
					object = selection; break;
				}
			}
		}
		return object; 
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return getEntityIdValue(value);
	}
	
	private List<?> getSelectionList(UIComponent component) {
		UISelectItems items = null;
		for(UIComponent child : component.getChildren()) {
			if(child instanceof UISelectItems) {
				items = (UISelectItems)child; break;
			}
		}
		List<?> list = (List<?>)items.getAttributes().get("value");
		return list;
	}
	
	public abstract String getEntityIdValue(Object value);
}
