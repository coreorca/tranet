package org.orcateam.tranet.web.support.admin.i18n.language;

import org.orcateam.tranet.core.dao.parameter.LanguageParameter;
import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.service.LanguageService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class LanguageListSupport extends BaseListSupport<Language, Integer> {

    LanguageParameter params = new LanguageParameter();

    @Autowired
    transient LanguageService entityService;

    @Override
    public void reset() {
        params = new LanguageParameter();
        super.reset();
    }

    @Override
    @Secured("admin.i18n.language.list")
    protected void initResultList() {
        Pagination<Language> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.i18n.language.view")
    public String navToView(Language entity) {
        NavRule rule = new NavRule("languageView", "id", entity.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.i18n.language.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("languageEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "languageList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public LanguageParameter getParams() {
        return params;
    }

    public void setParams(LanguageParameter params) {
        this.params = params;
    }

}
