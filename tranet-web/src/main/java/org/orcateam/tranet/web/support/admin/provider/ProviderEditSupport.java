package org.orcateam.tranet.web.support.admin.provider;

import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.provider.Provider;
import org.orcateam.tranet.core.service.CityService;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.core.service.ProviderService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class ProviderEditSupport extends BaseEditSupport<Provider, Integer> {

    private List<Country> countryList = new ArrayList<Country>();
    private List<City> cityList = new ArrayList<City>();

    @Autowired
    transient ProviderService providerService;

    @Autowired
    transient CountryService countryService;

    @Autowired
    transient CityService cityService;

    @Override
    @Secured("admin.provider.create")
    public void initCreate() throws Exception {
        Provider provider = new Provider();
        setInstance(provider);

        initCountryList();
    }

    @Override
    @Secured("admin.provider.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Provider provider = providerService.findEagerlyById(instanceId);
        setInstance(provider);

        initCountryList();
        initCityList();
    }

    private void initCountryList() {
        countryList = countryService.getAll();
    }

    private void initCityList() {
        cityList.clear();
        if (instance.getCountry() != null) {
            List<City> result = cityService.getListByCountryId(instance.getCountry().getId());
            cityList.addAll(result);
        }
    }

    @Override
    public String persist() {
        try {
            Provider pesisted = providerService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(pesisted.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }


    @Override
    public String update() {
        try {
            providerService.update(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            providerService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "providerEdit";
    }


    public String navToList() {
        NavRule rule = new NavRule("providerList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("providerView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("providerList");
        } else {
            NavRule rule = new NavRule("providerView");
            rule.addParam("providerId", instanceId.toString());
            return rule;
        }
    }

    public void countryAjaxChangeListener() {
        instance.setCity(null);
        initCityList();
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }
}
