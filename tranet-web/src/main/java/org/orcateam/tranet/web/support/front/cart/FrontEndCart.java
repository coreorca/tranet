package org.orcateam.tranet.web.support.front.cart;

import org.orcateam.tranet.core.model.embeddable.Money;
import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.util.SessionUtil;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.service.ViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Scope("session")
public class FrontEndCart {

    @Autowired
    SessionUtil sessionUtil;

    @Autowired
    ViewService viewService;


    private List<Reservation> reservationList = new ArrayList<Reservation>();

    public void add(Reservation reservation) {
        reservationList.add(reservation);
    }

    public void remove(Reservation reservation) {
        reservationList.remove(reservation);
    }

    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public Integer getCount() {
        return reservationList.size();
    }

    public Money getTotal() {
        Money money = new Money();
        money.setCurrencyType(sessionUtil.getCurrency());
        for (Reservation reservation : reservationList) {
            Double price = reservation.getPrice().getConverted().getValue();
            money.add(price);
        }
        return money;
    }

    public Integer getTotalPax() {
        Integer totalPax = 0;
        for (Reservation reservation : reservationList) {
            totalPax += reservation.getPax();
        }
        return totalPax;
    }

    public boolean isEmpty() {
        return reservationList.isEmpty();
    }



    
}
