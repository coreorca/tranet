package org.orcateam.tranet.web.support.admin.location.point;

import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.service.CityService;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.core.service.PointService;
import org.orcateam.tranet.core.util.PointUtil;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ManagedBean
@ViewScoped
public class PointEditSupport extends BaseEditSupport<Point, Integer> {

    private Country country = null;
    private String coordianate = null;

    private List<Country> countryList = null;
    private List<City> cityList = null;
    private List<PointTypeEnum> pointTypeEnumList = null;

    @Autowired
    transient PointService entityService;

    @Autowired
    transient CityService cityService;

    @Autowired
    transient CountryService countryService;

    @Override
    @Secured("admin.location.point.create")
    public void initCreate() {
        Point entity = new Point();
        setInstance(entity);
        initCountryList();
        initCityList();
        initPointTypeEnumList();
    }

    @Override
    @Secured("admin.location.point.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Point entity = entityService.getWithCity(instanceId);
        setInstance(entity);
        country = entity.getCity().getCountry();
        initCountryList();
        initCityList();
        initPointTypeEnumList();
    }

    private void initCountryList() {
        countryList = countryService.getAll();
    }

    private void initCityList() {
        cityList = null;
        if (country != null) {
            cityList = cityService.getListByCountryId(country.getId());
        }
    }

    private void initPointTypeEnumList() {
        pointTypeEnumList = new ArrayList<PointTypeEnum>(Arrays.asList(PointTypeEnum.values()));
    }

    @Override
    public String persist() {
        try {
            Point pesisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToPointView(pesisted.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToPointView(instance.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return nav.cleanRedirect(new NavRule("pointList"), navId.getValue());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    public void countryAjaxChangeListener() {
        initCityList();
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    public String navToPointView(Integer PointId) {
        NavRule rule = new NavRule("pointView");
        rule.addParam("id", PointId.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "pointEdit}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("pointList");
        } else {
            NavRule rule = new NavRule("pointView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public void setLatLng(String latLng) {
        instance.setLatLng(PointUtil.parseLatLng(latLng));
    }

    public String toLatLng() {
        return PointUtil.parseLatLng(getInstance().getLatLng());
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<PointTypeEnum> getPointTypeEnumList() {
        return pointTypeEnumList;
    }

    public void setPointTypeEnumList(List<PointTypeEnum> pointTypeEnumList) {
        this.pointTypeEnumList = pointTypeEnumList;
    }

    public String getCoordianate() {
        return coordianate;
    }

    public void setCoordianate(String coordianate) {
        this.coordianate = coordianate;
    }
}
