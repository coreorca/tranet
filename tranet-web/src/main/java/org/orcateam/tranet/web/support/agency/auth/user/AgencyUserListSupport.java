package org.orcateam.tranet.web.support.agency.auth.user;

import org.orcateam.tranet.core.dao.parameter.AgencyUserParameter;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.service.AgencyUserService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class AgencyUserListSupport extends BaseListSupport<AgencyUser, Integer> {

    private AgencyUserParameter agencyUserParameter = new AgencyUserParameter();

    @Autowired
    transient AgencyUserService agencyUserService;

    @Override
    @Secured("agency.auth.user.list")
    protected void initResultList() {
        try {
            Pagination<AgencyUser> pagination = agencyUserService.findWithPagination(getPager(), agencyUserParameter);
            applyResult(pagination);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected String getViewId() {
        return "agencyUserList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    @Override
    @Secured("agency.auth.user.create")
    public String navToCreate() {
        NavRule navToAdd = new NavRule("agencyUserEdit");
        return nav.redirect(navToAdd, getNavId());
    }


    @Override
    @Secured("agency.auth.user.view")
    public String navToView(AgencyUser agencyUser) {
        NavRule navToView = new NavRule("agencyUserView");
        navToView.addParam("instanceId", agencyUser.getId().toString());
        return nav.redirect(navToView, getNavId());
    }

    public AgencyUserParameter getAgencyUserParameter() {
        return agencyUserParameter;
    }

    public void setAgencyUserParameter(AgencyUserParameter agencyUserParameter) {
        this.agencyUserParameter = agencyUserParameter;
    }
}
