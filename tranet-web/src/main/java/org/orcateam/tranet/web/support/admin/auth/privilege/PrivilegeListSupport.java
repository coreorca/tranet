package org.orcateam.tranet.web.support.admin.auth.privilege;

import org.orcateam.tranet.core.dao.parameter.PrivilegeParameter;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.service.PrivilegeService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class PrivilegeListSupport extends BaseListSupport<Privilege, Integer> {

    private PrivilegeParameter privilegeParameter = new PrivilegeParameter();

    @Autowired
    transient PrivilegeService privilegeService;

    @Override
    protected String getViewId() {
        return "privilegeList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    @Override
    protected void initResultList() {
        try {
            Pagination<Privilege> pagination = privilegeService.findWithPagination(getPager(), privilegeParameter);
            applyResult(pagination);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public String navToView(Privilege instance) {
        return null;
    }

    @Override
    public void reset() {
        privilegeParameter = new PrivilegeParameter();
        super.reset();
    }

    //<editor-fold desc="navigation">
    public String navToCreate() {
        NavRule navToPrivilegeEdit = new NavRule("privilegeEdit");
        return nav.redirect(navToPrivilegeEdit, getNavId());
    }


    public PrivilegeParameter getPrivilegeParameter() {
        return privilegeParameter;
    }

    public void setPrivilegeParameter(PrivilegeParameter privilegeParameter) {
        this.privilegeParameter = privilegeParameter;
    }
}
