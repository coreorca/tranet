package org.orcateam.tranet.web.support.admin.location.point;

import org.orcateam.tranet.core.dao.parameter.PointParameter;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.service.PointService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.orcateam.tranet.web.navigation.NavRule;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class PointListSupport extends BaseListSupport<Point, Integer> {

    PointParameter params = new PointParameter();

    @Autowired
    transient PointService entityService;

    @Override
    public void reset() {
        params = new PointParameter();
        super.reset();
    }

    @Override
    @Secured("admin.location.point.list")
    protected void initResultList() {
        Pagination<Point> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.location.point.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("pointEdit"), navId.getValue());
    }

    @Override
    @Secured("admin.location.point.view")
    public String navToView(Point Point) {
        NavRule rule = new NavRule("pointView", "id", Point.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "pointList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public PointParameter getParams() {
        return params;
    }

    public void setParams(PointParameter params) {
        this.params = params;
    }

}
