package org.orcateam.tranet.web.converter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


@FacesConverter(value = "dateConverter")
public class DateConverter implements Converter {

    Logger logger = Logger.getLogger(DateConverter.class);

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            if (StringUtils.isEmpty(s)) {
                return null;
            }
            s = s.replace(".", "/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("tr", "TR"));

            if (!isRequired(uiComponent) && StringUtils.isBlank(s)) {
                return null;
            }
            Date date = sdf.parse(s);
            return date;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ConverterException(e);
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("tr", "TR"));
        return sdf.format((Date) o);
    }

    private boolean isRequired(UIComponent component) {
        boolean required = false;
        Object value = component.getAttributes().get("required");
        if (value != null) {
            required = new Boolean(value.toString());
        }
        return required;
    }


}
