package org.orcateam.tranet.web.support;

public class Ext<T> implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2201436987268496216L;
    private T instance;
    private T clone;
	private boolean active = false;
	private boolean newRecord = false;
	private boolean editing = false;
	private boolean selected = false;
	private boolean deleted = false;
	private boolean updated = false;
	private boolean marked = false;
    private Integer index;

	public Ext(T instance) {
		this.instance = instance;
	}

	public Ext(T instance, T clone) {
		this.instance = instance;
		this.clone = clone;
	}
	
	public Ext(T instance, boolean selected) {
		super();
		this.instance = instance;
		this.selected = selected;
	}

    public T getInstance() {
		return instance;
	}

	public void setInstance(T instance) {
		this.instance = instance;
	}

	public T getClone() {
		return clone;
	}

	public void setClone(T clone) {
		this.clone = clone;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isNewRecord() {
		return newRecord;
	}

	public void setNewRecord(boolean newRecord) {
		this.newRecord = newRecord;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		onSelected();
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isUpdated() {
		return updated;
	}

	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}
	
	public void onSelected(){
	}

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
