package org.orcateam.tranet.web.support.admin.reservation;

import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ReservationListSupport extends BaseListSupport<Reservation, Integer> {

    @Override
    protected void initResultList() {

    }

    @Override
    @Secured("admin.auth.reservation.create")
    public String navToCreate() {
        NavRule navToAdd = new NavRule("reservationEdit");
        return nav.redirect(navToAdd, getNavId());
    }

    @Override
    @Secured("admin.auth.reservation.view")
    public String navToView(Reservation reservation) {
        NavRule navToView = new NavRule("reservationView");
        navToView.addParam("instanceId", reservation.getId().toString());
        return nav.redirect(navToView, getNavId());
    }

    @Override
    protected String getViewId() {
        return "reservationView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }
}
