package org.orcateam.tranet.web.support;

public abstract class BasePageSupport extends BaseSupport<String, Integer> {

    public abstract void initPage();

    @Override
    protected void callInitFunctions() throws Exception {
        checkSecured("initPage");
        logger.debug("calling initPage function from support");
        initPage();
    }

}
