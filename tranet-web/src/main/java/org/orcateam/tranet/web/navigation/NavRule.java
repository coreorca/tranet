package org.orcateam.tranet.web.navigation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NavRule implements Serializable {

    private String viewId = null;

    private String selectedTab;

    private List<NavParam> paramList = new ArrayList<NavParam>();


    public NavRule(String viewId) {
        this.viewId = viewId;
    }

    public NavRule(String viewId, String param, String value) {
        this.viewId = viewId;
        addParam(param, value);
    }

    public NavRule(String viewId, List<NavParam> paramList) {
        this.viewId = viewId;
        this.paramList.addAll(paramList);
    }

    public void addParam(String param, String value) {
        paramList.add(new NavParam(param, value));
    }

    public String getViewId() {
        return viewId;
    }

    public List<NavParam> getParamList() {
        return paramList;
    }

    public String getHash() {
        return NavUtil.getHash(viewId, paramList);
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

}
