package org.orcateam.tranet.web.support.admin.i18n.language;

import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.service.LanguageService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.navigation.NavParam;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class LanguageEditSupport extends BaseEditSupport<Language, Integer> {

    @Autowired
    transient LanguageService entityService;

    @Override
    @Secured("admin.i18n.language.create")
    public void initCreate() {
        Language entity = new Language();
        setInstance(entity);
    }

    @Override
    @Secured("admin.i18n.language.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Language entity = entityService.get(instanceId);
        setInstance(entity);
        if (entity == null) {
            throw new NotFoundException();
        }
    }

    @Override
    public String persist() {
        try {
            Language persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "languageEdit}";
    }

    public String navToList() {
        NavRule rule = new NavRule("languageList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("languageView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("languageList");
        } else {
            NavRule rule = new NavRule("languageView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }


}
