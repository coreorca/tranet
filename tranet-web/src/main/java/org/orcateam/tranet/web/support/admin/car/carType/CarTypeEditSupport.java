package org.orcateam.tranet.web.support.admin.car.carType;

import org.orcateam.tranet.core.enumeration.CarShapeEnum;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.service.CarTypeService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CarTypeEditSupport extends BaseEditSupport<CarType, Integer> {

    @Autowired
    transient CarTypeService carTypeService;

    @Override
    @Secured("admin.car.carType.create")
    public void initCreate() throws Exception {
        CarType carType = new CarType();
        setInstance(carType);
    }

    @Override
    @Secured("admin.car.carType.update")
    public void initUpdate(Integer instanceId) throws Exception {
        CarType carType = carTypeService.get(instanceId);
        setInstance(carType);
    }

    @Override
    public String persist() {
        try {
            CarType persisted = carTypeService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }


    @Override
    public String update() {
        try {
            carTypeService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            carTypeService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "carTypeEdit";
    }


    public String navToList() {
        NavRule rule = new NavRule("carTypeList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("carTypeView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("carTypeList");
        } else {
            NavRule rule = new NavRule("carTypeView");
            rule.addParam("carTypeId", instanceId.toString());
            return rule;
        }
    }

    public CarShapeEnum[] getCarShapes() {
        return CarShapeEnum.values();
    }
}
