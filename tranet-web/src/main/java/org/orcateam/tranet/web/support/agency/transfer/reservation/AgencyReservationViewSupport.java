package org.orcateam.tranet.web.support.agency.transfer.reservation;

import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.service.ReservationService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class AgencyReservationViewSupport extends BaseViewSupport<Reservation, Integer> {

    @Autowired
    transient ReservationService reservationService;

    @Override
    @Secured("agency.transfer.reservation.view")
    public void initView(Integer instanceId) throws Exception {
        Reservation reservation = reservationService.findWithId(instanceId);
        setInstance(reservation);
    }

    @Override
    @Secured("agency.transfer.reservation.update")
    public String navToEdit() {
        NavRule rule = new NavRule("agencyReservationWizard");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "agencyReservationView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("agencyReservationList");
    }
}
