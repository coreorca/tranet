package org.orcateam.tranet.web.support.admin.agency.discountGroup;

import org.orcateam.tranet.core.dao.parameter.DiscountGroupParameter;
import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.service.DiscountGroupService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class DiscountGroupListSupport extends BaseListSupport<DiscountGroup, Integer> {

    private DiscountGroupParameter params = new DiscountGroupParameter();

    @Autowired
    transient DiscountGroupService entityService;

    @Override
    protected void initResultList() {
        Pagination<DiscountGroup> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    public void reset() {
        params = new DiscountGroupParameter();
        super.reset();
    }

    @Override
    @Secured("admin.agency.discountGroup.view")
    public String navToView(DiscountGroup instance) {
        NavRule rule = new NavRule("discountGroupView", "id", instance.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.agency.discountGroup.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("discountGroupEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "discountGroupList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public DiscountGroupParameter getParams() {
        return params;
    }

    public void setParams(DiscountGroupParameter params) {
        this.params = params;
    }
}
