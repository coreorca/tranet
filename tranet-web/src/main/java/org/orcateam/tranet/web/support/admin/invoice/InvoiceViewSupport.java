package org.orcateam.tranet.web.support.admin.invoice;

import org.orcateam.tranet.core.enumeration.InvoiceTypeEnum;
import org.orcateam.tranet.core.model.invoice.Invoice;
import org.orcateam.tranet.core.service.InvoiceService;
import org.orcateam.tranet.core.util.InvoiceUtil;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class InvoiceViewSupport extends BaseViewSupport<Invoice, Integer> {

    private InvoiceTypeEnum invoiceType = null;

    @Autowired
    transient InvoiceService entityService;

    @Override
    @Secured("admin.invoice.view")
    public void initView(Integer instanceId) throws Exception {
        Invoice entity = entityService.findEagerlyById(instanceId);
        if (entity == null) {
            throw new NotFoundException();
        }
        setInstance(entity);

        invoiceType = InvoiceUtil.getInvoiceType(entity);
    }

    @Override
    @Secured("admin.invoice.edit")
    public String navToEdit() {
        NavRule rule = new NavRule("invoiceWizard");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "invoiceView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("invoiceList");
    }

    public boolean isAgency() {
        return invoiceType.equals(InvoiceTypeEnum.AGENCY_INVOICE);
    }

    public boolean isIndividual() {
        return invoiceType.equals(InvoiceTypeEnum.INDIVIDUAL_INVOCE);
    }
}
