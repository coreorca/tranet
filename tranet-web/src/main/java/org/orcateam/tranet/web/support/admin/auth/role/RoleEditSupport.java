package org.orcateam.tranet.web.support.admin.auth.role;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.PrivilegeService;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.support.Ext;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Taken from hys
 */
@ManagedBean
@ViewScoped
public class RoleEditSupport extends BaseEditSupport<Role, Integer> {

    private List<Privilege> privileges = null;
    private List<Ext<Privilege>> privilegeList = null;
    private List<Ext<Privilege>> filteredPrivilegeList = null;
    private boolean selectAll = false;
    private String filterPrivilegeName = null;

    private String name = null;
    private String excludeName = null;
    private List<Role> roleList = null;
    private Role role = null;

    @Autowired
    transient RoleService roleService;

    @Autowired
    transient PrivilegeService privilegeService;

    @Override
    @Secured("admin.auth.role.create")
    public void initCreate() throws Exception {
        Role role = new Role();
        setInstance(role);

        privileges = new ArrayList<Privilege>();
        initPrivilegeList();
        initRoleList();
    }

    @Override
    @Secured("admin.auth.role.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Role role = roleService.findEagerlyById(instanceId);
        if (role == null) {
            throw new NotFoundException();
        }
        setInstance(role);

        privileges = Lists.newArrayList(getInstance().getPrivilegeSet());
        initPrivilegeList();
        initSelectedPrivileges();
        initRoleList();

        valueChangeListener();
    }

    @Override
    public String persist() {
        try {
            mergeSelectedPrivileges();
            getInstance().setPrivilegeSet(Sets.newHashSet(privileges));
            Role role = roleService.save(instance);
            setInstanceId(role.getId());
            setInstance(role);

            WebUtil.addInfoMessage("admin.auth.role.success.create");

            return navToView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String update() {
        try {
            mergeSelectedPrivileges();
            getInstance().setPrivilegeSet(Sets.newHashSet(privileges));
            setInstance(getInstance());
            roleService.update(instance);

            WebUtil.addInfoMessage("admin.auth.role.success.update");

            return navToView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initRoleList() {
        roleList = new ArrayList<Role>(roleService.findAll());
        if (isManaged()) {
            roleList.remove(getInstance());
        }
    }

    private void initPrivilegeList() {
        privilegeList = new ArrayList<Ext<Privilege>>();
        List<Privilege> plist = privilegeService.findAll();
        for (Privilege privilege : plist) {
            Ext<Privilege> privilegeExt = new Ext<Privilege>(privilege, false);
            privilegeList.add(privilegeExt);
        }
    }

    private void initSelectedPrivileges() {
        for (Privilege privilege : getInstance().getPrivilegeSet()) {
            for (Ext<Privilege> ext : privilegeList) {
                if (ext.getInstance().getId().equals(privilege.getId())) {
                    ext.setSelected(true);
                    break;
                }
            }
        }
    }

    private void initSelectedPrivilegesByForm() {
        for (Privilege privilege : privileges) {
            for (Ext<Privilege> ext : privilegeList) {
                if (ext.getInstance().getId().equals(privilege.getId())) {
                    ext.setSelected(true);
                    break;
                }
            }
        }
    }

    private void mergeSelectedPrivileges() {
        privileges = new ArrayList<Privilege>();
        for (Ext<Privilege> ext : privilegeList) {
            if (ext.isSelected()) {
                privileges.add(ext.getInstance());
            }
        }
    }

    private void changeSelectStatusPrivilegeList(boolean selected) {
        for (Ext<Privilege> ext : privilegeList) {
            ext.setSelected(selected);
        }
    }

    private void mergeSearchedPrivileges() {
        for (Ext<Privilege> ext : privilegeList) {
            if (privileges.contains(ext.getInstance())) {
                if (!ext.isSelected()) {
                    privileges.remove(ext.getInstance());
                }
            } else {
                if (ext.isSelected()) {
                    privileges.add(ext.getInstance());
                }
            }
        }
    }

    public void search() {
        mergeSearchedPrivileges();
        if (role != null) {
            privilegeList = new ArrayList<Ext<Privilege>>();
            Role role1 = roleService.findForRoleFilter(role.getId(), name, excludeName);
            for (Privilege privilege : role1.getPrivilegeSet()) {
                Ext<Privilege> privilegeExt = new Ext<Privilege>(privilege, false);
                if (privileges.contains(privilege)) {
                    privilegeExt.setSelected(true);
                }
                privilegeList.add(privilegeExt);
            }
        } else {
            privilegeList = new ArrayList<Ext<Privilege>>();
            List<Privilege> plist = privilegeService.findForRoleFilter(name, excludeName);
            for (Privilege privilege : plist) {
                Ext<Privilege> privilegeExt = new Ext<Privilege>(privilege, false);
                if (privileges.contains(privilege)) {
                    privilegeExt.setSelected(true);
                }
                privilegeList.add(privilegeExt);
            }
        }
        valueChangeListener();
    }

    public void reset() {
        name = null;
        excludeName = null;
        role = null;

        mergeSearchedPrivileges();
        initPrivilegeList();
        initSelectedPrivilegesByForm();
    }

    public void valueChangeListener() {
        if (privilegeList != null && privilegeList.size() > 0) {
            boolean allSelected = true;
            for (Ext<Privilege> privilegeExt : privilegeList) {
                if (!privilegeExt.isSelected()) {
                    allSelected = false;
                    break;
                }
            }
            selectAll = allSelected;
        } else {
            selectAll = false;
        }
    }

    @Override
    public String remove() {
        return null;
    }

    @Override
    public boolean isManaged() {
        return instance.getId() != null;
    }

    @Override
    protected String getViewId() {
        return "roleEdit";
    }

    public String navToView() {
        NavRule navToView = new NavRule("roleView");
        navToView.addParam("instanceId", instanceId.toString());
        return nav.cleanRedirect(navToView, getNavId());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("roleList");
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
    }

    public List<Ext<Privilege>> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<Ext<Privilege>> privilegeList) {
        this.privilegeList = privilegeList;
    }

    public List<Ext<Privilege>> getFilteredPrivilegeList() {
        return filteredPrivilegeList;
    }

    public void setFilteredPrivilegeList(List<Ext<Privilege>> filteredPrivilegeList) {
        this.filteredPrivilegeList = filteredPrivilegeList;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
        changeSelectStatusPrivilegeList(selectAll);
    }

    public String getFilterPrivilegeName() {
        return filterPrivilegeName;
    }

    public void setFilterPrivilegeName(String filterPrivilegeName) {
        this.filterPrivilegeName = filterPrivilegeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExcludeName() {
        return excludeName;
    }

    public void setExcludeName(String excludeName) {
        this.excludeName = excludeName;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
