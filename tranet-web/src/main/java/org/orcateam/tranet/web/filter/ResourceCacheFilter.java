package org.orcateam.tranet.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class ResourceCacheFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        long cacheAge = 3600;
        long expiry = new Date().getTime() + cacheAge * 1000;

        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        httpResponse.setDateHeader("Expires", expiry);
        httpResponse.setHeader("Cache-Control", "max-age=" + cacheAge);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
