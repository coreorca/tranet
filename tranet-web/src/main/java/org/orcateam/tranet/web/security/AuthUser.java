package org.orcateam.tranet.web.security;

import org.orcateam.tranet.core.model.auth.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class AuthUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public AuthUser(User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getEmail(), "password", authorities);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
