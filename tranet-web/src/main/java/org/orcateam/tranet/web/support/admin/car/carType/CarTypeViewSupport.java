package org.orcateam.tranet.web.support.admin.car.carType;

import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.service.CarTypeService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class CarTypeViewSupport extends BaseViewSupport<CarType, Integer> {

    @Autowired
    transient CarTypeService carTypeService;

    @Override
    @Secured("admin.car.carType.view")
    public void initView(Integer instanceId) throws Exception {
        CarType carType = carTypeService.get(instanceId);
        setInstance(carType);
    }

    @Override
    @Secured("admin.car.carType.update")
    public String navToEdit() {
        NavRule rule = new NavRule("carTypeEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "carTypeView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("carTypeList");
    }
}
