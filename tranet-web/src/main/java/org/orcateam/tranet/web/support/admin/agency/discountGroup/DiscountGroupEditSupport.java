package org.orcateam.tranet.web.support.admin.agency.discountGroup;

import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.service.DiscountGroupService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class DiscountGroupEditSupport extends BaseEditSupport<DiscountGroup, Integer> {

    @Autowired
    transient DiscountGroupService entityService;

    @Override
    @Secured("admin.agency.discountGroup.create")
    public void initCreate() throws Exception {
        DiscountGroup entity = new DiscountGroup();
        setInstance(entity);
    }

    @Override
    @Secured("admin.agency.discountGroup.update")
    public void initUpdate(Integer instanceId) throws Exception {
        DiscountGroup entity = entityService.get(instanceId);
        if (entity == null) {
            throw new NotFoundException();
        }
        setInstance(entity);
    }

    @Override
    public String persist() {
        try {
            DiscountGroup persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "discountGroupEdit";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("discountGroupList");
        } else {
            NavRule rule = new NavRule("discountGroupView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("discountGroupView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToList() {
        NavRule rule = new NavRule("discountGroupList");
        return nav.cleanRedirect(rule, navId.getValue());
    }
}
