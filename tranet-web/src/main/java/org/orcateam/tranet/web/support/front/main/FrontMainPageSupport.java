package org.orcateam.tranet.web.support.front.main;

import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BasePageSupport;
import org.orcateam.tranet.web.util.Time;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class FrontMainPageSupport extends BasePageSupport {

    @Override
    public void initPage() {

    }

    @Override
    protected String getViewId() {
        return "frontMainPage";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

}
