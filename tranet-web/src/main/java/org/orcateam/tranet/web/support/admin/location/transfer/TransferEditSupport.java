package org.orcateam.tranet.web.support.admin.location.transfer;

import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.model.transfer.TransferPrice;
import org.orcateam.tranet.core.service.*;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@ManagedBean
@ViewScoped
public class TransferEditSupport extends BaseEditSupport<Transfer, Integer> {

    private Double distance;
    private Integer time;

    private List<Country> countryList = new ArrayList<Country>();
    private List<City> cityList = new ArrayList<City>();

    private List<Point> airportList = new ArrayList<Point>();
    private List<Point> regionList = new ArrayList<Point>();

    /** Price Tab */
    private List<TransferPrice> transferPriceList = new ArrayList<TransferPrice>();
    private List<CurrencyType> currencyTypeList = new ArrayList<CurrencyType>();
    /** Price Tab */

    @Autowired
    transient TransferService entityService;

    @Autowired
    transient PointService pointService;

    @Autowired
    transient CountryService countryService;

    @Autowired
    transient CityService cityService;

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    @Autowired
    transient CarTypeService carTypeService;

    @Autowired
    transient TransferPriceService transferPriceService;

    @ManagedProperty("#{carTypeComponent}")
    private CarTypeComponent carTypeComponent;

    @Override
    @Secured(".create")
    public void initCreate() {
        Transfer entity = new Transfer();
        setInstance(entity);
        initLocationVars();

        initPriceVars();
    }

    @Override
    @Secured(".update")
    public void initUpdate(Integer instanceId) throws Exception {
        Transfer entity = entityService.getWithPoints(instanceId);
        setInstance(entity);
        if (entity == null) {
            throw new NotFoundException();
        }

        initLocationVars();

        initPriceVars();
    }

    private void initLocationVars() {
        initCountryList();
        initCityList();
        initPointList();
    }

    private void initCountryList() {
        countryList = countryService.getAll();
    }

    private void initCityList() {
        cityList.clear();
        if (instance.getCountry() != null) {
            List<City> result = cityService.getListByCountryId(instance.getCountry().getId());
            cityList.addAll(result);
        }
    }

    private void initPointList() {
        initAirportList();
        initRegionList();
    }

    private void initAirportList() {
        airportList.clear();
        if (instance.getCity() != null) {
            List<Point> result = pointService.getByCityAndType(instance.getCity().getId(), PointTypeEnum.AIRPORT);
            airportList.addAll(result);
        }
    }

    private void initRegionList() {
        regionList.clear();
        if (instance.getCity() != null) {
            List<Point> result = pointService.getByCityAndType(instance.getCity().getId(), PointTypeEnum.REGION);
            regionList.addAll(result);
        }
    }

    public void countryAjaxChangeListener() {
        instance.setCity(null);
        instance.setAirport(null);
        instance.setRegion(null);
        initCityList();
        initPointList();
    }

    public void cityAjaxChangeListener() {
        instance.setAirport(null);
        instance.setRegion(null);
        initPointList();
    }

    @Override
    public String persist() {
        try {
            beforeManage();

            Transfer persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            beforeManage();

            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    private void beforeManage() {
        if (transferPriceList != null) {
            instance.setPriceSet(new HashSet<TransferPrice>(transferPriceList));
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    public List<String> loadGeoList() {
        List<String> geoList = new ArrayList<String>();
        if (instance.getAirport() != null) {
            geoList.add(instance.getAirport().getLatLng());
        }
        if (instance.getRegion() != null) {
            geoList.add(instance.getRegion().getLatLng());
        }
        return geoList;
    }

    /** Price Tab */
    private void initPriceVars() {
        List<TransferPrice> transferPriceList = transferPriceService.findEagerlyByTransferId(instance.getId());
        instance.setPriceSet(new HashSet<TransferPrice>(transferPriceList));

        initCurrencyTypeList();
        initTransferPriceList();
    }

    private void initTransferPriceList() {
        transferPriceList.clear();
        transferPriceList = new ArrayList<TransferPrice>(instance.getPriceSet());
    }

    private void initCurrencyTypeList() {
        currencyTypeList = currencyTypeService.getAll();
    }

    public void beforeCarTypeSelect() {
        carTypeComponent.reset();
    }

    public void addNewTransferPrice(CarType carType) {
        for (TransferPrice transferPrice : transferPriceList) {
            if (transferPrice.getCarType().getId().equals(carType.getId())) {
                WebUtil.addWarnMessage("admin.location.transfer.tab.priceList.carTypeExists");
                return;
            }
        }
        TransferPrice transferPrice = new TransferPrice();
        transferPrice.setTransfer(instance);
        transferPrice.setCarType(carType);

        transferPriceList.add(transferPrice);
    }
    /** Price Tab */

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "transferEdit}";
    }

    public String navToList() {
        NavRule rule = new NavRule("transferList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("transferView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("transferList");
        } else {
            NavRule rule = new NavRule("transferView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public List<Point> getAirportList() {
        return airportList;
    }

    public void setAirportList(List<Point> airportList) {
        this.airportList = airportList;
    }

    public List<Point> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<Point> regionList) {
        this.regionList = regionList;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public List<TransferPrice> getTransferPriceList() {
        return transferPriceList;
    }

    public void setTransferPriceList(List<TransferPrice> transferPriceList) {
        this.transferPriceList = transferPriceList;
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }

    public CarTypeComponent getCarTypeComponent() {
        return carTypeComponent;
    }

    public void setCarTypeComponent(CarTypeComponent carTypeComponent) {
        this.carTypeComponent = carTypeComponent;
    }
}
