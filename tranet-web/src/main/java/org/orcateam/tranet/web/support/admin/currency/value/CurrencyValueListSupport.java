package org.orcateam.tranet.web.support.admin.currency.value;

import org.orcateam.tranet.core.dao.parameter.CurrencyValueParameter;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.core.service.CurrencyValueService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class CurrencyValueListSupport extends BaseListSupport<CurrencyValue, Integer> {

    CurrencyValueParameter params = new CurrencyValueParameter();

    private List<CurrencyType> currencyTypeList = null;

    @Autowired
    transient CurrencyValueService entityService;

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    @Override
    public void reset() {
        params = new CurrencyValueParameter();
        super.reset();
    }

    @Override
    @Secured("admin.currency.value.list")
    protected void initResultList() {
        Pagination<CurrencyValue> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
        initCurrencyTypeList();
    }

    private synchronized void initCurrencyTypeList() {
        if (currencyTypeList == null) {
            currencyTypeList = currencyTypeService.getAll();
        }
    }

    @Override
    @Secured("admin.currency.value.view")
    public String navToView(CurrencyValue entity) {
        NavRule rule = new NavRule("currencyValueView", "id", entity.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.currency.value.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("currencyValueEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "currencyValueList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public CurrencyValueParameter getParams() {
        return params;
    }

    public void setParams(CurrencyValueParameter params) {
        this.params = params;
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }
}
