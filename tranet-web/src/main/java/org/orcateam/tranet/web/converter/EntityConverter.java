package org.orcateam.tranet.web.converter;

import java.lang.reflect.Method;

import javax.faces.convert.FacesConverter;
import javax.persistence.Id;


@FacesConverter(value = "entityConverter")
public class EntityConverter extends BaseConverter {

	public String getEntityIdValue(Object object) {
		Method method = null;
		String value = null;
		try {
			for(Method m : object.getClass().getMethods()) {
				Id id = m.getAnnotation(Id.class);
				if(id != null || m.getName().equals("getId")) {
					method = m; break;
				}
			}
			if(method != null) {
				Object val = method.invoke(object);
				value = val.toString();
			}
		}
		catch (Exception e) {
			value = null;
		}
		return value;
	}
}
