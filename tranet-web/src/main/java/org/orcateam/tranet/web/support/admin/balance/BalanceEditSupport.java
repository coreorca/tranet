package org.orcateam.tranet.web.support.admin.balance;

import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.model.balance.Balance;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.service.AgencyService;
import org.orcateam.tranet.core.service.BalanceService;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.exception.NotImplementedMethodException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class BalanceEditSupport extends BaseEditSupport<Balance, Integer> {

    private Integer agencyId;

    private List<CurrencyType> currencyTypeList = new ArrayList<CurrencyType>();

    @Autowired
    transient BalanceService entityService;

    @Autowired
    transient AgencyService agencyService;

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    @Override
    @Secured("admin.balance.create")
    public void initCreate() throws Exception {
        if (agencyId == null) {
            throw new NotImplementedMethodException();
        }

        Agency agency = agencyService.get(agencyId);
        if (agency == null) {
            throw new NotFoundException();
        }

        Balance entity = new Balance();
        entity.setAgency(agency);

        setInstance(entity);

        initCurrenctTypeList();
    }

    private void initCurrenctTypeList() {
        currencyTypeList.clear();
        currencyTypeList = currencyTypeService.getAll();
    }

    @Override
    public void initUpdate(Integer instanceId) throws Exception {
        throw new NotImplementedMethodException();
    }

    @Override
    public String persist() {
        try {
            entityService.saveAndIncreaseAgencyBalance(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToBack();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            return null;
        }
    }

    @Override
    public String update() {
        return null;
    }

    @Override
    public String remove() {
        return null;
    }

    @Override
    public boolean isManaged() {
        return false;
    }

    @Override
    protected String getViewId() {
        return "balanceEdit";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        NavRule navRule = new NavRule("agencyEdit");
        navRule.addParam("id", agencyId.toString());
        return navRule;
    }

    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }
}
