package org.orcateam.tranet.web.service;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Properties;

@Service
public class ViewService {

    Properties props = null;
    Logger logger = Logger.getLogger(getClass());

    @PostConstruct
    public void init() {
        try {
            props = new Properties();
            props.load(getClass().getResourceAsStream("/navigation.properties"));
        } catch (Exception e) {
            //todo: use the our owsim exception handler.
            logger.error(e.getMessage(), e);
        }
    }

    public String getView(String view) {
        init();
        String path = props.getProperty(view, null);
        logger.debug("Getting view from viewmap: " + view + ", path: " + path);
        return path;
    }

}
