package org.orcateam.tranet.web.support.admin.auth.user;

import org.orcateam.tranet.core.dao.parameter.UserParameter;
import org.orcateam.tranet.core.enumeration.GenderEnum;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.service.UserService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class UserListSupport extends BaseListSupport<User, Integer> {

    private UserParameter userParameter = new UserParameter();

    @Autowired
    transient UserService userService;

    @Override
    @Secured("admin.auth.user.list")
    protected void initResultList() {
        try {
            Pagination<User> pagination = userService.findWithPagination(getPager(), userParameter);
            applyResult(pagination);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected String getViewId() {
        return "userList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    @Override
    @Secured("admin.auth.user.create")
    public String navToCreate() {
        NavRule navToAdd = new NavRule("userWizard");
        return nav.redirect(navToAdd, getNavId());
    }

    @Override
    @Secured("admin.auth.user.view")
    public String navToView(User user) {
        NavRule navToView = new NavRule("userView");
        navToView.addParam("id", user.getId().toString());
        return nav.redirect(navToView, getNavId());
    }

    public GenderEnum[] getGenderEnumList() {
        return GenderEnum.values();
    }

    public UserParameter getUserParameter() {
        return userParameter;
    }

    public void setUserParameter(UserParameter userParameter) {
        this.userParameter = userParameter;
    }
}
