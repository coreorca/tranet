package org.orcateam.tranet.web.support.admin.location.city;

import org.orcateam.tranet.core.dao.parameter.CityParameter;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.service.CityService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CityListSupport extends BaseListSupport<City, Integer> {

    CityParameter params = new CityParameter();

    @Autowired
    transient CityService entityService;

    @Override
    public void reset() {
        params = new CityParameter();
        super.reset();
    }

    @Override
    @Secured("admin.location.city.list")
    protected void initResultList() {
        Pagination<City> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Secured("admin.location.city.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("cityEdit"), navId.getValue());
    }

    @Secured("admin.location.city.view")
    public String navToView(City city) {
        NavRule rule = new NavRule("cityView", "id", city.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "cityList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public CityParameter getParams() {
        return params;
    }

    public void setParams(CityParameter params) {
        this.params = params;
    }

}
