package org.orcateam.tranet.web.support.admin.agency.discountGroup;

import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.service.DiscountGroupService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class DiscountGroupViewSupport extends BaseViewSupport<DiscountGroup, Integer> {

    @Autowired
    transient DiscountGroupService entityService;

    @Override
    @Secured("admin.agency.discountGroup.view")
    public void initView(Integer instanceId) throws Exception {
        DiscountGroup entity = entityService.get(instanceId);
        if (entity == null) {
            throw new NotFoundException();
        }
        setInstance(entity);
    }

    @Override
    @Secured("admin.agency.discountGroup.update")
    public String navToEdit() {
        NavRule rule = new NavRule("discountGroupEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "discountGroupView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("discountGroupList");
    }
}
