package org.orcateam.tranet.web.support.agency.auth.user;

import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.service.AgencyUserService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class AgencyUserViewSupport extends BaseViewSupport<AgencyUser, Integer> {

    @Autowired
    transient AgencyUserService agencyUserService;

    @Override
    @Secured("agency.auth.user.view")
    public void initView(Integer instanceId) throws Exception {
        AgencyUser agencyUser = agencyUserService.findEagerlyById(instanceId);
        setInstance(agencyUser);
    }

    @Override
    @Secured("agency.auth.user.update")
    public String navToEdit() {
        NavRule navToEdit = new NavRule("agencyUserEdit");
        navToEdit.addParam("instanceId", instanceId.toString());
        return nav.redirect(navToEdit, getNavId());
    }

    @Override
    protected String getViewId() {
        return "agencyUserView";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("agencyUserList");
    }
}
