package org.orcateam.tranet.web.security;

import com.sun.faces.application.ActionListenerImpl;
import org.apache.log4j.Logger;
import org.orcateam.tranet.web.exception.NonSecuredException;
import org.orcateam.tranet.web.util.WebUtil;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.component.ActionSource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecurityActionListener extends ActionListenerImpl implements ActionListener {
    static Logger logger = Logger.getLogger(SecurityActionListener.class);

    static String EL_REGEX = "#\\{([\\w]+)\\.([\\w]+)([\\(\\w, \\)]*)\\}";
    static Pattern pattern = null;

    static {
        if (pattern == null) {
            pattern = Pattern.compile(EL_REGEX);
        }
    }

    @Override
    public void processAction(final ActionEvent event) {
        final FacesContext context = FacesContext.getCurrentInstance();

        String expr = getExpression(context, event);

        if (expr == null) {
            super.processAction(event);
            return;
        }

        Matcher matcher = pattern.matcher(expr);
        if (!matcher.find()) {
            super.processAction(event);
            return;
        }
        String target = matcher.group(1); // class expression
        String method = matcher.group(2); // method name

        final ELContext elContext = context.getELContext();
        final ExpressionFactory factory = context.getApplication().getExpressionFactory();

        final ValueExpression ve = factory.createValueExpression(elContext, "#{" + target + '}', Object.class);
        final Object result = ve.getValue(elContext);

        checkMethods(result.getClass(), method, event);
    }

    private String getExpression(final FacesContext context, final ActionEvent event) {
        final Application application = context.getApplication();
        final ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler) application.getNavigationHandler();

        final UIComponent source = event.getComponent();
        final ActionSource actionSource = (ActionSource) source;
        MethodBinding binding;

        context.getELContext();
        binding = actionSource.getAction();
        if (binding != null) {
            return binding.getExpressionString();
        }
        return null;
    }

    private void checkMethods(Class clazz, final String method, final ActionEvent event) {
        final Method[] methods = clazz.getMethods();
        for (final Method meth : methods) {
            if (meth.getName().equals(method)) {
                if (meth.isAnnotationPresent(Secured.class)) {
                    try {
                        SecurityUtil.checkSecured(clazz, meth.getName(), meth.getParameterTypes());
                        super.processAction(event);
                    } catch (NonSecuredException e) {
                        logger.debug("Method secured exception", e);
                        WebUtil.addErrorMessage("global.messages.error.securedAction");
                        return;
                    }
                } else {
                    super.processAction(event);
                }
                break;
            }
        }
    }
}
