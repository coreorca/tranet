package org.orcateam.tranet.web.support;

import org.orcateam.tranet.web.exception.NavIdExpiredException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BaseWizardSupport<E, PK> extends BaseSupport<E, PK> {

    private List<String> stateList = new ArrayList<String>(0);
    private List<Integer> skippedStateIndexList = new ArrayList<Integer>(0);

    private Integer index = 0;

    public abstract void initWizard() throws Exception;

    public abstract void initWizard(PK instanceId) throws Exception;

    public abstract String finish();

    public abstract String persist();


    public abstract String update();

    public abstract boolean isManaged();

    @Override
    protected void callInitFunctions() throws Exception {
        if (instanceId != null) {
            checkSecured("initWizard", instanceId.getClass());
            logger.debug("calling initWizard(" + instanceId + ") function from support");
            initWizard(instanceId);
        } else {
            checkSecured("initWizard");
            logger.debug("calling initWizard() function from support.");
            initWizard();
        }
    }

    public void putAll(String[] states) {
        stateList.addAll(Arrays.asList(states));
        skippedStateIndexList.clear();
    }

    public void next() {
        if (index + 1 < stateList.size()) {
            index++;
            if (isSkipped(index)) {
                next();
            }
        }
    }

    public void previous() {
        if (index > 0) {
            index--;
            if (isSkipped(index)) {
                previous();
            }
        }
    }

    public String cancel() throws NavIdExpiredException {
        return navToBack();
    }

    public boolean isLastState() {
        return index.equals(stateList.size() - 1);
    }

    public boolean isFirstState() {
        return index.equals(0);
    }

    public boolean isSkipped(Integer currentStateIndex) {
        return skippedStateIndexList.contains(currentStateIndex);
    }

    public void skip(Integer currentStateIndex) {
        if (!isSkipped(currentStateIndex)) {
            skippedStateIndexList.add(currentStateIndex);
        }
    }

    public void unskip(Integer currentStateIndex) {
        if (isSkipped(currentStateIndex)) {
            skippedStateIndexList.remove(currentStateIndex);
        }
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public List<String> getStateList() {
        return stateList;
    }

    public void setStateList(List<String> stateList) {
        this.stateList = stateList;
    }

    public List<Integer> getSkippedStateIndexList() {
        return skippedStateIndexList;
    }

    public void setSkippedStateIndexList(List<Integer> skippedStateIndexList) {
        this.skippedStateIndexList = skippedStateIndexList;
    }
}
