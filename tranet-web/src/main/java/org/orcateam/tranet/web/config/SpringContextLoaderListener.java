package org.orcateam.tranet.web.config;

public class SpringContextLoaderListener extends org.springframework.web.context.ContextLoaderListener {


    public SpringContextLoaderListener() {
        System.setProperty("org.apache.el.parser.COERCE_TO_ZERO", "false");
    }
}
