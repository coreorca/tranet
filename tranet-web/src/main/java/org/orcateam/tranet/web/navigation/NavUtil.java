package org.orcateam.tranet.web.navigation;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;

public class NavUtil {


    public final static String NAV_ID = "navId";
    public final static String SKIP_HISTORY = "sknav";
    public static Integer MAX_NAV_EXPIRE_TIME = 15;

    public static String getHash(String viewId, List<NavParam> paramList) {
        StringBuffer str = new StringBuffer(viewId);
        for (NavParam param : paramList) {
            str.append(param.toString());
        }
        String hash = DigestUtils.md5Hex(str.toString());
        return hash;
    }

}
