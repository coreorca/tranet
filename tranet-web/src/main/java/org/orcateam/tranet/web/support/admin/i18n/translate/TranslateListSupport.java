package org.orcateam.tranet.web.support.admin.i18n.translate;

import org.orcateam.tranet.core.dao.parameter.LanguageParameter;
import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.service.LanguageService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class TranslateListSupport extends BaseListSupport<Language, Integer> {

    LanguageParameter params = new LanguageParameter();

    @Autowired
    transient LanguageService entityService;

    @Override
    public void reset() {
        params = new LanguageParameter();
        super.reset();
    }

    @Override
    @Secured("admin.i18n.translate.list")
    protected void initResultList() {
        Pagination<Language> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.i18n.translate.edit")
    public String navToView(Language entity) {
        NavRule rule = new NavRule("translateBulkEdit");
        rule.addParam("code", entity.getCode().getValue());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    public String navToCreate() {
        return null;
    }

    @Override
    protected String getViewId() {
        return "translateList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public LanguageParameter getParams() {
        return params;
    }

    public void setParams(LanguageParameter params) {
        this.params = params;
    }

}
