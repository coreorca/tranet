package org.orcateam.tranet.web.support.admin.auth.user;

import org.orcateam.tranet.core.enumeration.UserTypeEnum;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.service.UserService;
import org.orcateam.tranet.core.util.UserUtil;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class UserViewSupport extends BaseViewSupport<User, Integer> {

    @Autowired
    transient UserService userService;

    @Override
    @Secured("admin.auth.user.view")
    public void initView(Integer instanceId) throws Exception {
        User user = userService.findEagerlyById(instanceId);

        if (user == null) {
            throw new NotFoundException();
        }

        setInstance(user);
    }

    @Override
    protected String getViewId() {
        return "userView";
    }

    @Secured("admin.auth.user.update")
    public String navToEdit() {
        NavRule navToEdit = new NavRule("userWizard");
        navToEdit.addParam("id", instanceId.toString());
        return nav.redirect(navToEdit, getNavId());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("userList");
    }

    public UserTypeEnum getUserType() {
        try {
            return UserUtil.getUserType(instance);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
