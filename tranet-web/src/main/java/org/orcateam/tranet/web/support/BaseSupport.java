package org.orcateam.tranet.web.support;


import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.orcateam.tranet.core.exception.OrcaException;
import org.orcateam.tranet.web.BaseBean;
import org.orcateam.tranet.web.exception.NavIdExpiredException;
import org.orcateam.tranet.web.exception.NonSecuredException;
import org.orcateam.tranet.web.navigation.NavId;
import org.orcateam.tranet.web.navigation.NavParam;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.navigation.NavUtil;
import org.orcateam.tranet.web.security.Authentication;
import org.orcateam.tranet.web.security.SecurityUtil;
import org.orcateam.tranet.web.service.Navigation;
import org.orcateam.tranet.web.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class BaseSupport<E, PK> extends BaseBean {

    protected E instance;
    protected PK instanceId;
    protected NavId navId;

    @Autowired
    transient protected Navigation nav;

    @Autowired
    transient protected Authentication authentication;

    protected Logger logger = Logger.getLogger(getClass());

    protected abstract void callInitFunctions() throws Exception;

    protected abstract String getViewId();

    protected abstract NavRule getDefaultBackNavRule();

    public void init() throws Exception {
        try {
            boolean postBack = FacesContext.getCurrentInstance().isPostback();
            if (!postBack) {
                initNavRule();
                callInitFunctions();
            }
        } catch (OrcaException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    protected void checkSecured(String methodName) throws NonSecuredException {
        checkSecured(methodName, null);
    }

    protected void checkSecured(String methodName, Class<?>... parameters) throws NonSecuredException {
        SecurityUtil.checkSecured(getClass(), methodName, parameters);
    }

    protected void initNavRule() throws NavIdExpiredException {
        String viewId = getViewId();
        List<NavParam> paramList = getNavParamList();
        NavRule navRule = new NavRule(viewId, paramList);
        navId = nav.push(navRule);
    }

    public List<NavParam> getNavParamList() {
        List<NavParam> paramList = new ArrayList<NavParam>();
        Map<String, String> params = HttpUtil.getHttpParamMap();
        for (String param : params.keySet()) {
            if (NavUtil.NAV_ID.equals(param)) {
                continue;
            }
            String value = params.get(param);
            paramList.add(new NavParam(param, value));
        }
        return paramList;
    }

    public Long getNavId() {
        return navId.getValue();
    }


    public String navToBack() throws NavIdExpiredException {
        String back = nav.back(navId);
        if (StringUtils.isBlank(back)) {
            back = nav.getNavURL(getDefaultBackNavRule(), navId.getValue());
        }
        return back;
    }

    public E getInstance() {
        return instance;
    }

    public void setInstance(E instance) {
        this.instance = instance;
    }

    public PK getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(PK instanceId) {
        this.instanceId = instanceId;
    }

    public String getSelectedTab() {
        return navId.peekFirst().getSelectedTab();
    }

    public void setSelectedTab(String selectedTab) {
        navId.peekFirst().setSelectedTab(selectedTab);
    }

}
