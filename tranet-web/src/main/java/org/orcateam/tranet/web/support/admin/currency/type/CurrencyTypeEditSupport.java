package org.orcateam.tranet.web.support.admin.currency.type;

import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CurrencyTypeEditSupport extends BaseEditSupport<CurrencyType, Integer> {

    @Autowired
    transient CurrencyTypeService entityService;

    @Override
    @Secured("admin.currency.type.create")
    public void initCreate() {
        CurrencyType entity = new CurrencyType();
        setInstance(entity);
    }

    @Override
    @Secured("admin.currency.type.update")
    public void initUpdate(Integer instanceId) {
        CurrencyType entity = entityService.get(instanceId);
        setInstance(entity);
    }

    @Override
    public String persist() {
        try {
            CurrencyType persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "currencyTypeEdit}";
    }

    public String navToList() {
        NavRule rule = new NavRule("currencyTypeList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("currencyTypeView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("currencyTypeList");
        } else {
            NavRule rule = new NavRule("currencyTypeView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }


}
