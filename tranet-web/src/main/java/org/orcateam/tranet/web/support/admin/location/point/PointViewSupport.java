package org.orcateam.tranet.web.support.admin.location.point;

import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.service.PointService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class PointViewSupport extends BaseViewSupport<Point, Integer> {

    @Autowired
    transient PointService entityService;

    @Override
    @Secured("admin.location.point.view")
    public void initView(Integer instanceId) throws Exception {
        Point result = entityService.getWithCity(instanceId);
        setInstance(result);

    }

    @Override
    @Secured("admin.location.point.update")
    public String navToEdit() {
        NavRule rule = new NavRule("pointEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "pointView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("pointList");
    }

}
