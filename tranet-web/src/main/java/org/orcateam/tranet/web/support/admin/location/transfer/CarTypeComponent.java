package org.orcateam.tranet.web.support.admin.location.transfer;


import org.orcateam.tranet.core.dao.parameter.CarTypeParameter;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.service.CarTypeService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CarTypeComponent extends BaseListSupport<CarType, Integer> {

    private CarTypeParameter params = new CarTypeParameter();

    @Autowired
    transient CarTypeService carTypeService;

    @Override
    public void reset() {
        params = new CarTypeParameter();
        super.reset();
    }

    @Override
    protected void initResultList() {
        try {
            Pagination<CarType> pagination = carTypeService.findWithPagination(getPager(), params);
            applyResult(pagination);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public String navToView(CarType instance) {
        return null;
    }

    @Override
    public String navToCreate() {
        return null;
    }

    @Override
    protected String getViewId() {
        return null;
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public CarTypeParameter getParams() {
        return params;
    }

    public void setParams(CarTypeParameter params) {
        this.params = params;
    }
}
