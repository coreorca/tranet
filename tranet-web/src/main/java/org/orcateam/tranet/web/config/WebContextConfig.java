package org.orcateam.tranet.web.config;

import org.orcateam.tranet.core.config.CoreContextConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({CoreContextConfig.class, WebSecurityConfig.class})
@ComponentScan({"org.orcateam.tranet.web"})
public class WebContextConfig {

    @Autowired
    CoreContextConfig coreContext;

}
