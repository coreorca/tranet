package org.orcateam.tranet.web.support.admin.location.transfer;

import org.orcateam.tranet.core.dao.parameter.TransferParameter;
import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.service.TransferService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class TransferListSupport extends BaseListSupport<Transfer, Integer> {

    TransferParameter params = new TransferParameter();

    @Autowired
    transient TransferService entityService;

    @Override
    public void reset() {
        params = new TransferParameter();
        super.reset();
    }

    @Override
    @Secured("admin.location.transfer.list")
    protected void initResultList() {
        Pagination<Transfer> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);
    }

    @Override
    @Secured("admin.location.transfer.view")
    public String navToView(Transfer entity) {
        NavRule rule = new NavRule("transferView", "id", entity.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.location.transfer.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("transferEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "transferList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public TransferParameter getParams() {
        return params;
    }

    public void setParams(TransferParameter params) {
        this.params = params;
    }

}
