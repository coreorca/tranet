package org.orcateam.tranet.web.support.admin.auth.role;

import com.google.common.collect.Lists;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class RoleViewSupport extends BaseViewSupport<Role, Integer> {

    private List<Privilege> privilegeList = new ArrayList<Privilege>(0);

    @Autowired
    transient RoleService roleService;

    @Override
    @Secured("admin.auth.role.view")
    public void initView(Integer instanceId) throws Exception {
        Role role = roleService.findEagerlyById(instanceId);
        if (role == null) {
            throw new NotFoundException();
        }
        setInstance(role);
        privilegeList = Lists.newArrayList(role.getPrivilegeSet());
    }

    @Override
    protected String getViewId() {
        return "roleView";
    }

    @Secured("admin.auth.role.update")
    public String navToEdit() {
        NavRule navToEdit = new NavRule("roleEdit");
        navToEdit.addParam("instanceId", instanceId.toString());
        return nav.redirect(navToEdit, getNavId());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("roleList");
    }

    public List<Privilege> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<Privilege> privilegeList) {
        this.privilegeList = privilegeList;
    }
}
