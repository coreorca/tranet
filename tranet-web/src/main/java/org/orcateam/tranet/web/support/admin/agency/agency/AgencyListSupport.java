package org.orcateam.tranet.web.support.admin.agency.agency;

import org.orcateam.tranet.core.dao.parameter.AgencyParameter;
import org.orcateam.tranet.core.enumeration.PaymentTypeEnum;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.service.AgencyService;
import org.orcateam.tranet.core.service.DiscountGroupService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AgencyListSupport extends BaseListSupport<Agency, Integer> {

    private AgencyParameter params = new AgencyParameter();

    private List<DiscountGroup> discountGroupList = new ArrayList<DiscountGroup>();

    @Autowired
    transient AgencyService entityService;

    @Autowired
    transient DiscountGroupService discountGroupService;

    @Override
    protected void initResultList() {
        Pagination<Agency> pagination = entityService.findWithPagination(getPager(), params);
        applyResult(pagination);

        initDiscountGroupList();
    }

    private void initDiscountGroupList() {
        discountGroupList = discountGroupService.getAll();
    }

    @Override
    public void reset() {
        params = new AgencyParameter();
        super.reset();
    }

    @Override
    @Secured("admin.agency.agency.view")
    public String navToView(Agency instance) {
        NavRule rule = new NavRule("agencyView", "id", instance.getId().toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    @Secured("admin.agency.agency.create")
    public String navToCreate() {
        return nav.redirect(new NavRule("agencyEdit"), navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "agencyList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public PaymentTypeEnum[] getPaymentTypes() {
        return PaymentTypeEnum.values();
    }

    public AgencyParameter getParams() {
        return params;
    }

    public void setParams(AgencyParameter params) {
        this.params = params;
    }

    public List<DiscountGroup> getDiscountGroupList() {
        return discountGroupList;
    }

    public void setDiscountGroupList(List<DiscountGroup> discountGroupList) {
        this.discountGroupList = discountGroupList;
    }
}
