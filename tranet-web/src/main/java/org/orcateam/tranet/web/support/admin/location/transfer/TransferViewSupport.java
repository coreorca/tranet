package org.orcateam.tranet.web.support.admin.location.transfer;

import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.model.transfer.TransferPrice;
import org.orcateam.tranet.core.service.TransferPriceService;
import org.orcateam.tranet.core.service.TransferService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class TransferViewSupport extends BaseViewSupport<Transfer, Integer> {

    private Double distance;
    private Integer time;

    /** Price Tab */
    private List<TransferPrice> transferPriceList = new ArrayList<TransferPrice>();
    /** Price Tab */

    @Autowired
    transient TransferService entityService;

    @Autowired
    transient TransferPriceService transferPriceService;

    @Override
    @Secured("admin.location.transfer.view")
    public void initView(Integer instanceId) throws Exception {
        Transfer entity = entityService.getWithPoints(instanceId);
        setInstance(entity);
        if (entity == null) {
            throw new NotFoundException();
        }

        initPriceList();
    }

    private void initPriceList() {
        transferPriceList = transferPriceService.findEagerlyByTransferId(instance.getId());
    }

    @Secured("admin.location.transfer.update")
    public String navToEdit() {
        NavRule rule = new NavRule("transferEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    public List<String> loadGeoList() {
        List<String> geoList = new ArrayList<String>();
        geoList.add(instance.getAirport().getLatLng());
        geoList.add(instance.getRegion().getLatLng());
        return geoList;
    }

    @Override
    protected String getViewId() {
        return "transferView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("transferList");
    }

    public Double getDistanceKM() {
        Double val = distance;
        if (val == null) {
            val = 0d;
        }
        return val / 1000;
    }

    public Integer getTimeMunite() {
        Integer val = time;
        if (val == null) {
            val = 0;
        }
        return val / 60;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public List<TransferPrice> getTransferPriceList() {
        return transferPriceList;
    }

    public void setTransferPriceList(List<TransferPrice> transferPriceList) {
        this.transferPriceList = transferPriceList;
    }
}
