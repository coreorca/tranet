package org.orcateam.tranet.web.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.service.UserService;
import org.orcateam.tranet.core.util.SessionUtil;
import org.orcateam.tranet.web.security.AuthUser;
import org.orcateam.tranet.web.security.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class OrcaAuthProviderService extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    UserService userService;

    Logger logger = Logger.getLogger(OrcaAuthProviderService.class);

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken token) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken token) throws AuthenticationException {
        try {
            String password = DigestUtils.md5Hex((String) token.getCredentials());
            User user = userService.login(username, password);
            if (user == null) {
                throw new BadCredentialsException("user not found");
            }
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            Set<String> privilegeSet = new HashSet<String>();
            for (Privilege privilege : user.getRole().getPrivilegeSet()) {
                authorities.add(new SimpleGrantedAuthority(privilege.getName()));
                privilegeSet.add(privilege.getName());
            }
            return new AuthUser(user, authorities);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BadCredentialsException(e.getMessage());
        }
    }
}
