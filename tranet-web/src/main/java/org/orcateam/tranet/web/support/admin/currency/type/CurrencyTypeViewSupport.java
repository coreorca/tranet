package org.orcateam.tranet.web.support.admin.currency.type;

import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseViewSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class CurrencyTypeViewSupport extends BaseViewSupport<CurrencyType, Integer> {

    @Autowired
    transient CurrencyTypeService entityService;

    @Override
    @Secured("admin.currency.type.view")
    public void initView(Integer instanceId) {
        CurrencyType result = entityService.get(instanceId);
        setInstance(result);
    }

    @Secured("admin.currency.type.update")
    public String navToEdit() {
        NavRule rule = new NavRule("currencyTypeEdit");
        rule.addParam("id", instanceId.toString());
        return nav.redirect(rule, navId.getValue());
    }

    @Override
    protected String getViewId() {
        return "currencyTypeView}";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("currencyTypeList");
    }

}
