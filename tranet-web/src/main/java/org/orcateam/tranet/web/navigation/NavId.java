package org.orcateam.tranet.web.navigation;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;

public class NavId implements Serializable {

    private Long value;
    private Date expiryDate;

    //this rule is being used to hold lastNav's data for back() method.
    private NavRule lastPoppedNav;

    private LinkedList<NavRule> ruleLinkedList = new LinkedList<NavRule>();

    public NavId(Long value) {
        this.value = value;
        this.expiryDate = new Date();
    }

    public void push(NavRule rule) {
        try {
            refresh();
            NavRule last = ruleLinkedList.peekFirst();
            if (last != null && last.getHash().equals(rule.getHash())) {
                return;
            }
            if (lastPoppedNav != null) {
                rule.setSelectedTab(lastPoppedNav.getSelectedTab());
            }
            ruleLinkedList.push(rule);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NavRule pop() {
        try {
            refresh();
            ruleLinkedList.pop();
            NavRule lastNav = ruleLinkedList.pop();
            lastPoppedNav = lastNav;
            return lastNav;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public NavRule peekFirst() {
        try {
            refresh();
            return ruleLinkedList.peekFirst();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void clean() {
        refresh();
        ruleLinkedList.clear();
    }

    public void refresh() {
        expiryDate = new Date();
    }

    public Long getValue() {
        return value;
    }

    public boolean isExpired() {
        boolean expired = false;
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(expiryDate);
        calendar.add(Calendar.MINUTE, NavUtil.MAX_NAV_EXPIRE_TIME);
        if (calendar.getTime().before(new Date())) {
            expired = true;
        }
        return expired;
    }
}
