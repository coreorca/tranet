package org.orcateam.tranet.web.components;

import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.enumeration.RouteTypeEnum;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.service.CityService;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.core.service.PointService;
import org.orcateam.tranet.core.service.TransferService;
import org.orcateam.tranet.web.BaseBean;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ManagedBean
@ViewScoped
public class RouteSelectorComponent extends BaseBean {

    @NotNull
    private RouteTypeEnum routeType = null;

    @NotNull
    private Country country = null;

    @NotNull
    private City city = null;

    @NotNull
    private Point airport = null;

    @NotNull
    private Point region = null;

    private List<RouteTypeEnum> routeTypeEnumList = null;
    private List<Country> countryList = null;
    private List<City> cityList = null;
    private List<Point> airportList = null;
    private List<Point> regionList = null;

    private List<Transfer> transferList = null;

    @Autowired
    transient CountryService countryService;

    @Autowired
    transient CityService cityService;

    @Autowired
    transient TransferService transferService;

    @Autowired
    transient PointService pointService;

    public void reset() {
        country = null;
        city = null;
        airport = null;
        region = null;
        routeType = null;
        countryList = null;
        cityList = null;
        airportList = null;
        regionList = null;
        routeTypeEnumList = null;
    }

    public void init() {
        routeTypeEnumList = Arrays.asList(RouteTypeEnum.values());
        initCountryList();
        initCityList();
        initAirportList();
        initRegionList();

    }

    public void init(RouteTypeEnum routeType, Country country, City city, Point from, Point to) {
        this.routeType = routeType;
        this.country = country;
        this.city = city;
        if(RouteTypeEnum.AIRPORT_TO_REGION.equals(routeType)) {
            this.airport = from;
            this.region = to;
        }
        else {
            this.airport = to;
            this.region = from;
        }
        init();
    }

    private void initCountryList() {
        countryList = countryService.getAll();
    }

    private void initCityList() {
        cityList = null;
        if (country != null) {
            cityList = cityService.getListByCountryId(country.getId());
            if (cityList.isEmpty()) {
                WebUtil.addErrorMessage("component.routeSelector.noAvailableCitySelectedCountry");
            }
        }
    }

    private void initAirportList() {
        airportList = null;
        if (city != null) {
            airportList = pointService.getByCityAndType(city.getId(), PointTypeEnum.AIRPORT);
            if (airportList.isEmpty()) {
                WebUtil.addErrorMessage("component.routeSelector.noAvailableAirportSelectedCity");
            }
        }
    }

    private void initRegionList() {
        regionList = new ArrayList<Point>();
        if (airport != null) {
            transferList = transferService.getAvailableTransferListByAirport(airport);
            for (Transfer transfer : transferList) {
                regionList.add(transfer.getRegion());
            }
            if (regionList.isEmpty()) {
                WebUtil.addErrorMessage("component.routeSelector.noAvailableRegionSelectedAirport");
            }
        }
    }

    public void countryAjaxChangeListener() {
        city = null;
        airport = null;
        region = null;
        initCityList();
        initAirportList();
        initRegionList();
    }

    public void cityAjaxChangeListner() {
        airport = null;
        region = null;
        initAirportList();
        initRegionList();
    }

    public void airportAjaxChangeListener() {
        region = null;
        initRegionList();
    }

    public Transfer getTransfer() {
        Transfer transfer = null;
        for (Transfer t : transferList) {
            if (t.getAirport().getId().equals(airport.getId()) && t.getRegion().getId().equals(region.getId())) {
                transfer = t;
                break;
            }
        }
        return transfer;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Point getAirport() {
        return airport;
    }

    public void setAirport(Point airport) {
        this.airport = airport;
    }

    public Point getRegion() {
        return region;
    }

    public void setRegion(Point region) {
        this.region = region;
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public List<Point> getAirportList() {
        return airportList;
    }

    public void setAirportList(List<Point> airportList) {
        this.airportList = airportList;
    }

    public List<Point> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<Point> regionList) {
        this.regionList = regionList;
    }

    public RouteTypeEnum getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteTypeEnum routeType) {
        this.routeType = routeType;
    }

    public List<RouteTypeEnum> getRouteTypeEnumList() {
        return routeTypeEnumList;
    }

    public void setRouteTypeEnumList(List<RouteTypeEnum> routeTypeEnumList) {
        this.routeTypeEnumList = routeTypeEnumList;
    }
}
