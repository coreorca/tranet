package org.orcateam.tranet.web.support.admin.invoice;

import org.orcateam.tranet.core.dao.parameter.InvoiceParameter;
import org.orcateam.tranet.core.model.invoice.Invoice;
import org.orcateam.tranet.core.service.InvoiceService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class InvoiceListSupport extends BaseListSupport<Invoice, Integer> {

    private InvoiceParameter params = new InvoiceParameter();

    @Autowired
    transient InvoiceService entityService;

    @Override
    @Secured("admin.invoice.list")
    protected void initResultList() {
        try {
            Pagination<Invoice> pagination = entityService.findWithPagination(getPager(), params);
            applyResult(pagination);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void reset() {
        params = new InvoiceParameter();
        super.reset();
    }

    @Override
    @Secured("admin.incoive.view")
    public String navToView(Invoice instance) {
        NavRule navToView = new NavRule("invoiceView");
        navToView.addParam("id", instance.getId().toString());
        return nav.redirect(navToView, getNavId());
    }

    @Override
    @Secured("admin.invoice.create")
    public String navToCreate() {
        NavRule navToAdd = new NavRule("invoiceWizard");
        return nav.redirect(navToAdd, getNavId());
    }

    @Override
    protected String getViewId() {
        return "invoiceList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    public InvoiceParameter getParams() {
        return params;
    }

    public void setParams(InvoiceParameter params) {
        this.params = params;
    }
}
