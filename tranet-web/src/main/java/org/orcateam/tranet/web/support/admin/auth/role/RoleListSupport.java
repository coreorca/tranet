package org.orcateam.tranet.web.support.admin.auth.role;

import org.orcateam.tranet.core.dao.parameter.RoleParameter;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.core.util.Pagination;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseListSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class RoleListSupport extends BaseListSupport<Role, Integer> {

    private RoleParameter roleParameter = new RoleParameter();

    @Autowired
    transient RoleService roleService;

    @Override
    @Secured("admin.auth.role.list")
    protected void initResultList() {
        try {
            Pagination<Role> pagination = roleService.findWithPagination(getPager(), roleParameter);
            applyResult(pagination);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected String getViewId() {
        return "roleList";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return null;
    }

    @Override
    @Secured("admin.auth.role.create")
    public String navToCreate() {
        NavRule navToAdd = new NavRule("roleEdit");
        return nav.redirect(navToAdd, getNavId());
    }

    @Override
    @Secured("admin.auth.role.view")
    public String navToView(Role role) {
        NavRule navToView = new NavRule("roleView");
        navToView.addParam("instanceId", role.getId().toString());
        return nav.redirect(navToView, getNavId());
    }

    public RoleParameter getRoleParameter() {
        return roleParameter;
    }

    public void setRoleParameter(RoleParameter roleParameter) {
        this.roleParameter = roleParameter;
    }
}
