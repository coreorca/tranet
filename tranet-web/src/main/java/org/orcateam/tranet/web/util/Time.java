package org.orcateam.tranet.web.util;

import java.io.Serializable;

public class Time implements Serializable {

    private Integer hour = 0;
    private Integer minute = 0;

    public Time() {
    }

    public Time(Integer hour, Integer minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return TimeUtil.toStringTime(this);
    }
}

