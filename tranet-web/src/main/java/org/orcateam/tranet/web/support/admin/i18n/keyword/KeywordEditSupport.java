package org.orcateam.tranet.web.support.admin.i18n.keyword;

import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.service.KeywordService;
import org.orcateam.tranet.web.exception.NotFoundException;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.security.Secured;
import org.orcateam.tranet.web.support.BaseEditSupport;
import org.orcateam.tranet.web.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class KeywordEditSupport extends BaseEditSupport<Keyword, Integer> {

    @Autowired
    transient KeywordService entityService;

    @Override
    @Secured("admin.i18n.keyword.create")
    public void initCreate() {
        Keyword entity = new Keyword();
        setInstance(entity);
    }

    @Override
    @Secured("admin.i18n.keyword.update")
    public void initUpdate(Integer instanceId) throws Exception {
        Keyword entity = entityService.get(instanceId);
        setInstance(entity);
        if (entity == null) {
            throw new NotFoundException();
        }
    }

    @Override
    public String persist() {
        try {
            Keyword persisted = entityService.save(instance);
            WebUtil.addInfoMessage("global.messages.create.success");
            return navToView(persisted.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String update() {
        try {
            entityService.update(instance);
            WebUtil.addInfoMessage("global.messages.update.success");
            return navToView(instance.getId());
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public String remove() {
        try {
            entityService.remove(instance);
            WebUtil.addInfoMessage("global.messages.remove.success");
            return navToList();
        } catch (Exception e) {
            WebUtil.addExceptionError(e);
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean isManaged() {
        return getInstance().getId() != null;
    }

    @Override
    protected String getViewId() {
        return "keywordEdit}";
    }

    public String navToList() {
        NavRule rule = new NavRule("keywordList");
        return nav.cleanRedirect(rule, navId.getValue());
    }

    public String navToView(Integer id) {
        NavRule rule = new NavRule("keywordView");
        rule.addParam("id", id.toString());
        return nav.cleanRedirect(rule, navId.getValue());
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        if (instanceId == null) {
            return new NavRule("keywordList");
        } else {
            NavRule rule = new NavRule("keywordView");
            rule.addParam("id", instanceId.toString());
            return rule;
        }
    }


}
