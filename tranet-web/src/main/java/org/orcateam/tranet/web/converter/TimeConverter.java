package org.orcateam.tranet.web.converter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.orcateam.tranet.web.util.Time;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "timeConverter")
public class TimeConverter implements Converter {

    static Logger logger = Logger.getLogger(TimeConverter.class);

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            String[] timeArray = StringUtils.split(s, ":");
            if (timeArray.length < 2) {
                throw new ConverterException("time format is hh:mm");
            }
            Integer hour = Integer.valueOf(timeArray[0]);
            Integer munite = Integer.valueOf(timeArray[1]);
            if (hour < 0 || hour > 23) {
                throw new ConverterException("hour value must be 0-23");
            }
            if (munite < 0 || munite > 59) {
                throw new ConverterException("munite value must be 0 - 59");
            }
            return new Time(hour, munite);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ConverterException(e);
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return o.toString();
    }
}
