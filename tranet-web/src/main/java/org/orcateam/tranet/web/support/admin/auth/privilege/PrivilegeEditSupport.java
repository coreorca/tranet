package org.orcateam.tranet.web.support.admin.auth.privilege;

import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.web.navigation.NavRule;
import org.orcateam.tranet.web.support.BaseEditSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class PrivilegeEditSupport extends BaseEditSupport<Privilege, Integer> {


    @Override
    public void initCreate() throws Exception {
        Privilege privilege = new Privilege();
        setInstance(privilege);
    }

    @Override
    public void initUpdate(Integer instanceId) throws Exception {

    }

    @Override
    public String persist() {
        System.out.println(instance.getName());
        return null;
    }

    @Override
    public String update() {
        return null;
    }

    @Override
    public String remove() {
        return null;
    }

    @Override
    public boolean isManaged() {
        return instance.getId() != null;
    }

    @Override
    protected String getViewId() {
        return "privilegeEdit";
    }

    @Override
    protected NavRule getDefaultBackNavRule() {
        return new NavRule("privilegeList");
    }

}
