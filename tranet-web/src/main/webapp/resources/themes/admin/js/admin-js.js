var edited = false;

var setEdited = function () {
    edited = true;
}

var setNotEdited = function () {
    edited = false;
}

var isEdited = function () {
    if (edited == true) {
        if (confirm('Are you sure you want to leave this page before unsaved data?')) {
            edited = false;
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return true;
    }
}