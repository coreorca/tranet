var marker;
var map;
function initialize() {
    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(36.885238, 30.702570)
    };

    map = new google.maps.Map(document.getElementById('google-map'),
        mapOptions);

    marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        title: 'Marker',
        draggable: true,
        animation: google.maps.Animation.DROP
    });

    google.maps.event.addListener(marker, "dragend", function (event) {
        var point = marker.getPosition();
        map.panTo(point);
        updateCoordinates(point);
    });

    google.maps.event.addListener(map, "rightclick", function (event) {
        marker.setPosition(event.latLng);
        var point = marker.getPosition();
        updateCoordinates(point);
    });

    loadPosition();

    disableRightClickMenu();
}

function setMarkerPosition(data) {
    var spdata = data.split(",");
    var newLatLng = new google.maps.LatLng(parseFloat(spdata[0]), parseFloat(spdata[1]));
    marker.setPosition(newLatLng);
    map.panTo(newLatLng);
}

google.maps.event.addDomListener(window, 'load', initialize);

var disableRightClickMenu = function() {
    document.onmousedown = function (event) {
        if (event.button == 2) {
            return false;
        }
    }
}