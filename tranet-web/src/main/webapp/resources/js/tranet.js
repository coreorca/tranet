jQuery(function () {
    reloadPage();
    initDefaultTheme();
});

var ajaxErrorFunction = function (event) {
    initDefaultTheme();
};

var reloadPage = function () {
    var isReload = $('#isRefreshView').val();
    if (isReload == "true") {
        $('.btn').attr("disabled", "disabled");
        $('#isRefreshView').val("false");
        isReload = false;
        location.reload();
    }
    else {
        $('#isRefreshView').val("true");
    }
};

var initDefaultTheme = function () {
    initThemeAlert();
    initTimePicker();
    initDatePicker();
    initMonthPicker();
    initYearPicker();
    initMoneyInput();
    initPhoneNumberInput();
    initIbanInput();
    stopAjaxAction();
};

var startAjaxAction = function () {
    NProgress.start();
    $('.btn').attr("disabled", "disabled");
    $('.datepicker').attr("disabled", "disabled");
    $('.monthpicker').attr("disabled", "disabled");
    $('.yearpicker').attr("disabled", "disabled");
};

var stopAjaxAction = function () {
    NProgress.done();
    $('.btn').removeAttr("disabled");
    $('.datepicker').removeAttr("disabled");
    $('.monthpicker').removeAttr("disabled");
    $('.yearpicker').removeAttr("disabled");
};

var initThemeAlert = function () {
    $(".-th-default-alert-block").click(function () {
        $(".alert").hide("slow");
    });
};

var initTimePicker = function () {
    $('.timepicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.timepicker').mask("00:00", {maxlength: true});
};

var initDatePicker = function () {

    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        todayBtn: true
    });

    $('.datepicker').mask('##/##/####', {maxlength: true});
};

var initMonthPicker = function () {
    $('.monthpicker').datepicker({
        format: "MM yyyy",
        minViewMode: 1,
        autoclose: true,
        todayHighlight: true,
        todayBtn: true,
        forceParse: false
    });
};

var initYearPicker = function () {
    $('.yearpicker').datepicker({
        format: "yyyy",
        autoclose: true,
        todayHighlight: true,
        todayBtn: true,
        forceParse: false,
        minViewMode: 2
    });
    $('.yearpicker').mask('####', {maxlength: true});
};

var showDatePicker = function (comp) {
    $(comp).datepicker('show');
};


var initMoneyInput = function () {
    $('.moneyInput').mask("#.###.###.##0,00", {reverse: true});
    $('.moneyInput').click(function () {
        var value = parseInt($(this).val());
        if (value == 0) {
            $(this).select();
        }
    });
};

var initPhoneNumberInput = function () {
    $('.phoneNumberInput').mask("(###) ### ## ##", {reverse: false, minmaxlength: 10});
    $('.phoneNumberInput').click(function () {
        var value = parseInt($(this).val());
        if (value == 0) {
            $(this).select();
        }
    });
};

var initIbanInput = function () {
    $('.ibanInput').mask('AA00 0000 0000 0000 0000 0000 00');
};
