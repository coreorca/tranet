var map;


function initialize() {
    var mapOptions = {
        zoom: 8,
        center: new google.maps.LatLng(36.885238, 30.702570)
    };
    map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
    loadGeoList();

}


function updateGeoList(data) {

    var latlngbounds = new google.maps.LatLngBounds();
    var lat_lng = new Array();

    map.clearMarkers();
    map.clear

    var path = new google.maps.MVCArray();

    var service = new google.maps.DirectionsService();

    var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });

    data.forEach(function (c) {
        var spdata = c.split(",");
        var newLatLng = new google.maps.LatLng(parseFloat(spdata[0]), parseFloat(spdata[1]));
        map.panTo(newLatLng);
        lat_lng.push(newLatLng);

        var marker = new google.maps.Marker({
            position: newLatLng,
            map: map,
            title: 'Marker',
            draggable: false,
            animation: google.maps.Animation.DROP
        });

        latlngbounds.extend(marker.position);

    });

    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);


    for (var i = 0; i < lat_lng.length; i++) {
        if ((i + 1) < lat_lng.length) {
            var src = lat_lng[i];
            var des = lat_lng[i + 1];
            path.push(src);
            poly.setPath(path);
            service.route({
                origin: src,
                destination: des,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            }, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    var dist = parseFloat(result.routes[0].legs[0].distance.value);
                    var time = parseInt(result.routes[0].legs[0].duration.value);
                    setDistanceAndTime(dist, time);
                    for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                        path.push(result.routes[0].overview_path[i]);
                    }
                }
            });
        }
    }
}


google.maps.event.addDomListener(window, 'load', initialize);
google.maps.Map.prototype.markers = new Array();
google.maps.Map.prototype.addMarker = function (marker) {
    this.markers[this.markers.length] = marker;
};

google.maps.Map.prototype.getMarkers = function () {
    return this.markers
};

google.maps.Map.prototype.clearMarkers = function () {
    for (var i = 0; i < this.markers.length; i++) {
        this.markers[i].setMap(null);
    }
    this.markers = new Array();
};