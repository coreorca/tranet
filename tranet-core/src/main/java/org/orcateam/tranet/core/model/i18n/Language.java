package org.orcateam.tranet.core.model.i18n;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LANGUAGE")
public class Language extends BaseEntity {

    private Code code = new Code();
    private String name;
    private String locale;


    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotEmpty
    @Length(max = 16)
    @Column(name = "NAME", length = 16, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotEmpty
    @Length(max = 16)
    @Column(name = "LOCALE", length = 16, nullable = false)
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
