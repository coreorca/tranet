package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.location.Country;

public interface CountryDao extends GenericDao<Country, Integer> {


}
