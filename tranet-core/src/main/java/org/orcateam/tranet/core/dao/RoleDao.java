package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.auth.Role;

import java.util.List;

public interface RoleDao extends GenericDao<Role, Integer> {
    public Role findEagerlyById(Integer id);

    public List<Role> findAll();

    public Role findForRoleFilter(Integer id, String name, String excludeName);
}
