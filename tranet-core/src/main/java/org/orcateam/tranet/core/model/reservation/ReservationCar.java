package org.orcateam.tranet.core.model.reservation;

import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.model.embeddable.Money;
import org.orcateam.tranet.core.model.provider.Provider;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "RESERVATION_CAR")
public class ReservationCar extends BaseEntity {

    private Reservation reservation;

    private CarType carType;
    private Provider provider;

    private Money price = new Money();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RESERVATION_ID", nullable = false)
    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAR_TYPE_ID", nullable = false)
    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVIDER_ID", nullable = true)
    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }


    @Embedded
    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }


}
