package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.agency.DiscountGroup;

public interface DiscountGroupService extends GenericService<DiscountGroup, Integer> {

}
