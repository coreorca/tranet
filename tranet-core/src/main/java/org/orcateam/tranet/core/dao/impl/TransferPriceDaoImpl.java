package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.orcateam.tranet.core.dao.TransferPriceDao;
import org.orcateam.tranet.core.model.transfer.TransferPrice;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TransferPriceDaoImpl extends GenericDaoImpl<TransferPrice, Integer> implements TransferPriceDao {

    @Override
    public List<TransferPrice> findEagerlyByTransferId(Integer transferId) {
        Criteria criteria = getSession().createCriteria(TransferPrice.class, "price");
        criteria.createAlias("transfer", "transfer", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("carType", "carType", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("transfer.id", transferId));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

}
