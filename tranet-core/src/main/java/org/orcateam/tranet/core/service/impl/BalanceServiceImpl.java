package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.AgencyDao;
import org.orcateam.tranet.core.dao.BalanceDao;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.balance.Balance;
import org.orcateam.tranet.core.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BalanceServiceImpl extends GenericServiceImpl<Balance, Integer> implements BalanceService {

    @Autowired
    private BalanceDao entityDao;

    @Autowired
    private AgencyDao agencyDao;

    @Override
    protected GenericDao<Balance, Integer> getDao() {
        return entityDao;
    }

    @Override
    public void saveAndIncreaseAgencyBalance(Balance instance) {
        save(instance);

        Agency agency = instance.getAgency();
        Double value = agency.getMoney().getValue();
        value += instance.getMoney().getValue();
        agency.getMoney().setValue(value);

        agencyDao.save(agency);
    }
}
