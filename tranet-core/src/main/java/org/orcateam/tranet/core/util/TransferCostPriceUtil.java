package org.orcateam.tranet.core.util;

import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.model.transfer.TransferPrice;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TransferCostPriceUtil {

    public static Map<CarType, TransferPrice> getPriceMap(Set<TransferPrice> priceSet) {
        Map<CarType, TransferPrice> priceMap = new HashMap<CarType, TransferPrice>();
        for (TransferPrice price : priceSet) {
            priceMap.put(price.getCarType(), price);
        }
        return priceMap;
    }
}
