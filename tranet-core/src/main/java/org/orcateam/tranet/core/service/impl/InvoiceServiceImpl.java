package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.InvoiceDao;
import org.orcateam.tranet.core.model.invoice.Invoice;
import org.orcateam.tranet.core.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class InvoiceServiceImpl extends GenericServiceImpl<Invoice, Integer> implements InvoiceService {

    @Autowired
    InvoiceDao entityDao;

    @Override
    protected GenericDao<Invoice, Integer> getDao() {
        return entityDao;
    }

    @Override
    public Invoice findEagerlyById(Integer id) {
        return entityDao.findEagerlyById(id);
    }

}
