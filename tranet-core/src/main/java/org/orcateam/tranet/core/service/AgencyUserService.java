package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.model.auth.AgencyUser;

import java.util.List;

public interface AgencyUserService extends GenericService<AgencyUser, Integer> {


    public AgencyUser findEagerlyById(Integer agencyUserId);

    public List<AgencyUser> getAllByAgencyId(Integer agencyId);

    public AgencyUser saveNewUser(AgencyUser user, String password);

}
