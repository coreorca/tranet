package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.service.GenericService;
import org.orcateam.tranet.core.util.Pager;
import org.orcateam.tranet.core.util.Pagination;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
public abstract class GenericServiceImpl<E, PK extends Serializable> implements GenericService<E, PK> {

    protected abstract GenericDao<E, PK> getDao();

    @Override
    public Pagination<E> findWithPagination(Pager pager, Object param) {
        return getDao().findWithPagination(pager, param);
    }

    @Override
    public E get(PK instanceId) {
        return getDao().get(instanceId);
    }

    @Override
    public E save(E instance) {
        return getDao().save(instance);
    }

    @Override
    public void update(E instance) {
        getDao().update(instance);
    }

    @Override
    public void remove(E instance) {
        getDao().remove(instance);
    }

    @Override
    public List<E> getAll() {
        return getDao().getAll();
    }
}
