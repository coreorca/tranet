package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.TransferPriceDao;
import org.orcateam.tranet.core.model.transfer.TransferPrice;
import org.orcateam.tranet.core.service.TransferPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransferPriceServiceImpl extends GenericServiceImpl<TransferPrice, Integer> implements TransferPriceService {

    @Autowired
    TransferPriceDao entityDao;

    @Override
    protected GenericDao<TransferPrice, Integer> getDao() {
        return entityDao;
    }

    @Override
    public List<TransferPrice> findEagerlyByTransferId(Integer transferId) {
        return entityDao.findEagerlyByTransferId(transferId);
    }
}
