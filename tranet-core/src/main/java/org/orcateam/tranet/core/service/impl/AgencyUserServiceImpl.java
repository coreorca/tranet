package org.orcateam.tranet.core.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.orcateam.tranet.core.dao.AgencyUserDao;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.service.AgencyUserService;
import org.orcateam.tranet.core.util.RoleUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("agencyUserService")
public class AgencyUserServiceImpl extends GenericServiceImpl<AgencyUser, Integer> implements AgencyUserService {

    @Autowired
    AgencyUserDao agencyUserDao;

    @Override
    public AgencyUser findEagerlyById(Integer agencyUserId) {
        return agencyUserDao.findEagerlyById(agencyUserId);
    }

    @Override
    public List<AgencyUser> getAllByAgencyId(Integer agencyId) {
        return agencyUserDao.getAllByAgencyId(agencyId);
    }


    @Override
    public AgencyUser saveNewUser(AgencyUser user, String password) {
        if (password != null) {
            String hashedPassword = DigestUtils.md5Hex(password);
            user.setPassword(hashedPassword);
        }
        user.setRole(RoleUtil.getAgencyRole());
        return agencyUserDao.save(user);
    }

    @Override
    protected GenericDao<AgencyUser, Integer> getDao() {
        return agencyUserDao;
    }

}
