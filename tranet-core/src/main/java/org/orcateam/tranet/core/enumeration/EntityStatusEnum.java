package org.orcateam.tranet.core.enumeration;

public enum EntityStatusEnum {

    PASSIVE(0, "global.enum.entityStatus.passive"),
    ACTIVE(1, "global.enum.entityStatus.active");

    private Integer index;
    private String message;


    EntityStatusEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }
}
