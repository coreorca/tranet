package org.orcateam.tranet.core.model.reservation;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.enumeration.ReservationStatusEnum;
import org.orcateam.tranet.core.enumeration.ReservationTypeEnum;
import org.orcateam.tranet.core.enumeration.RouteTypeEnum;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.model.embeddable.Code;
import org.orcateam.tranet.core.model.embeddable.Money;
import org.orcateam.tranet.core.model.invoice.Invoice;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.location.Point;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "RESERVATION")
public class Reservation extends BaseEntity {

    private Code code = new Code();

    private User user;

    private RouteTypeEnum routeType;
    private Country country;
    private City city;
    private Point from;
    private Point to;

    private Invoice invoice;
    private ReservationTypeEnum type;

    private Money price;

    private Double discount;
    private Double discountRate;

    private Date date;
    private String flightNo;
    private Date flightDate;

    private Date pickupDate;

    private Integer pax;
    private String email;
    private String phone;
    private String pickupSignText;
    private String fullAddress;


    private ReservationStatusEnum reservationStatus;

    private Set<ReservationCar> reservationCarSet = new HashSet<ReservationCar>();

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ROUTE_TYPE", nullable = false)
    public RouteTypeEnum getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteTypeEnum routeType) {
        this.routeType = routeType;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_ID", nullable = false)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CITY_ID", nullable = false)
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FROM_ID", nullable = false)
    public Point getFrom() {
        return from;
    }

    public void setFrom(Point from) {
        this.from = from;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TO_ID", nullable = false)
    public Point getTo() {
        return to;
    }

    public void setTo(Point to) {
        this.to = to;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVOICE_ID")
    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "RESERVATION_TYPE", nullable = false)
    public ReservationTypeEnum getType() {
        return type;
    }

    public void setType(ReservationTypeEnum type) {
        this.type = type;
    }

    @Embedded
    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    @Column(name = "DISCOUNT", precision = 22, scale = 2, columnDefinition = "DECIMAL(22,2)")
    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    @Column(name = "DISCOUNT_RATE", precision = 3, scale = 2, columnDefinition = "DECIMAL(3,2)")
    public Double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESERVATION_DATE", nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @NotEmpty
    @Length(max = 16)
    @Column(name = "FLIGHT_NO", nullable = false, length = 16)
    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }


    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FLIGHT_DATE", nullable = false)
    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PICKUP_DATE", nullable = false)
    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    @NotNull
    @Column(name = "PAX", nullable = false)
    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    @Email
    @Length(max = 128)
    @Column(name = "E_MAIL", nullable = false, length = 128)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Column(name = "PHONE", length = 20, nullable = true)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @NotEmpty
    @Column(name = "PICKUP_SIGN_TEXT", length = 128, nullable = false)
    public String getPickupSignText() {
        return pickupSignText;
    }

    public void setPickupSignText(String pickupSignText) {
        this.pickupSignText = pickupSignText;
    }

    @NotEmpty
    @Column(name = "FULL_ADDRESS", length = 512, nullable = true)
    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "RESERVATION_STATUS", nullable = false)
    public ReservationStatusEnum getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatusEnum reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "reservation", orphanRemoval = true)
    public Set<ReservationCar> getReservationCarSet() {
        return reservationCarSet;
    }

    public void setReservationCarSet(Set<ReservationCar> reservationCars) {
        this.reservationCarSet = reservationCars;
    }
}
