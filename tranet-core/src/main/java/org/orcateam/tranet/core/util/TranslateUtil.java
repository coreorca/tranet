package org.orcateam.tranet.core.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class TranslateUtil {

    private static String[] trchars = {"ı", "ğ", "ü", "ş", "ö", "ç"};
    private static String[] enchars = {"i", "g", "u", "s", "o", "c"};

    public static String getHash(String word) {
        String wh = getCleanText(word);
        if (StringUtils.isNotBlank(wh)) {
            return DigestUtils.md5Hex(wh);
        }
        return null;
    }

    public static String getCleanText(String word) {
        if (StringUtils.isBlank(word)) {
            return null;
        }
        Locale locale = new Locale("tr", "TR");
        word = StringUtils.lowerCase(word, locale);
        word = StringUtils.replace(word, " ", "");
        word = StringUtils.replaceEach(word, trchars, enchars);
        word = word.replaceAll("[^A-Za-z0-9]", "");
        return word;
    }

}
