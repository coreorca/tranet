package org.orcateam.tranet.core.util;

import java.io.Serializable;

public class OrderBy implements Serializable {

    private String name;
    private boolean asc = true;

    public OrderBy() {
    }

    public OrderBy(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }
}
