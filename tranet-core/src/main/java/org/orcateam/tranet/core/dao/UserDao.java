package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.auth.CustomerUser;
import org.orcateam.tranet.core.model.auth.User;

import java.util.List;

public interface UserDao extends GenericDao<User, Integer> {

    public User login(String email, String password);

    public User findEagerlyById(Integer id);

    public List<CustomerUser> getAllCustomerUsers();
}
