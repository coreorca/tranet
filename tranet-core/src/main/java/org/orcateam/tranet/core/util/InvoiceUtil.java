package org.orcateam.tranet.core.util;

import org.orcateam.tranet.core.enumeration.InvoiceTypeEnum;
import org.orcateam.tranet.core.exception.OrcaException;
import org.orcateam.tranet.core.exception.PropertyNotFoundException;
import org.orcateam.tranet.core.model.invoice.AgencyInvoice;
import org.orcateam.tranet.core.model.invoice.IndividualInvoice;
import org.orcateam.tranet.core.model.invoice.Invoice;

public class InvoiceUtil {

    public static InvoiceTypeEnum getInvoiceType(Invoice invoice) throws OrcaException {
        if (invoice instanceof AgencyInvoice) {
            return InvoiceTypeEnum.AGENCY_INVOICE;
        } else if (invoice instanceof IndividualInvoice) {
            return InvoiceTypeEnum.INDIVIDUAL_INVOCE;
        }
        throw new PropertyNotFoundException();
    }

    public static Invoice getInvoice(InvoiceTypeEnum invoiceTypeEnum) throws OrcaException {
        if (invoiceTypeEnum.equals(InvoiceTypeEnum.AGENCY_INVOICE)) {
            return new AgencyInvoice();
        } else if (invoiceTypeEnum.equals(InvoiceTypeEnum.INDIVIDUAL_INVOCE)) {
            return new IndividualInvoice();
        }
        throw new PropertyNotFoundException();
    }
}
