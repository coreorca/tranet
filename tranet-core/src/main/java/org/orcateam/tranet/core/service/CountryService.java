package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.model.location.Country;

public interface CountryService extends GenericService<Country, Integer> {


    public Country saveWithKeyword(Country country, String keywordText);

    public void updateWithKeyword(Country country, String keywordText);

}
