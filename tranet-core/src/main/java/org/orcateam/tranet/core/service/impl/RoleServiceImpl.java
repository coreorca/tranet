package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.RoleDao;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("roleService")
public class RoleServiceImpl extends GenericServiceImpl<Role, Integer> implements RoleService {

    @Autowired
    RoleDao roleDao;

    @Override
    protected GenericDao<Role, Integer> getDao() {
        return roleDao;
    }

    @Override
    public Role findEagerlyById(Integer id) {
        return roleDao.findEagerlyById(id);
    }

    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }

    @Override
    public Role findForRoleFilter(Integer id, String name, String excludeName) {
        return roleDao.findForRoleFilter(id, name, excludeName);
    }
}
