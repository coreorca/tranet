package org.orcateam.tranet.core.model.transfer;

import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.model.embeddable.Money;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TRANSFER_PRICE", uniqueConstraints = @UniqueConstraint(name = "TRASNFER_PRICE_UNQ", columnNames = {"TRANSFER_ID", "CAR_TYPE_ID"}))
public class TransferPrice extends BaseEntity {

    private CarType carType;
    private Transfer transfer;
    private Money money = new Money();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAR_TYPE_ID", nullable = false)
    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSFER_ID", nullable = false)
    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    @Embedded
    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }
}
