package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.provider.Provider;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = Provider.class, alias = "provider")
public class ProviderParameter {

    @FieldInfo(compareMethod = CompareMethod.EQ, fieldName = "code.value")
    private String code = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String name = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String email = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String phone = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String fax = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
