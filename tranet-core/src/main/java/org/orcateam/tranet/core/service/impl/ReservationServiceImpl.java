package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.ReservationDao;
import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ReservationServiceImpl extends GenericServiceImpl<Reservation, Integer> implements ReservationService {

    @Autowired
    ReservationDao entityDao;

    @Override
    protected GenericDao<Reservation, Integer> getDao() {
        return entityDao;
    }

    public Reservation findWithId(Integer id) {
        return entityDao.findWithId(id);
    }

}
