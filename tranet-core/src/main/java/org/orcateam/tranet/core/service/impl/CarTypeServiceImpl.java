package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CarTypeServiceImpl extends GenericServiceImpl<CarType, Integer> implements CarTypeService {

    @Autowired
    CarTypeDao entityDao;

    @Override
    protected GenericDao<CarType, Integer> getDao() {
        return entityDao;
    }

    @Override
    public CarType findEagerlyById(Integer id) {
        return entityDao.findEagerlyById(id);
    }
}
