package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.car.CarType;

public interface CarTypeService extends GenericService<CarType, Integer> {

    public CarType findEagerlyById(Integer id);
}
