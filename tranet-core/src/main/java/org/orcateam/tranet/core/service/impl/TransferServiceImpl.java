package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.TransferDao;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TransferServiceImpl extends GenericServiceImpl<Transfer, Integer> implements TransferService {

    @Autowired
    TransferDao entityDao;

    @Override
    protected GenericDao<Transfer, Integer> getDao() {
        return entityDao;
    }


    @Override
    public Transfer getWithPoints(Integer transferId) {
        return entityDao.getWithPoints(transferId);
    }

    @Override
    public List<Transfer> getAvailableTransferListByAirport(Point airport) {
        return entityDao.getAvailableTransferListByAirport(airport);
    }
}
