package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.orcateam.tranet.core.dao.AgencyUserDao;
import org.orcateam.tranet.core.model.auth.AgencyUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AgencyUserDaoImpl extends GenericDaoImpl<AgencyUser, Integer> implements AgencyUserDao {

    @Override
    public List<AgencyUser> getAllByAgencyId(Integer agencyId) {
        Criteria criteria = getSession().createCriteria(AgencyUser.class, "user");
        criteria.createAlias("agency", "agency");
        criteria.add(Restrictions.eq("agency.id", agencyId));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public AgencyUser findEagerlyById(Integer agencyUserId) {
        Query query = getSession().createQuery("select agencyUser from AgencyUser agencyUser " +
                "left join fetch agencyUser.agency as agency " +
                "where agencyUser.id =:agencyUserId ");
        query.setParameter("agencyUserId", agencyUserId);
        AgencyUser result = (AgencyUser) query.uniqueResult();
        return result;
    }
}
