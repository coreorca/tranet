package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.i18n.Language;

public interface LanguageDao extends GenericDao<Language, Integer> {

    public Language getByCode(String code);

}
