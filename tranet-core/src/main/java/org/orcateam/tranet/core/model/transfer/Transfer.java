package org.orcateam.tranet.core.model.transfer;

import org.hibernate.validator.constraints.Length;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.model.location.Point;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TRANSFER")
public class Transfer extends BaseEntity {

    private Code code = new Code();
    private String description;

    private Country country;
    private City city;

    private Point airport;
    private Point region;

    private Set<TransferPrice> priceSet = new HashSet<TransferPrice>();

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @Length(max = 1024)
    @Column(name = "DESCRIPTION", nullable = true, length = 1024)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_ID", nullable = false)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CITY_ID", nullable = false)
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AIRPORT_ID", nullable = false)
    public Point getAirport() {
        return airport;
    }

    public void setAirport(Point airport) {
        this.airport = airport;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", nullable = false)
    public Point getRegion() {
        return region;
    }

    public void setRegion(Point region) {
        this.region = region;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "transfer", orphanRemoval = true)
    public Set<TransferPrice> getPriceSet() {
        return priceSet;
    }

    public void setPriceSet(Set<TransferPrice> priceSet) {
        this.priceSet = priceSet;
    }

}
