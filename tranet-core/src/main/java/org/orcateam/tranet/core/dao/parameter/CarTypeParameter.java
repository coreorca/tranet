package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.car.CarType;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

import java.io.Serializable;

@ClazzInfo(clazz = CarType.class, alias = "carType")
public class CarTypeParameter implements Serializable {

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String code;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String name;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private Integer pax;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private Integer trunkSize;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private Integer brand;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private Integer modelYear;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    public Integer getTrunkSize() {
        return trunkSize;
    }

    public void setTrunkSize(Integer trunkSize) {
        this.trunkSize = trunkSize;
    }

    public Integer getBrand() {
        return brand;
    }

    public void setBrand(Integer brand) {
        this.brand = brand;
    }

    public Integer getModelYear() {
        return modelYear;
    }

    public void setModelYear(Integer modelYear) {
        this.modelYear = modelYear;
    }
}
