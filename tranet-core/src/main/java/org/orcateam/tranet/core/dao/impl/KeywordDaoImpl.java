package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.KeywordDao;
import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.util.TranslateUtil;
import org.springframework.stereotype.Repository;


@Repository
public class KeywordDaoImpl extends GenericDaoImpl<Keyword, Integer> implements KeywordDao {


    @Override
    public Keyword getOrSave(String text) {
        String hash = TranslateUtil.getHash(text);
        Keyword keyword = getByHash(hash);
        if (keyword == null) {
            keyword = new Keyword();
            keyword.setText(text);
            keyword.setHash(hash);
            keyword = save(keyword);
        }
        return keyword;
    }

    @Override
    public Keyword getByHash(String hash) {
        Query q = getSession().createQuery("select kw from Keyword kw where kw.hash = :hash");
        q.setParameter("hash", hash);
        Keyword result = (Keyword) q.uniqueResult();
        return result;
    }

}
