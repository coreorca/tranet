package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.provider.Provider;

public interface ProviderDao extends GenericDao<Provider, Integer> {
    public Provider findEagerlyById(Integer id);
}
