package org.orcateam.tranet.core.enumeration;

public enum ReservationStatusEnum {

    WAITING(0, "enum.reservation.status.waiting"),
    CANCELED(1, "enum.reservation.status.canceled"),
    APPROVAL(2, "enum.reservation.status.approval"),
    CONFIRMED(3, "enum.reservation.status.confirmed"),
    NOT_CONFIRMED(4, "enum.reservation.status.not_confirmed");

    private Integer index;
    private String message;

    ReservationStatusEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }

}

    