package org.orcateam.tranet.core.model.auth;

import org.orcateam.tranet.core.model.provider.Provider;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PROVIDER_USER")
public class ProviderUser extends User {

    private Provider provider;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVIDER_ID", nullable = false)
    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
