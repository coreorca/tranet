package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.i18n.Translate;

public interface TranslateService extends GenericService<Translate, Integer> {

}
