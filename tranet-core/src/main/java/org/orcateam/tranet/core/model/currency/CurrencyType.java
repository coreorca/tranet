package org.orcateam.tranet.core.model.currency;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;

import javax.persistence.*;

@Entity
@Table(name = "CURRENCY_TYPE")
public class CurrencyType extends BaseEntity {


    private Code code = new Code();
    private String name;

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotEmpty
    @Length(max = 64)
    @Column(name = "NAME", length = 64, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
