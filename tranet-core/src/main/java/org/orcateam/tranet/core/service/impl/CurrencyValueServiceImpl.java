package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.CurrencyValueDao;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.service.CurrencyValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CurrencyValueServiceImpl extends GenericServiceImpl<CurrencyValue, Integer> implements CurrencyValueService {

    @Autowired
    CurrencyValueDao entityDao;

    @Override
    protected GenericDao<CurrencyValue, Integer> getDao() {
        return entityDao;
    }

    @Override
    public List<CurrencyValue> getAllWithType() {
        return entityDao.getAllWithType();
    }

    @Override
    public CurrencyValue getWithTypes(Integer valueId) {
        return entityDao.getWithTypes(valueId);
    }
}
