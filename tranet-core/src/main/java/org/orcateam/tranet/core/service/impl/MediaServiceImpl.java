package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.media.Media;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MediaServiceImpl extends GenericServiceImpl<Media, Integer> implements MediaService {

    @Autowired
    MediaDao entityDao;

    @Override
    protected GenericDao<Media, Integer> getDao() {
        return entityDao;
    }

}
