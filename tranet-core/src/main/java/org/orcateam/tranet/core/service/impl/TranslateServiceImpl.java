package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.TranslateDao;
import org.orcateam.tranet.core.model.i18n.Translate;
import org.orcateam.tranet.core.service.TranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TranslateServiceImpl extends GenericServiceImpl<Translate, Integer> implements TranslateService {

    @Autowired
    TranslateDao entityDao;

    @Override
    protected GenericDao<Translate, Integer> getDao() {
        return entityDao;
    }

}
