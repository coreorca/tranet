package org.orcateam.tranet.core.service.ext;


import org.orcateam.tcbmce.exception.TCMBCurrencyExporterException;
import org.orcateam.tranet.core.model.currency.CurrencyValue;

import java.util.Map;

public interface CurrencyValueTCMBImporter {

    public Map<String, CurrencyValue> getCurrencyValueMap() throws TCMBCurrencyExporterException;

}
