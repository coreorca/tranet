package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.i18n.Translate;

public interface TranslateDao extends GenericDao<Translate, Integer> {

}
