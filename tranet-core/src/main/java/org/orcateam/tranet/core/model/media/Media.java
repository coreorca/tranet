package org.orcateam.tranet.core.model.media;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;


@Entity
@Table(name = "MEDIA")
public class Media extends BaseEntity {

    private String name;
    private String extension;
    private String mimeType;

    private byte[] data;


    @Length(max = 128)
    @NotEmpty
    @Column(name = "NAME", nullable = false, length = 128)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotEmpty
    @Length(max = 4)
    @Column(name = "EXTENSION", length = 4, nullable = false)
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @NotEmpty
    @Length(max = 64)
    @Column(name = "MIME_TYPE", nullable = false, length = 64)
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Lob
    @Column(name = "DATA", nullable = false)
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
