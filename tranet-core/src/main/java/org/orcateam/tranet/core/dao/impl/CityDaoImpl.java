package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.CityDao;
import org.orcateam.tranet.core.model.location.City;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CityDaoImpl extends GenericDaoImpl<City, Integer> implements CityDao {

    @Override
    public City getWithCountry(Integer cityId) {
        Query query = getSession().createQuery("select city from City city " +
                "left join fetch city.country " +
                "where city.id = :cityId");
        query.setParameter("cityId", cityId);
        City result = (City) query.uniqueResult();
        return result;
    }

    @Override
    public List<City> getListByCountryId(Integer countryId) {
        Query query = getSession().createQuery("select city from City city where city.country.id = :countryId");
        query.setParameter("countryId", countryId);
        List<City> result = query.list();
        return result;
    }
}
