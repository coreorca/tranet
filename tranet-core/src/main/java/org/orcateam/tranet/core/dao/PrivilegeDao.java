package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.auth.Privilege;

import java.util.List;

public interface PrivilegeDao extends GenericDao<Privilege, Integer> {

    public List<Privilege> findAll();

    public List<Privilege> findForRoleFilter(String name, String excludeName);
}
