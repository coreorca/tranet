package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.provider.Provider;
import org.springframework.stereotype.Repository;


@Repository
public class ProviderDaoImpl extends GenericDaoImpl<Provider, Integer> implements ProviderDao {

    @Override
    public Provider findEagerlyById(Integer id) {
        Criteria criteria = getSession().createCriteria(Provider.class, "provider");
        criteria.createAlias("country", "country", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("city", "city", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("provider.id", id));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return (Provider) criteria.uniqueResult();
    }

}
