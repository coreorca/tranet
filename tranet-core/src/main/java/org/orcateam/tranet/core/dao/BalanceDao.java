package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.balance.Balance;

public interface BalanceDao extends GenericDao<Balance, Integer> {
}
