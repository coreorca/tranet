package org.orcateam.tranet.core.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.UserDao;
import org.orcateam.tranet.core.model.auth.CustomerUser;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl extends GenericServiceImpl<User, Integer> implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    protected GenericDao<User, Integer> getDao() {
        return userDao;
    }

    @Override
    public User findEagerlyById(Integer id) {
        return userDao.findEagerlyById(id);
    }

    @Override
    public User login(String email, String password) {
        return userDao.login(email, password);
    }

    @Override
    public List<CustomerUser> getAllCustomerUsers() {
        return userDao.getAllCustomerUsers();
    }

    @Override
    public User saveNewUser(User user, String password) {
        if (StringUtils.isNotBlank(password)) {
            String hashedPassword = DigestUtils.md5Hex(password);
            user.setPassword(hashedPassword);
        }
        return userDao.save(user);
    }

    @Override
    public void updateWithNewPassword(User user, String password) {
        if (StringUtils.isNotBlank(password)) {
            String hashedPassword = DigestUtils.md5Hex(password);
            user.setPassword(hashedPassword);
        }
        userDao.update(user);
    }
}
