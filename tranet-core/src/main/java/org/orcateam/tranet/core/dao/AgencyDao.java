package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.agency.Agency;

public interface AgencyDao extends GenericDao<Agency, Integer> {

    public Agency findEagerlyById(Integer id);

}
