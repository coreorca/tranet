package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.CurrencyTypeDao;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.springframework.stereotype.Repository;


@Repository
public class CurrencyTypeDaoImpl extends GenericDaoImpl<CurrencyType, Integer> implements CurrencyTypeDao {

    @Override
    public CurrencyType getCurrencyTypeByCode(String code) {
        Query query = getSession().createQuery("select currencyType from CurrencyType currencyType " +
                "where currencyType.code.value =:code ");
        query.setParameter("code", code);
        CurrencyType result = (CurrencyType) query.uniqueResult();
        return result;
    }
}
