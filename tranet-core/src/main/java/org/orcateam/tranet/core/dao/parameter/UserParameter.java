package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.enumeration.GenderEnum;
import org.orcateam.tranet.core.model.auth.User;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = User.class, alias = "user")
@Join(collection = "role", alias = "role", joinType = JoinType.LEFT_OUTER_JOIN)
public class UserParameter {

    @FieldInfo(compareMethod = CompareMethod.LIKE, fieldName = "name", alias = "role")
    private String roleName;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String name;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String surname;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String email;

    @FieldInfo(compareMethod = CompareMethod.LIKE)
    private GenderEnum gender;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String description;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
