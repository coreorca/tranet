package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.currency.CurrencyValue;

import java.util.List;

public interface CurrencyValueDao extends GenericDao<CurrencyValue, Integer> {

    public List<CurrencyValue> getAllWithType();

    public CurrencyValue getWithTypes(Integer valueId);

}
