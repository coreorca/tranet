package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.location.City;

import java.util.List;

public interface CityService extends GenericService<City, Integer> {

    public City getWithCountry(Integer cityId);

    public List<City> getListByCountryId(Integer countryId);

}
