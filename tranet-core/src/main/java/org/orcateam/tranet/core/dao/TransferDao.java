package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.transfer.Transfer;

import java.util.List;

public interface TransferDao extends GenericDao<Transfer, Integer> {

    public Transfer getWithPoints(Integer transferId);

    public List<Transfer> getAvailableTransferListByAirport(Point airport);

}
