package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.model.balance.Balance;

public interface BalanceService extends GenericService<Balance, Integer> {

    public void saveAndIncreaseAgencyBalance(Balance balance);

}
