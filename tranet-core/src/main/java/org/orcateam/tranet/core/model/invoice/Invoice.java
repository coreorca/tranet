package org.orcateam.tranet.core.model.invoice;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Money;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "INVOICE")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Invoice extends BaseEntity {

    private String no;
    private Date date;
    private Money money = new Money();

    @NotEmpty
    @Length(max = 32)
    @Column(name = "NO", length = 32, nullable = false)
    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INVOICE_DATE", nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Embedded
    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }
}
