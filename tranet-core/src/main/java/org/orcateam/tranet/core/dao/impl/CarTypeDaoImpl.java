package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.car.CarType;
import org.springframework.stereotype.Repository;


@Repository
public class CarTypeDaoImpl extends GenericDaoImpl<CarType, Integer> implements CarTypeDao {

    @Override
    public CarType findEagerlyById(Integer id) {
        Query query = getSession().createQuery("select carType from CarType carType " +
                "where carType.id =:id ");
        query.setParameter("id", id);
        CarType result = (CarType) query.uniqueResult();
        return result;
    }
}
