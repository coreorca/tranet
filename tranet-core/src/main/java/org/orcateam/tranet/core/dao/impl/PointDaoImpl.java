package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.PointDao;
import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.location.Point;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class PointDaoImpl extends GenericDaoImpl<Point, Integer> implements PointDao {

    @Override
    public Point getWithCity(Integer pointId) {
        Query query = getSession().createQuery("select point from Point point left join fetch point.city as city " +
                "left join fetch city.country as country " +
                "where point.id = :pointId");
        query.setParameter("pointId", pointId);
        Point result = (Point) query.uniqueResult();
        return result;
    }

    @Override
    public List<Point> getByCityAndType(Integer cityId, PointTypeEnum typeEnum) {
        Query query = getSession().createQuery("select point from Point point " +
                "where point.city.id = :cityId and point.type = :pointType ");
        query.setParameter("cityId", cityId);
        query.setParameter("pointType", typeEnum);
        List<Point> result = query.list();
        return result;
    }
}
