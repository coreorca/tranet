package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.currency.CurrencyValue;

import java.util.List;

public interface CurrencyValueService extends GenericService<CurrencyValue, Integer> {

    public List<CurrencyValue> getAllWithType();

    public CurrencyValue getWithTypes(Integer valueId);

}
