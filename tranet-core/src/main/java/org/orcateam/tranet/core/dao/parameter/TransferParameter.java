package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.transfer.Transfer;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.annotation.Joins;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = Transfer.class, alias = "transfer")
@Joins({
        @Join(collection = "airport", alias = "airport", joinType = JoinType.LEFT_OUTER_JOIN),
        @Join(collection = "region", alias = "region", joinType = JoinType.LEFT_OUTER_JOIN),
        @Join(collection = "country", alias = "country", joinType = JoinType.LEFT_OUTER_JOIN),
        @Join(collection = "city", alias = "city", joinType = JoinType.LEFT_OUTER_JOIN)
})
public class TransferParameter {


    @FieldInfo(compareMethod = CompareMethod.EQ, fieldName = "code.value")
    private String code;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
