package org.orcateam.tranet.core.model.balance;

import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.embeddable.Money;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "BALANCE")
public class Balance extends BaseEntity {

    private Agency agency;

    private Money money = new Money();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AGENCY_ID", nullable = false)
    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    @Embedded
    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }
}
