package org.orcateam.tranet.core.util;

import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.i18n.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Scope("session")
public class SessionUtil {

    private Language language;
    private CurrencyType currency;

    @Autowired
    Configurator configurator;

    @PostConstruct
    public void init() {
        language = configurator.getDefaultLanguage();
        currency = configurator.getDefaultCurrencyType();
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }
}
