package org.orcateam.tranet.core.enumeration;

public enum PointTypeEnum {

    REGION(0, "global.enum.point.region"),
    AIRPORT(1, "global.enum.point.airport");

    private Integer index;
    private String message;

    PointTypeEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }

}

    