package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.model.auth.Privilege;

import java.util.List;


public interface PrivilegeService extends GenericService<Privilege, Integer> {

    public List<Privilege> findAll();

    public List<Privilege> findForRoleFilter(String name, String excludeName);

}
