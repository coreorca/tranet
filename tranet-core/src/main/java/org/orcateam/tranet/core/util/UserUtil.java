package org.orcateam.tranet.core.util;

import org.orcateam.tranet.core.enumeration.UserTypeEnum;
import org.orcateam.tranet.core.exception.OrcaException;
import org.orcateam.tranet.core.exception.PropertyNotFoundException;
import org.orcateam.tranet.core.model.auth.*;

public class UserUtil {
    public static UserTypeEnum getUserType(User user) throws OrcaException {
        if (user.isSystemUser()) {
            return UserTypeEnum.SYSTEM_USER;
        } else  if (user.isAgencyUser()) {
            return UserTypeEnum.AGENCY_USER;
        } else  if (user.isProviderUser()) {
            return UserTypeEnum.PROVIDER_USER;
        } else  if (user.isCustomerUser()) {
            return UserTypeEnum.CUSTOMER_USER;
        }
        throw new PropertyNotFoundException();
    }

    public static User getUser(UserTypeEnum userTypeEnum) throws OrcaException {
        if (userTypeEnum.equals(UserTypeEnum.SYSTEM_USER)) {
            return new SystemUser();
        } else if (userTypeEnum.equals(UserTypeEnum.AGENCY_USER)) {
            return new AgencyUser();
        } else if (userTypeEnum.equals(UserTypeEnum.PROVIDER_USER)) {
            return new ProviderUser();
        } else if (userTypeEnum.equals(UserTypeEnum.CUSTOMER_USER)) {
            return new CustomerUser();
        }
        throw new PropertyNotFoundException();
    }
}
