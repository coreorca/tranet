package org.orcateam.tranet.core.config;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;


@Configuration
@EnableTransactionManagement
@PropertySources(value = {@PropertySource("classpath:/tranet-core.properties")})
public class DataSourceConfig {

    @Autowired
    Environment env;


    @Bean(destroyMethod = "close")
    public DataSource dataSource() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(env.getProperty("jdbc.username"));
        dataSource.setPassword(env.getProperty("jdbc.password"));
        dataSource.setJdbcUrl(env.getProperty("jdbc.url"));
        try {
            dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }

        // seting c3p0 properties
        dataSource.setUnreturnedConnectionTimeout(new Integer(env.getProperty("c3p0.unreturnedConnectionTimeout")));
        dataSource.setMaxStatements(new Integer(env.getProperty("c3p0.maxStatements")));
        dataSource.setMaxIdleTime(new Integer(env.getProperty("c3p0.maxIdleTime")));
        dataSource.setAcquireIncrement(new Integer(env.getProperty("c3p0.acquire_increment")));
        dataSource.setIdleConnectionTestPeriod(new Integer(env.getProperty("c3p0.idleConnectionTestPeriod")));
        dataSource.setMaxPoolSize(new Integer(env.getProperty("c3p0.maxPoolSize")));
        dataSource.setMinPoolSize(new Integer(env.getProperty("c3p0.minPoolSize")));
        dataSource.setTestConnectionOnCheckin(new Boolean(env.getProperty("c3p0.testConnectionOnCheckin")));
        dataSource.setPreferredTestQuery(env.getProperty("c3p0.testQueryString"));


        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[]{"org.orcateam.tranet.core.model"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }


    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }


    final Properties hibernateProperties() {
        return new Properties() {
            {
                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
                setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
                setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
//                setProperty("hibernate.default_schema", env.getProperty("hibernate.default_schema"));
            }
        };
    }


}
