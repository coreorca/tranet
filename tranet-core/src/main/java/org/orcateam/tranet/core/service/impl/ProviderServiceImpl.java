package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.provider.Provider;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProviderServiceImpl extends GenericServiceImpl<Provider, Integer> implements ProviderService {

    @Autowired
    ProviderDao entityDao;

    @Override
    protected GenericDao<Provider, Integer> getDao() {
        return entityDao;
    }

    @Override
    public Provider findEagerlyById(Integer id) {
        return entityDao.findEagerlyById(id);
    }
}
