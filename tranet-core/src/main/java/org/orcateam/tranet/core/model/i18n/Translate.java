package org.orcateam.tranet.core.model.i18n;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TRANSLATE")
public class Translate extends BaseEntity {

    private String text;
    private String langCode;
    private Language language;
    private Keyword keyword;

    @NotEmpty
    @Length(max = 2048)
    @Column(name = "TEXT", nullable = false, length = 2048)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToOne
    @JoinColumn(name = "LANGUAGE_ID", nullable = false)
    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @ManyToOne
    @JoinColumn(name = "KEYWORD_ID", nullable = false)
    public Keyword getKeyword() {
        return keyword;
    }

    public void setKeyword(Keyword keyword) {
        this.keyword = keyword;
    }

    @Column(name = "LANG_CODE", nullable = false)
    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}
