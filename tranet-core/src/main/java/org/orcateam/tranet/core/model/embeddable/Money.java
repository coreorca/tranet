package org.orcateam.tranet.core.model.embeddable;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.util.SessionUtil;
import org.orcateam.tranet.core.util.SpringContext;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class Money implements Serializable {

    private Double value = 0d;
    private CurrencyType currencyType;

    public Money() {

    }

    public Money(Double value, CurrencyType currencyType) {
        this.value = value;
        this.currencyType = currencyType;
    }

    @NotNull
    @Column(name = "MONEY_VALUE", nullable = false, precision = 22, scale = 2, columnDefinition = "DECIMAL(22,2)")
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "MONEY_CURRENCY_TYPE_ID", nullable = false)
    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    @Transient
    public Money getConverted() {
        SessionUtil sessionUtil = (SessionUtil) SpringContext.getContext().getBean("sessionUtil");
        Money money = new Money();
        money.setCurrencyType(sessionUtil.getCurrency());
        money.setValue(value);
        return money;
    }

    @Transient
    public void add(Double addValue) {
        value += addValue;
    }

    @Override
    public String toString() {
        if (Hibernate.isInitialized(currencyType)) {
            return value.toString() + " " + currencyType.getCode();
        } else {
            return value.toString();
        }
    }
}
