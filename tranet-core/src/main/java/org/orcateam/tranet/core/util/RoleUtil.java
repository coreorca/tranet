package org.orcateam.tranet.core.util;

import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.RoleService;

public class RoleUtil {

    public static Role getAgencyRole() {
        RoleService roleService = (RoleService)SpringContext.getContext().getBean("roleService");
        Role role = roleService.findEagerlyById(2);
        return role;
    }
}
