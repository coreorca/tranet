package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.enumeration.PaymentTypeEnum;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.annotation.Joins;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;

@ClazzInfo(clazz = Agency.class, alias = "agency")
@Joins({
        @Join(collection = "discountGroup", alias = "discountGroup")
})
public class AgencyParameter {

    @FieldInfo(fieldName = "code.value")
    private String code = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE)
    private String name = null;

    @FieldInfo
    private PaymentTypeEnum paymentType = null;

    @FieldInfo
    private DiscountGroup discountGroup = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PaymentTypeEnum getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    public DiscountGroup getDiscountGroup() {
        return discountGroup;
    }

    public void setDiscountGroup(DiscountGroup discountGroup) {
        this.discountGroup = discountGroup;
    }
}
