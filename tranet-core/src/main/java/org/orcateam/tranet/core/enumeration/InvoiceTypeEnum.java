package org.orcateam.tranet.core.enumeration;

import org.orcateam.tranet.core.exception.OrcaException;
import org.orcateam.tranet.core.exception.PropertyNotFoundException;

public enum InvoiceTypeEnum {
    AGENCY_INVOICE("global.enum.invoiceType.agencyInvoice", 1),
    INDIVIDUAL_INVOCE("global.enum.invoiceType.individualInvoice", 2);

    private String message;
    private Integer level;

    InvoiceTypeEnum(String message, Integer level) {
        this.message = message;
        this.level = level;
    }

    public static InvoiceTypeEnum resolveFrom(String name) throws OrcaException {
        for (InvoiceTypeEnum typeEnum : InvoiceTypeEnum.values()) {
            if (typeEnum.toString().equals(name)) {
                return typeEnum;
            }
        }
        throw new PropertyNotFoundException();
    }

    public static InvoiceTypeEnum resolveFrom(Integer level) throws OrcaException {
        for (InvoiceTypeEnum typeEnum : InvoiceTypeEnum.values()) {
            if (typeEnum.getLevel().equals(level)) {
                return typeEnum;
            }
        }
        throw new PropertyNotFoundException();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
