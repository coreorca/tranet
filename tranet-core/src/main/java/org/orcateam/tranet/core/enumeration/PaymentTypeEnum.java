package org.orcateam.tranet.core.enumeration;

public enum PaymentTypeEnum {

    CASH(1, "global.enum.paymentType.cash"),
    PREPAID(2, "global.enum.paymentType.prepaid"),
    CREDIT_CARD(3, "global.enum.paymentType.creditCard"),
    PAYPAL(4, "global.enum.paymentType.paypal");

    private Integer index;
    private String message;


    PaymentTypeEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }
}
