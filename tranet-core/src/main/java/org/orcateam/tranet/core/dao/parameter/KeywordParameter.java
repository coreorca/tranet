package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = Keyword.class, alias = "keyword")
@Join(collection = "translateMap", alias = "translate", joinType = JoinType.LEFT_OUTER_JOIN)
public class KeywordParameter {


    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String text = null;

    @FieldInfo(fieldName = "langCode", alias = "translate")
    private String code;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
