package org.orcateam.tranet.core.enumeration;

public enum GenderEnum {

    MALE(0, "global.enum.gender.male"),
    FAMALE(1, "global.enum.gender.female");

    private Integer index;
    private String message;

    GenderEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }
}
