package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DiscountGroupServiceImpl extends GenericServiceImpl<DiscountGroup, Integer> implements DiscountGroupService {

    @Autowired
    DiscountGroupDao entityDao;

    @Override
    protected GenericDao<DiscountGroup, Integer> getDao() {
        return entityDao;
    }

}
