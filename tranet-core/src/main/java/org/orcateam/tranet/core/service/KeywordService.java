package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.i18n.Keyword;

import java.util.List;

public interface KeywordService extends GenericService<Keyword, Integer> {

    public Keyword getByHash(String hash);

    public Keyword getOrSave(String text);

    public void update(List<Keyword> keywordList);

}
