package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;

@ClazzInfo(clazz = DiscountGroup.class, alias = "discountGroup")
public class DiscountGroupParameter {

    @FieldInfo(fieldName = "code.value")
    private String code = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE)
    private String name = null;

    @FieldInfo
    private Double discountRate = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }
}
