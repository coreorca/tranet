package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;

@ClazzInfo(clazz = Reservation.class, alias = "reservation")
public class ReservationParameter {

}
