package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.location.Point;

import java.util.List;

public interface PointDao extends GenericDao<Point, Integer> {

    public Point getWithCity(Integer pointId);

    public List<Point> getByCityAndType(Integer cityId, PointTypeEnum typeEnum);

}
