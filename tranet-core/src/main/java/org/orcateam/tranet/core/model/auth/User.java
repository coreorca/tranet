package org.orcateam.tranet.core.model.auth;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.orcateam.tranet.core.enumeration.GenderEnum;
import org.orcateam.tranet.core.model.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "USER")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User extends BaseEntity {


    private Role role;

    private String name;
    private String surname;
    private String email;
    private String password;
    private GenderEnum gender;

    private String description;


    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLE_ID", nullable = false)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @NotBlank
    @Length(max = 32)
    @Column(name = "NAME", length = 32, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotBlank
    @Length(max = 32)
    @Column(name = "SURNAME", length = 32, nullable = false)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Email
    @NotBlank
    @Length(max = 128)
    @Column(name = "EMAIL", length = 128, nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotBlank
    @Length(max = 128)
    @Column(name = "PASSWORD", length = 128, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @NotNull
    //@Length(max = 16) - causes UnexpectedTypeException
    @Enumerated(EnumType.STRING)
    @Column(name = "GENDER", nullable = false, length = 16)
    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    @Length(max = 512)
    @Column(name = "DESCRIPTION", length = 512, nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public boolean isSystemUser() {
        return (this instanceof SystemUser);
    }

    @Transient
    public boolean isAgencyUser() {
        return (this instanceof AgencyUser);
    }

    @Transient
    public boolean isProviderUser() {
        return (this instanceof ProviderUser);
    }

    @Transient
    public boolean isCustomerUser() {
        return (this instanceof CustomerUser);
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname();
    }
}
