package org.orcateam.tranet.core.dao.impl;

import org.orcateam.tranet.core.dao.BalanceDao;
import org.orcateam.tranet.core.model.balance.Balance;
import org.springframework.stereotype.Repository;

@Repository
public class BalanceDaoImpl extends GenericDaoImpl<Balance, Integer> implements BalanceDao {
}
