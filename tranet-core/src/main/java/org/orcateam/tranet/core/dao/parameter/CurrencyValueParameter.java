package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.annotation.Joins;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;

@ClazzInfo(clazz = CurrencyValue.class, alias = "currencyValue")
@Joins({
        @Join(collection = "from", alias = "from", joinType = JoinType.LEFT_OUTER_JOIN),
        @Join(collection = "to", alias = "to", joinType = JoinType.LEFT_OUTER_JOIN)
})
public class CurrencyValueParameter {

    @FieldInfo(fieldName = "from")
    private CurrencyType from;

    @FieldInfo(fieldName = "to")
    private CurrencyType to;

    public CurrencyType getFrom() {
        return from;
    }

    public void setFrom(CurrencyType from) {
        this.from = from;
    }

    public CurrencyType getTo() {
        return to;
    }

    public void setTo(CurrencyType to) {
        this.to = to;
    }
}
