package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.CurrencyValueDao;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CurrencyValueDaoImpl extends GenericDaoImpl<CurrencyValue, Integer> implements CurrencyValueDao {

    @Override
    public List<CurrencyValue> getAllWithType() {
        Query query = getSession().createQuery("select cval from CurrencyValue  cval " +
                "left join fetch cval.from as cfrom " +
                "left join fetch cval.to as cfto  ");
        List<CurrencyValue> result = query.list();
        return result;
    }

    @Override
    public CurrencyValue getWithTypes(Integer valueId) {
        Query query = getSession().createQuery("select cval from CurrencyValue cval " +
                "left join fetch cval.from as ctfrom " +
                "left join fetch cval.to as ctto " +
                "where cval.id = :valueId ");
        query.setParameter("valueId", valueId);
        CurrencyValue result = (CurrencyValue) query.uniqueResult();
        return result;
    }
}
