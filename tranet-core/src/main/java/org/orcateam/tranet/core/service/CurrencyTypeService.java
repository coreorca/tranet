package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.currency.CurrencyType;

public interface CurrencyTypeService extends GenericService<CurrencyType, Integer> {


    public CurrencyType getTL();

    public CurrencyType getCurrencyTypeByCode(String code);

}
