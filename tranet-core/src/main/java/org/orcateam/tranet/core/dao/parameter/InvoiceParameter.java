package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.invoice.Invoice;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;

import java.util.Date;

@ClazzInfo(clazz = Invoice.class, alias = "invoice")
public class InvoiceParameter {

    @FieldInfo(compareMethod = CompareMethod.LIKE)
    private String no;

    @FieldInfo(compareMethod = CompareMethod.GT, fieldName = "date")
    private Date greaterThan;

    @FieldInfo(compareMethod = CompareMethod.LT, fieldName = "date")
    private Date lessThan;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Date getGreaterThan() {
        return greaterThan;
    }

    public void setGreaterThan(Date greaterThan) {
        this.greaterThan = greaterThan;
    }

    public Date getLessThan() {
        return lessThan;
    }

    public void setLessThan(Date lessThan) {
        this.lessThan = lessThan;
    }
}
