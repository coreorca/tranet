package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CityServiceImpl extends GenericServiceImpl<City, Integer> implements CityService {

    @Autowired
    CityDao entityDao;

    @Override
    protected GenericDao<City, Integer> getDao() {
        return entityDao;
    }

    @Override
    public City getWithCountry(Integer cityId) {
        return entityDao.getWithCountry(cityId);
    }

    @Override
    public List<City> getListByCountryId(Integer countryId) {
        return entityDao.getListByCountryId(countryId);
    }
}
