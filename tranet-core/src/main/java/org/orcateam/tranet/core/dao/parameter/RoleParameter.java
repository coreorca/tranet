package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = Role.class, alias = "role")
@Join(collection = "privilegeSet", alias = "privilege", joinType = JoinType.LEFT_OUTER_JOIN)
public class RoleParameter {

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String name = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String description = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE, fieldName = "name", alias = "privilege")
    private String privilegeName = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }
}
