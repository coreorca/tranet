package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.location.City;

import java.util.List;

public interface CityDao extends GenericDao<City, Integer> {

    public City getWithCountry(Integer cityId);

    public List<City> getListByCountryId(Integer countryId);

}
