package org.orcateam.tranet.core.dao.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.agency.DiscountGroup;
import org.springframework.stereotype.Repository;


@Repository
public class DiscountGroupDaoImpl extends GenericDaoImpl<DiscountGroup, Integer> implements DiscountGroupDao {

}
