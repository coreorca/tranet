package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.agency.Agency;
import org.springframework.stereotype.Repository;


@Repository
public class AgencyDaoImpl extends GenericDaoImpl<Agency, Integer> implements AgencyDao {
    @Override
    public Agency findEagerlyById(Integer id) {
        Criteria criteria = getSession().createCriteria(Agency.class, "agency");
        criteria.createAlias("agency.discountGroup", "discountGroup", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("agency.balanceSet", "balance", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("agency.id", id));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return (Agency) criteria.uniqueResult();
    }
}
