package org.orcateam.tranet.core.pagination.enumaration;

public enum CompareMethod {
    EQ, GT, LT, IN, LIKE
}
