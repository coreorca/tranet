package org.orcateam.tranet.core.enumeration;

public enum CarShapeEnum {
    SEDAN(1, "global.enum.carShapeEnum.sedan"),
    HATCHBACK(2, "global.enum.carShapeEnum.hatchback"),
    STATION_WAGON(3, "global.enum.carShapeEnum.stationWagon"),
    VAN(4, "global.enum.carShapeEnum.van"),
    MIDI_BUS(4, "global.enum.carShapeEnum.midiBus"),
    BUS(5, "global.enum.carShapeEnum.bus"),
    LIMO(6, "global.enum.carShapeEnum.limo");

    private Integer index;
    private String message;

    CarShapeEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }
}
