package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.KeywordDao;
import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class KeywordServiceImpl extends GenericServiceImpl<Keyword, Integer> implements KeywordService {

    @Autowired
    KeywordDao entityDao;

    @Override
    protected GenericDao<Keyword, Integer> getDao() {
        return entityDao;
    }

    @Override
    public Keyword getByHash(String hash) {
        return entityDao.getByHash(hash);
    }

    @Override
    public Keyword getOrSave(String text) {
        return entityDao.getOrSave(text);
    }

    @Override
    @Cacheable("translate")
    public Keyword get(Integer instanceId) {
        return super.get(instanceId);
    }

    @Override
    public void update(List<Keyword> keywordList) {
        for (Keyword keyword : keywordList) {
            update(keyword);
        }
    }
}
