package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = Privilege.class, alias = "privilege")
public class PrivilegeParameter {

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String name = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.ANYWHERE)
    private String description = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
