package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.media.Media;

public interface MediaService extends GenericService<Media, Integer> {

}
