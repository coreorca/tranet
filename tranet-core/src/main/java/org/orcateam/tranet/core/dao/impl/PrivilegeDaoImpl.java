package org.orcateam.tranet.core.dao.impl;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.orcateam.tranet.core.dao.PrivilegeDao;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrivilegeDaoImpl extends GenericDaoImpl<Privilege, Integer> implements PrivilegeDao {

    @Override
    public List<Privilege> findAll() {
        Criteria criteria = getSession().createCriteria(Privilege.class, "privilege");
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public List<Privilege> findForRoleFilter(String name, String excludeName) {
        Criteria criteria = getSession().createCriteria(Privilege.class, "privilege");
        if (!StringUtils.isBlank(name)) {
            criteria.add(Restrictions.ilike("privilege.name", name, MatchMode.ANYWHERE));
        }
        if (!StringUtils.isBlank(excludeName)) {
            criteria.add(Restrictions.not(Restrictions.ilike("privilege.name", excludeName, MatchMode.ANYWHERE)));
        }
        criteria.addOrder(Order.asc("name"));
        return criteria.list();
    }

}
