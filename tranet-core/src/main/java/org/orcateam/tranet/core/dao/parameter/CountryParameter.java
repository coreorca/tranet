package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

@ClazzInfo(clazz = Country.class, alias = "country")
public class CountryParameter {

    @FieldInfo(compareMethod = CompareMethod.EQ, fieldName = "code.value")
    private String code = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String name = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
