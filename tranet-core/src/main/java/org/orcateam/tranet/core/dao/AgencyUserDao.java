package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.auth.AgencyUser;

import java.util.List;

public interface AgencyUserDao extends GenericDao<AgencyUser, Integer> {

    public List<AgencyUser> getAllByAgencyId(Integer agencyId);

    public AgencyUser findEagerlyById(Integer agencyUserId);

}
