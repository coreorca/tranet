package org.orcateam.tranet.core.model.invoice;

import org.orcateam.tranet.core.model.agency.Agency;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "AGENCY_INVOICE")
public class AgencyInvoice extends Invoice {

    private Agency agency;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AGENCY_ID", nullable = false)
    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }
}
