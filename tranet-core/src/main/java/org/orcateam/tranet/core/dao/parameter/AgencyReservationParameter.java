package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.enumeration.ReservationStatusEnum;
import org.orcateam.tranet.core.enumeration.RouteTypeEnum;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.reservation.Reservation;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

import java.util.Date;

@ClazzInfo(clazz = Reservation.class, alias = "reservation")
public class AgencyReservationParameter {

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String code;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private RouteTypeEnum routeType;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private Point to;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private Point from;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private Date date;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private Integer flightNo;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private Integer pax;

    @FieldInfo(compareMethod = CompareMethod.EQ)
    private ReservationStatusEnum reservationStatus;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RouteTypeEnum getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteTypeEnum routeType) {
        this.routeType = routeType;
    }

    public Point getTo() {
        return to;
    }

    public void setTo(Point to) {
        this.to = to;
    }

    public Point getFrom() {
        return from;
    }

    public void setFrom(Point from) {
        this.from = from;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(Integer flightNo) {
        this.flightNo = flightNo;
    }

    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    public ReservationStatusEnum getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatusEnum reservationStatus) {
        this.reservationStatus = reservationStatus;
    }
}
