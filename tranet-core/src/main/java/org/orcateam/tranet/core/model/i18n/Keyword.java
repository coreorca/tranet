package org.orcateam.tranet.core.model.i18n;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "KEYWORD")
public class Keyword extends BaseEntity {

    private String text;
    private String hash;

    private Map<String, Translate> translateMap = new HashMap<String, Translate>(0);


    @NotEmpty
    @Length(max = 2048)
    @Column(name = "TEXT", length = 2048, nullable = false)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @NotEmpty
    @Length(min = 32, max = 32)
    @Column(name = "TEXT_HASH", length = 32, nullable = false)
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @MapKey(name = "langCode")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "keyword")
    public Map<String, Translate> getTranslateMap() {
        return translateMap;
    }

    public void setTranslateMap(Map<String, Translate> translateMap) {
        this.translateMap = translateMap;
    }
}
