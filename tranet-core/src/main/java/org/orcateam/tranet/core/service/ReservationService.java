package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.reservation.Reservation;

public interface ReservationService extends GenericService<Reservation, Integer> {

    public Reservation findWithId(Integer id);

}
