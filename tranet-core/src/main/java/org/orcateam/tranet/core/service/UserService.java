package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.model.auth.CustomerUser;
import org.orcateam.tranet.core.model.auth.User;

import java.util.List;

public interface UserService extends GenericService<User, Integer> {

    public User findEagerlyById(Integer id);

    public User login(String email, String password);

    public User saveNewUser(User user, String password);

    public void updateWithNewPassword(User user, String password);

    public List<CustomerUser> getAllCustomerUsers();

}
