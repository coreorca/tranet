package org.orcateam.tranet.core.config;

import org.orcateam.tranet.core.util.SpringContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import({DataSourceConfig.class, CacheConfig.class})
@ComponentScan({"org.orcateam.tranet.core"})
public class CoreContextConfig {

    @Autowired
    DataSourceConfig dataSourceConfig;

    @Bean
    public SpringContext springContext() {
        return new SpringContext();
    }
}
