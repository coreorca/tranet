package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.orcateam.tranet.core.dao.UserDao;
import org.orcateam.tranet.core.enumeration.EntityStatusEnum;
import org.orcateam.tranet.core.model.auth.CustomerUser;
import org.orcateam.tranet.core.model.auth.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements UserDao {
    @Override
    public User findEagerlyById(Integer id) {
        Criteria criteria = getSession().createCriteria(User.class, "user");
        criteria.createAlias("role", "role");
        criteria.add(Restrictions.eq("user.id", id));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return (User) criteria.uniqueResult();
    }

    @Override
    public User login(String email, String password) {
        Query query = getSession().createQuery("select user from User user " +
                "left join fetch user.role as role " +
                "left join fetch role.privilegeSet as privilege " +
                "where user.email = :email and user.password = :password and user.status = :active");
        query.setParameter("email", email);
        query.setParameter("password", password);
        query.setParameter("active", EntityStatusEnum.ACTIVE);
        User user = (User) query.uniqueResult();
        return user;
    }

    @Override
    public List<CustomerUser> getAllCustomerUsers() {
        Criteria criteria = getSession().createCriteria(CustomerUser.class, "user");
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }
}
