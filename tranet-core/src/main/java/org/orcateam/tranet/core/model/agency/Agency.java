package org.orcateam.tranet.core.model.agency;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.enumeration.PaymentTypeEnum;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.balance.Balance;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.embeddable.Code;
import org.orcateam.tranet.core.model.embeddable.Money;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "AGENCY")
public class Agency extends BaseEntity {

    private Code code = new Code();
    private String name;
    private String description;
    private PaymentTypeEnum paymentType;
    private Money money = new Money();
    private Set<Balance> balanceSet;
    private DiscountGroup discountGroup;

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotEmpty
    @Length(max = 128)
    @Column(name = "NAME", nullable = false, length = 128)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Length(max = 1024)
    @Column(name = "DESCRIPTION", nullable = true, length = 1024)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Enumerated(EnumType.STRING)
    public PaymentTypeEnum getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    @Embedded
    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "agency", orphanRemoval = true)
    public Set<Balance> getBalanceSet() {
        return balanceSet;
    }

    public void setBalanceSet(Set<Balance> balanceSet) {
        this.balanceSet = balanceSet;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DISCOUNT_GROUP_ID")
    public DiscountGroup getDiscountGroup() {
        return discountGroup;
    }

    public void setDiscountGroup(DiscountGroup discountGroup) {
        this.discountGroup = discountGroup;
    }
}
