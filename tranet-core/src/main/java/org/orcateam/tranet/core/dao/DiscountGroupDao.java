package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.agency.DiscountGroup;

public interface DiscountGroupDao extends GenericDao<DiscountGroup, Integer> {

}
