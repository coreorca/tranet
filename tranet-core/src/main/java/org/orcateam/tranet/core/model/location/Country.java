package org.orcateam.tranet.core.model.location;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;
import org.orcateam.tranet.core.model.i18n.KeywordFK;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "COUNTRY")
public class Country extends BaseEntity {

    private Code code = new Code();
    private KeywordFK name = new KeywordFK();

    private Set<City> citySet = new HashSet<City>(0);

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @Embedded
    public KeywordFK getName() {
        return name;
    }

    public void setName(KeywordFK name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "country", orphanRemoval = true)
    public Set<City> getCitySet() {
        return citySet;
    }

    public void setCitySet(Set<City> citySet) {
        this.citySet = citySet;
    }
}
