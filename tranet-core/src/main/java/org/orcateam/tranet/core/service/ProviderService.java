package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.provider.Provider;

public interface ProviderService extends GenericService<Provider, Integer> {
    public Provider findEagerlyById(Integer id);
}
