package org.orcateam.tranet.core.model.auth;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.orcateam.tranet.core.model.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ROLE")
public class Role extends BaseEntity {

    private String name;
    private String description;

    private Set<Privilege> privilegeSet = new HashSet<Privilege>(0);

    @NotBlank
    @Length(max = 45)
    @Column(name = "NAME", nullable = false, length =  45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotBlank
    @Length(max = 128)
    @Column(name = "DESCRIPTION", nullable = false, length = 128)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable (name = "ROLE_PRIVILEGE",
            joinColumns = {@JoinColumn (name = "ROLE_ID", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRIVILEGE_ID", nullable = false)}
    )
    public Set<Privilege> getPrivilegeSet() {
        return privilegeSet;
    }

    public void setPrivilegeSet(Set<Privilege> privilegeSet) {
        this.privilegeSet = privilegeSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;

        Role role = (Role) o;

        if (!getId().equals(role.getId())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
