package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.ReservationDao;
import org.orcateam.tranet.core.model.reservation.Reservation;
import org.springframework.stereotype.Repository;


@Repository
public class ReservationDaoImpl extends GenericDaoImpl<Reservation, Integer> implements ReservationDao {

    @Override
    public Reservation findWithId(Integer id) {
        Query query = getSession().createQuery("select reservation from Reservation reservation " +
                "where reservation.id = :reservationId");
        query.setParameter("reservationId", id);
        Reservation result = (Reservation) query.uniqueResult();
        return result;
    }
}
