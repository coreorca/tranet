package org.orcateam.tranet.core.model.provider;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;
import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.model.location.Country;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PROVIDER")
public class Provider extends BaseEntity {

    private Code code = new Code();
    private String name;

    private String email;
    private String phone;
    private String fax;
    private String address;
    private Country country;
    private City city;

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotEmpty
    @Length(max = 128)
    @Column(name = "NAME", nullable = false, length = 128)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Email
    @NotBlank
    @Length(max = 128)
    @Column(name = "EMAIL", length = 128, nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotEmpty
    @Length(max = 15)
    @Column(name = "PHONE", length = 15, nullable = false)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Length(max = 15)
    @Column(name = "FAX", length = 15, nullable = true)
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @NotEmpty
    @Length(max = 1024)
    @Column(name = "ADDRESS", length = 1024, nullable = false)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_ID", nullable = false)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CITY_ID", nullable = false)
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return name;
    }
}
