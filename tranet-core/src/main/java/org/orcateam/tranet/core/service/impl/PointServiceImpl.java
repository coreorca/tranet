package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.PointDao;
import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PointServiceImpl extends GenericServiceImpl<Point, Integer> implements PointService {

    @Autowired
    PointDao entityDao;

    @Override
    protected GenericDao<Point, Integer> getDao() {
        return entityDao;
    }

    @Override
    public Point getWithCity(Integer regionId) {
        return entityDao.getWithCity(regionId);
    }

    @Override
    public List<Point> getByCityAndType(Integer cityId, PointTypeEnum typeEnum) {
        return entityDao.getByCityAndType(cityId, typeEnum);
    }
}
