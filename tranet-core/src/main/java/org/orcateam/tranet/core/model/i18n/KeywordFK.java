package org.orcateam.tranet.core.model.i18n;

import org.apache.commons.lang3.StringUtils;
import org.orcateam.tranet.core.service.KeywordService;
import org.orcateam.tranet.core.util.SessionUtil;
import org.orcateam.tranet.core.util.SpringContext;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class KeywordFK implements Serializable {

    private Integer id;
    private Keyword keyword = null;

    public KeywordFK() {
    }

    public KeywordFK(Integer id) {
        this.id = id;
        this.keyword = null;
    }

    @NotNull
    @Column(name = "KEYWORD_ID", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Transient
    public String getText() {
        Keyword kw = getKeyword();
        if (kw != null) {
            return kw.getText();
        }
        return null;
    }

    @Transient
    public String getTranslated() {
        SessionUtil sessionUtil = SpringContext.getContext().getBean(SessionUtil.class);
        String translated = getTranslated(sessionUtil.getLanguage().getCode().getValue());
        return translated;
    }

    @Transient
    private String getTranslated(String language) {
        String text = null;
        Translate translate = getKeyword().getTranslateMap().get(language);
        if (translate != null) {
            text = translate.getText();
        }
        if (StringUtils.isBlank(text)) {
            text = keyword.getText();
        }
        return text;
    }

    @Transient
    private Keyword getKeyword() {
        if (keyword == null) {
            loadKeyword();
        }
        return keyword;
    }

    @Transient
    private synchronized void loadKeyword() {
        if (keyword == null && id != null) {
            KeywordService service = SpringContext.getContext().getBean(KeywordService.class);
            keyword = service.get(id);
        }
    }

    @Override
    public String toString() {
        return getTranslated();
    }
}
