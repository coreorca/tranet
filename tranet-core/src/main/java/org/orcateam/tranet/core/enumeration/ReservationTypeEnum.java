package org.orcateam.tranet.core.enumeration;

public enum ReservationTypeEnum {
    INDIVIDUAL(1, "global.enum.reservationTypeEnum.individual"),
    AGENCY(2, "global.enum.reservationTypeEnum.agency");

    private Integer index;
    private String message;

    ReservationTypeEnum(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }

}

    