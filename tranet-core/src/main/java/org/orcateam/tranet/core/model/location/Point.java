package org.orcateam.tranet.core.model.location;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "DESTINATION_POINT")
public class Point extends BaseEntity {

    private Code code = new Code();
    private String name;
    private City city;
    private PointTypeEnum type;

    private String latLng;

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotEmpty
    @Length(max = 128)
    @Column(name = "NAME", nullable = false, length = 128)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CITY_ID", nullable = false)
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "POINT_TYPE", length = 16, nullable = false)
    public PointTypeEnum getType() {
        return type;
    }

    public void setType(PointTypeEnum type) {
        this.type = type;
    }

    @Column(name = "GEO_LAT_LNG")
    public String getLatLng() {
        return latLng;
    }

    public void setLatLng(String latLng) {
        this.latLng = latLng;
    }
}
