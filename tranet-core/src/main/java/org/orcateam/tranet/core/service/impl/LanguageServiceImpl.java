package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LanguageServiceImpl extends GenericServiceImpl<Language, Integer> implements LanguageService {

    @Autowired
    LanguageDao entityDao;

    @Override
    protected GenericDao<Language, Integer> getDao() {
        return entityDao;
    }

    @Override
    public Language getByCode(String code) {
        return entityDao.getByCode(code);
    }
}
