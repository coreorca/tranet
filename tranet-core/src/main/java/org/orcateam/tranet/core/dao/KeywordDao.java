package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.i18n.Keyword;

public interface KeywordDao extends GenericDao<Keyword, Integer> {

    public Keyword getOrSave(String text);

    public Keyword getByHash(String hash);

}
