package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.agency.Agency;

public interface AgencyService extends GenericService<Agency, Integer> {

    public Agency findEagerlyById(Integer id);

}
