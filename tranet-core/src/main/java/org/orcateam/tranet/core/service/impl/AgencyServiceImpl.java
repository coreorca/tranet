package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.agency.Agency;
import org.orcateam.tranet.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AgencyServiceImpl extends GenericServiceImpl<Agency, Integer> implements AgencyService {

    @Autowired
    AgencyDao entityDao;

    @Override
    protected GenericDao<Agency, Integer> getDao() {
        return entityDao;
    }

    @Override
    public Agency findEagerlyById(Integer id) {
        return entityDao.findEagerlyById(id);
    }
}
