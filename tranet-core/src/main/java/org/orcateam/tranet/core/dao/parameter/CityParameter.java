package org.orcateam.tranet.core.dao.parameter;

import org.orcateam.tranet.core.model.location.City;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;
import org.orcateam.tranet.core.pagination.enumaration.MatchMode;

import java.io.Serializable;

@ClazzInfo(clazz = City.class, alias = "city")
@Join(collection = "country", alias = "country", joinType = JoinType.LEFT_OUTER_JOIN)
public class CityParameter implements Serializable {

    @FieldInfo(compareMethod = CompareMethod.EQ, fieldName = "code.value")
    private String code = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START)
    private String name = null;

    @FieldInfo(compareMethod = CompareMethod.LIKE, matchMode = MatchMode.START, fieldName = "name", alias = "country")
    private String countryName;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
