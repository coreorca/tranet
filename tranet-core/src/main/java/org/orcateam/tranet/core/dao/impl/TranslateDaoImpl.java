package org.orcateam.tranet.core.dao.impl;

import org.orcateam.tranet.core.dao.TranslateDao;
import org.orcateam.tranet.core.model.i18n.Translate;
import org.springframework.stereotype.Repository;


@Repository
public class TranslateDaoImpl extends GenericDaoImpl<Translate, Integer> implements TranslateDao {

}
