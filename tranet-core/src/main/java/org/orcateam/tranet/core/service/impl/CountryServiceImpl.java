package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.CountryDao;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.model.i18n.KeywordFK;
import org.orcateam.tranet.core.model.location.Country;
import org.orcateam.tranet.core.service.CountryService;
import org.orcateam.tranet.core.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("countryService")
public class CountryServiceImpl extends GenericServiceImpl<Country, Integer> implements CountryService {

    @Autowired
    CountryDao countryDao;

    @Autowired
    KeywordService keywordService;

    @Override
    protected GenericDao<Country, Integer> getDao() {
        return countryDao;
    }

    @Override
    public Country saveWithKeyword(Country country, String keywordText) {
        Keyword keyword = keywordService.getOrSave(keywordText);
        country.setName(new KeywordFK(keyword.getId()));
        return save(country);
    }

    @Override
    public void updateWithKeyword(Country country, String keywordText) {
        Keyword keyword = keywordService.getOrSave(keywordText);
        country.setName(new KeywordFK(keyword.getId()));
        update(country);
    }
}
