package org.orcateam.tranet.core.model.embeddable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Code implements Serializable {

    String value;

    public Code() {

    }

    public Code(String value) {
        this.value = value;
    }

    @NotEmpty
    @Length(max = 64)
    @Column(name = "CODE", nullable = false, length = 64, unique = true)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
