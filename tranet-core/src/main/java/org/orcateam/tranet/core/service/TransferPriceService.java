package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.model.transfer.TransferPrice;

import java.util.List;

public interface TransferPriceService extends GenericService<TransferPrice, Integer> {

    public List<TransferPrice> findEagerlyByTransferId(Integer transferId);

}
