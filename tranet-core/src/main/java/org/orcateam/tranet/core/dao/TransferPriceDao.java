package org.orcateam.tranet.core.dao;

import org.orcateam.tranet.core.model.transfer.TransferPrice;

import java.util.List;

public interface TransferPriceDao extends GenericDao<TransferPrice, Integer> {

    public List<TransferPrice> findEagerlyByTransferId(Integer transferId);

}
