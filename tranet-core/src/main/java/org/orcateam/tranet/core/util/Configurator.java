package org.orcateam.tranet.core.util;

import org.apache.log4j.Logger;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.i18n.Language;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.core.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class Configurator {

    private Language defaultLanguage = null;
    private CurrencyType defaultCurrencyType = null;

    private List<Language> languageList = null;
    private List<CurrencyType> currencyTypeList = null;

    static Logger logger = Logger.getLogger(Configurator.class);

    @Autowired
    LanguageService languageService;

    @Autowired
    CurrencyTypeService currencyTypeService;

    @PostConstruct
    public void init() {
        reload();
    }

    public void reload() {
        try {
            initLanguageList();
            initCurrencyTypeList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void initLanguageList() {
        languageList = languageService.getAll();
        for (Language language : languageList) {
            if (language.getCode().getValue().equalsIgnoreCase("EN")) {
                defaultLanguage = language;
                break;
            }
        }
        if (defaultLanguage == null) {
            defaultLanguage = languageList.get(0);
        }
    }

    private void initCurrencyTypeList() {
        currencyTypeList = currencyTypeService.getAll();
        for (CurrencyType currencyType : currencyTypeList) {
            if (currencyType.getCode().getValue().equalsIgnoreCase("USD")) {
                defaultCurrencyType = currencyType;
                break;
            }
        }
        if (defaultCurrencyType == null) {
            defaultCurrencyType = currencyTypeList.get(0);
        }
    }

    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Language defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public CurrencyType getDefaultCurrencyType() {
        return defaultCurrencyType;
    }

    public void setDefaultCurrencyType(CurrencyType defaultCurrencyType) {
        this.defaultCurrencyType = defaultCurrencyType;
    }

    public List<Language> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(List<Language> languageList) {
        this.languageList = languageList;
    }

    public List<CurrencyType> getCurrencyTypeList() {
        return currencyTypeList;
    }

    public void setCurrencyTypeList(List<CurrencyType> currencyTypeList) {
        this.currencyTypeList = currencyTypeList;
    }
}
