package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.invoice.Invoice;

public interface InvoiceDao extends GenericDao<Invoice, Integer> {

    public Invoice findEagerlyById(Integer id);

}
