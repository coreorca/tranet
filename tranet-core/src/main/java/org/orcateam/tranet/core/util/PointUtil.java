package org.orcateam.tranet.core.util;

import org.apache.commons.lang3.StringUtils;

public class PointUtil {

    public static String parseLatLng(String latLng) {
        if (StringUtils.isNotBlank(latLng)) {
            latLng = StringUtils.replace(latLng, "(", "");
            latLng = StringUtils.replace(latLng, ")", "");
            latLng = StringUtils.replace(latLng, " ", "");
            return latLng;
        }
        return null;
    }

}
