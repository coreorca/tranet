package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.car.CarType;

public interface CarTypeDao extends GenericDao<CarType, Integer> {

    public CarType findEagerlyById(Integer Id);
}
