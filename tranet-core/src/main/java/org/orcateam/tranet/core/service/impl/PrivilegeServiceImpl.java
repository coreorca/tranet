package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.dao.PrivilegeDao;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("privilegeService")
public class PrivilegeServiceImpl extends GenericServiceImpl<Privilege, Integer> implements PrivilegeService {

    @Autowired
    PrivilegeDao privilegeDao;

    @Override
    protected GenericDao<Privilege, Integer> getDao() {
        return privilegeDao;
    }

    @Override
    public List<Privilege> findAll() {
        return privilegeDao.findAll();
    }

    @Override
    public List<Privilege> findForRoleFilter(String name, String excludeName) {
        return privilegeDao.findForRoleFilter(name, excludeName);
    }
}
