package org.orcateam.tranet.core.dao.impl;

import org.orcateam.tranet.core.dao.CountryDao;
import org.orcateam.tranet.core.model.location.Country;
import org.springframework.stereotype.Repository;

@Repository
public class CountryDaoImpl extends GenericDaoImpl<Country, Integer> implements CountryDao {


}
