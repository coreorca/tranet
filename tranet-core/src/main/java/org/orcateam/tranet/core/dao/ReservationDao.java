package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.reservation.Reservation;

public interface ReservationDao extends GenericDao<Reservation, Integer> {

    public Reservation findWithId(Integer id);

}
