package org.orcateam.tranet.core.dao.impl;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.orcateam.tranet.core.dao.RoleDao;
import org.orcateam.tranet.core.model.auth.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, Integer> implements RoleDao {
    @Override
    public Role findEagerlyById(Integer id) {
        Criteria criteria = getSession().createCriteria(Role.class, "role");
        criteria.createAlias("privilegeSet", "privilege", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("role.id", id));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return (Role) criteria.uniqueResult();
    }

    @Override
    public List<Role> findAll() {
        Criteria criteria = getSession().createCriteria(Role.class, "role");
        return criteria.list();
    }

    @Override
    public Role findForRoleFilter(Integer id, String name, String excludeName) {
        Criteria criteria = getSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("id", id));
        criteria.createAlias("privilegeSet", "privilege", JoinType.LEFT_OUTER_JOIN);
        if (!StringUtils.isBlank(name)) {
            criteria.add(Restrictions.ilike("privilege.name", name, MatchMode.ANYWHERE));
        }
        if (!StringUtils.isBlank(excludeName)) {
            criteria.add(Restrictions.not(Restrictions.ilike("privilege.name", excludeName, MatchMode.ANYWHERE)));
        }
        criteria.addOrder(Order.asc("privilege.name"));
        return (Role) criteria.uniqueResult();
    }
}

