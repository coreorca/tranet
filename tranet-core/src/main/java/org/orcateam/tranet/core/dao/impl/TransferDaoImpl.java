package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.enumeration.PointTypeEnum;
import org.orcateam.tranet.core.model.location.Point;
import org.orcateam.tranet.core.model.transfer.Transfer;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class TransferDaoImpl extends GenericDaoImpl<Transfer, Integer> implements TransferDao {

    @Override
    public Transfer getWithPoints(Integer transferId) {
        Query query = getSession().createQuery("select tf from Transfer tf " +
                "left join fetch tf.airport as airport " +
                "left join fetch tf.region as region " +
                "left join fetch tf.country as country " +
                "left join fetch tf.city as city " +
                "where tf.id = :transferId");
        query.setParameter("transferId", transferId);
        Transfer result = (Transfer) query.uniqueResult();
        return result;
    }

    @Override
    public List<Transfer> getAvailableTransferListByAirport(Point airport) {
        Query query = getSession().createQuery("select distinct transfer from Transfer transfer " +
                "left join fetch transfer.airport as airport  " +
                "left join fetch transfer.region as region " +
                "inner join fetch transfer.priceSet as price " +
                "left join fetch price.carType as carType " +
                "where region.type = :regionType and airport.id = :airportId");
        query.setParameter("regionType", PointTypeEnum.REGION);
        query.setParameter("airportId", airport.getId());
        List<Transfer> result = query.list();
        return result;
    }
}
