package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.invoice.Invoice;

public interface InvoiceService extends GenericService<Invoice, Integer> {

    public Invoice findEagerlyById(Integer id);

}
