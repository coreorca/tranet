package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.media.Media;

public interface MediaDao extends GenericDao<Media, Integer> {

}
