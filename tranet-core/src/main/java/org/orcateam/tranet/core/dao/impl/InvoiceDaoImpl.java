package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.invoice.Invoice;
import org.springframework.stereotype.Repository;


@Repository
public class InvoiceDaoImpl extends GenericDaoImpl<Invoice, Integer> implements InvoiceDao {

    @Override
    public Invoice findEagerlyById(Integer id) {
        Criteria criteria = getSession().createCriteria(Invoice.class, "invoice");
        criteria.createAlias("invoice.agency", "agency", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("invoice.customerUser", "customerUser", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("invoice.id", id));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return (Invoice) criteria.uniqueResult();
    }

}
