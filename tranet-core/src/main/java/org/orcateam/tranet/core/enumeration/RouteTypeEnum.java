package org.orcateam.tranet.core.enumeration;

public enum  RouteTypeEnum {
    REGION_TO_AIRPORT(0, "global.enum.routeTypeEnum.regionToAirport", PointTypeEnum.REGION, PointTypeEnum.AIRPORT),
    AIRPORT_TO_REGION(1, "global.enum.routeTypeEnum.airportToRegion", PointTypeEnum.AIRPORT, PointTypeEnum.REGION);

    private Integer index;
    private String message;

    private PointTypeEnum from;
    private PointTypeEnum to;

    private RouteTypeEnum(Integer index, String message, PointTypeEnum from, PointTypeEnum to) {
        this.index = index;
        this.message = message;
        this.from = from;
        this.to = to;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }

    public PointTypeEnum getFrom() {
        return from;
    }

    public PointTypeEnum getTo() {
        return to;
    }
}
