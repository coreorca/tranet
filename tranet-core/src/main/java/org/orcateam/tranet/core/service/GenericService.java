package org.orcateam.tranet.core.service;

import org.orcateam.tranet.core.util.Pager;
import org.orcateam.tranet.core.util.Pagination;

import java.io.Serializable;
import java.util.List;

public interface GenericService<E, PK extends Serializable> {

    public Pagination<E> findWithPagination(Pager pager, Object param);

    public E get(PK instanceId);

    public E save(E instance);

    public void update(E instance);

    public void remove(E instance);

    public List<E> getAll();

}
