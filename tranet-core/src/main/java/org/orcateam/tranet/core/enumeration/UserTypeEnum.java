package org.orcateam.tranet.core.enumeration;

import org.orcateam.tranet.core.exception.OrcaException;
import org.orcateam.tranet.core.exception.PropertyNotFoundException;

public enum UserTypeEnum {
    SYSTEM_USER("global.enum.userType.systemUser", 1),
    AGENCY_USER("global.enum.userType.agencyUser", 2),
    PROVIDER_USER("global.enum.userType.providerUser", 3),
    CUSTOMER_USER("global.enum.userType.customerUser", 4);

    private String message;
    private Integer level;

    UserTypeEnum(String message, Integer level) {
        this.message = message;
        this.level = level;
    }

    public static UserTypeEnum resolveFrom(String name) throws OrcaException {
        for (UserTypeEnum typeEnum : UserTypeEnum.values()) {
            if (typeEnum.toString().equals(name)) {
                return typeEnum;
            }
        }
        throw new PropertyNotFoundException();
    }

    public static UserTypeEnum resolveFrom(Integer level) throws OrcaException {
        for (UserTypeEnum typeEnum : UserTypeEnum.values()) {
            if (typeEnum.getLevel().equals(level)) {
                return typeEnum;
            }
        }
        throw new PropertyNotFoundException();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
