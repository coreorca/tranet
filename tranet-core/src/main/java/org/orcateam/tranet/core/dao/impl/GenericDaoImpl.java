package org.orcateam.tranet.core.dao.impl;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.pagination.annotation.ClazzInfo;
import org.orcateam.tranet.core.pagination.annotation.FieldInfo;
import org.orcateam.tranet.core.pagination.annotation.Join;
import org.orcateam.tranet.core.pagination.annotation.Joins;
import org.orcateam.tranet.core.pagination.enumaration.CompareMethod;
import org.orcateam.tranet.core.pagination.enumaration.JoinType;
import org.orcateam.tranet.core.util.OrderBy;
import org.orcateam.tranet.core.util.Pager;
import org.orcateam.tranet.core.util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

public class GenericDaoImpl<E, PK extends Serializable> implements GenericDao<E, PK> {

    private Class<E> type;

    @Autowired
    SessionFactory sessionFactory;


    public GenericDaoImpl() {
        this.type = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Session getSession() {
        Session session = sessionFactory.getCurrentSession();
        return session;
    }

    @Override
    public E get(PK instanceId) {
        return (E) getSession().get(type, instanceId);
    }

    @Override
    public E save(E instance) {
        return (E) getSession().merge(instance);
    }

    @Override
    public void update(E instance) {
        getSession().update(instance);
    }

    @Override
    public void remove(E instance) {
        getSession().delete(instance);
    }

    @Override
    public List<E> getAll() {
        Criteria criteria = getSession().createCriteria(type);
        criteria.addOrder(Order.asc("id"));
        return (List<E>) criteria.list();
    }

    @Override
    public Pagination<E> findWithPagination(Pager pager, Object param) {
        try {
            Class obj = param.getClass();

            String clazzName = null;
            String mainAlias = null;
            if (obj.isAnnotationPresent(ClazzInfo.class)) {
                ClazzInfo annotation = (ClazzInfo) obj.getAnnotation(ClazzInfo.class);
                clazzName = annotation.clazz().getName().substring(annotation.clazz().getName().lastIndexOf(".") + 1);
                mainAlias = annotation.alias();
            }

            List<String> whereClauses = new ArrayList<String>();
            Map<String, Object> values = new HashMap<String, Object>();

            for (Field f : obj.getDeclaredFields()) {
                if (f.isAnnotationPresent(FieldInfo.class)) {
                    FieldInfo annotation = f.getAnnotation(FieldInfo.class);
                    f.setAccessible(true);
                    Class<?> fieldType = f.getType();

                    Object object = f.get(param);

                    boolean isMap = object != null && object instanceof Map;
                    boolean isCollection = object != null && object instanceof Collection;
                    boolean isString = object != null && object instanceof String;

                    int mapSize = 0;
                    int collectionSize = 0;
                    int stringSize = 0;

                    if (object != null) {
                        if (isMap) {
                            mapSize = ((Map) object).size();
                        }
                        if (isCollection) {
                            collectionSize = ((Collection) object).size();
                        }
                        if (isString) {
                            stringSize = ((String) object).length();
                        }
                    }

                    if (object != null
                            && !(isString && stringSize == 0)
                            && !(isMap && mapSize == 0)
                            && !(isCollection && collectionSize == 0)) {

                        String fieldName = f.getName();
                        if (!annotation.fieldName().equals("")) {
                            fieldName = annotation.fieldName();
                        }

                        String alias = mainAlias;
                        if (StringUtils.isNotEmpty(annotation.alias())) {
                            alias = annotation.alias();
                        }

                        if (isMap) {
                            Map<String, String> map = (Map<String, String>) object;
                            if (map.size() > 0) {
                                for (Map.Entry<String, String> attribute : map.entrySet()) {
                                    if (!StringUtils.isEmpty(attribute.getKey())
                                            && !StringUtils.isEmpty(attribute.getValue())) {

                                        String paramNameKey = alias.concat(attribute.getKey()).concat("KEY");
                                        String paramNameVal = alias.concat(attribute.getKey()).concat("VAL");

                                        if (annotation.compareMethod() == CompareMethod.LIKE) {
                                            values.put(paramNameKey, attribute.getKey());
                                            values.put(paramNameVal, annotation.matchMode().toMatchString(attribute.getValue()));

                                            String clause = alias + ".key = :" + paramNameKey + " AND " + toLower(alias + ".value", paramNameVal);

                                            whereClauses.add(clause);
                                        } else {
                                            throw new RuntimeException("Not implemented.");
                                        }
                                    }
                                }
                            }
                        } else {
                            String paramName = alias.concat(fieldName);
                            paramName = StringUtils.replace(paramName, ".", "");

                            if (annotation.compareMethod() == CompareMethod.EQ) {
                                values.put(paramName, object);

                                String clause = alias + "." + fieldName + " = :" + paramName;
                                whereClauses.add(clause);
                            } else if (annotation.compareMethod() == CompareMethod.GT) {
                                values.put(paramName, object);

                                String clause = alias + "." + fieldName + " > :" + paramName;
                                whereClauses.add(clause);
                            } else if (annotation.compareMethod() == CompareMethod.LT) {
                                values.put(paramName, object);

                                String clause = alias + "." + fieldName + " < :" + paramName;
                                whereClauses.add(clause);
                            } else if (annotation.compareMethod() == CompareMethod.IN) {
                                values.put(paramName, object);

                                String clause = alias + "." + fieldName + " IN (:" + paramName + ")";
                                whereClauses.add(clause);
                            } else if (annotation.compareMethod() == CompareMethod.LIKE) {
                                values.put(paramName, annotation.matchMode().toMatchString(object.toString()));

                                String clause = toLower(alias + "." + fieldName, paramName);
                                whereClauses.add(clause);
                            }
                        }
                    }
                }
            }

            StringBuilder selectBuilder = new StringBuilder();

            select(clazzName, selectBuilder, mainAlias);
            join(obj, selectBuilder, mainAlias, true);
            where(whereClauses, selectBuilder);
            order(pager, selectBuilder, mainAlias);

            StringBuilder countBuilder = new StringBuilder();
            count(clazzName, countBuilder, mainAlias);
            join(obj, countBuilder, mainAlias, false);
            where(whereClauses, countBuilder);

            Query query = query(values, selectBuilder);
            Query countQuery = query(values, countBuilder);

            query.setFirstResult(pager.getFirstResult());
            query.setMaxResults(pager.getMaxResult());

            List<E> result = (List<E>) query.list();
            Long count = (Long) countQuery.uniqueResult();

            return new Pagination<E>(result, count.intValue());
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Query query(Map<String, Object> values, StringBuilder queryBuilder) {
        Query query = getSession().createQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : values.entrySet()) {
            if (entry.getValue() instanceof Collection) {
                query.setParameterList(entry.getKey(), (Collection) entry.getValue());
            } else {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    private void order(Pager pager, StringBuilder queryBuilder, String alias) {
        queryBuilder.append(" ORDER BY ");

        OrderBy orderBy = pager.getOrderBy();
        if (!StringUtils.isEmpty(orderBy.getName())) {
            if (orderBy.isAsc()) {
                queryBuilder.append(orderBy.getName()).append(" ASC");
            } else {
                queryBuilder.append(orderBy.getName()).append(" DESC");
            }
        } else {
            queryBuilder.append(alias).append(".id").append(" ASC");
        }
    }

    private void select(String clazzName, StringBuilder queryBuilder, String alias) {
        queryBuilder.append("SELECT distinct ").append(alias).append(" FROM ");

        queryBuilder.append(clazzName).append(" ").append(alias);
    }

    private void count(String clazzName, StringBuilder queryBuilder, String alias) {
        queryBuilder.append("SELECT count(distinct ").append(alias).append(".id) as b FROM ");

        queryBuilder.append(clazzName).append(" ").append(alias).append(" ");
    }

    private void join(Class obj, StringBuilder queryBuilder, String alias, boolean fetch) {
        if (obj.isAnnotationPresent(Join.class)) {
            Join annotation = (Join) obj.getAnnotation(Join.class);
            joinAnnotation(queryBuilder, annotation, alias, fetch);
        }

        if (obj.isAnnotationPresent(Joins.class)) {
            Joins annotation = (Joins) obj.getAnnotation(Joins.class);
            for (Join join : annotation.value()) {
                joinAnnotation(queryBuilder, join, alias, fetch);
            }
        }
    }

    private void where(List<String> whereClauses, StringBuilder queryBuilder) {
        if (whereClauses.size() > 0) {
            int i = 0;
            queryBuilder.append(" WHERE ");
            for (String s : whereClauses) {
                i++;
                queryBuilder.append(s);
                if (whereClauses.size() != i) {
                    queryBuilder.append(" AND ");
                }
            }
        }
    }

    private void joinAnnotation(StringBuilder queryBuilder, Join annotation, String mainAlias, boolean fetch) {
        JoinType joinType = annotation.joinType();
        String collection = annotation.collection();
        String alias = annotation.alias();
        if (joinType.equals(JoinType.LEFT_OUTER_JOIN)) {
            queryBuilder.append(" LEFT JOIN ");
            if (fetch) {
                queryBuilder.append(" FETCH ");
            }
            queryBuilder.append(" ").append(mainAlias).append(".").append(collection).append(" AS ").append(annotation.alias());
        } else {
            throw new RuntimeException("Not implemented yet");
        }
    }

    private String toLower(String field, String param) {
        return "LOWER (" + field + ") like LOWER (:" + param + ")";
    }
}
