package org.orcateam.tranet.core.dao.impl;

import org.hibernate.Query;
import org.orcateam.tranet.core.dao.LanguageDao;
import org.orcateam.tranet.core.model.i18n.Language;
import org.springframework.stereotype.Repository;


@Repository
public class LanguageDaoImpl extends GenericDaoImpl<Language, Integer> implements LanguageDao {

    @Override
    public Language getByCode(String code) {
        Query query = getSession().createQuery("select lang from Language lang where lang.code.value = :code");
        query.setParameter("code", code);
        Language result = (Language) query.uniqueResult();
        return result;
    }
}
