package org.orcateam.tranet.core.dao;


import org.orcateam.tranet.core.model.currency.CurrencyType;

public interface CurrencyTypeDao extends GenericDao<CurrencyType, Integer> {

    public CurrencyType getCurrencyTypeByCode(String code);

}
