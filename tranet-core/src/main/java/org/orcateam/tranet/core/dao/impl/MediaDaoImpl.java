package org.orcateam.tranet.core.dao.impl;

import org.orcateam.tranet.core.dao.*;
import org.orcateam.tranet.core.model.media.Media;
import org.springframework.stereotype.Repository;


@Repository
public class MediaDaoImpl extends GenericDaoImpl<Media, Integer> implements MediaDao {

}
