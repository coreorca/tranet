package org.orcateam.tranet.core.model.car;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.orcateam.tranet.core.enumeration.CarShapeEnum;
import org.orcateam.tranet.core.model.BaseEntity;
import org.orcateam.tranet.core.model.embeddable.Code;
import org.orcateam.tranet.core.model.media.Media;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CAR_TYPE")
public class CarType extends BaseEntity {

    private Code code = new Code();
    private String name;
    private String description;
    private Integer pax;
    private String brand;
    private Integer modelYear;
    private Integer trunkSize;
    private CarShapeEnum shape;

    private Set<Media> mediaSet = new HashSet<Media>();

    @Embedded
    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    @NotEmpty
    @Length(max = 128)
    @Column(name = "NAME", nullable = false, length = 128)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Length(max = 1024)
    @Column(name = "DESCRIPTION", nullable = true, length = 1024)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "PAX", nullable = false)
    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    @Length(max = 64)
    @Column(name = "BRAND", nullable = false, length = 64)
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Column(name = "MODEL_YEAR", nullable = false, length = 4)
    public Integer getModelYear() {
        return modelYear;
    }

    public void setModelYear(Integer age) {
        this.modelYear = age;
    }

    @Column(name = "TRUNK_SIZE")
    public Integer getTrunkSize() {
        return trunkSize;
    }

    public void setTrunkSize(Integer trunkSize) {
        this.trunkSize = trunkSize;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "SHAPE", nullable = false, length = 16)
    public CarShapeEnum getShape() {
        return shape;
    }

    public void setShape(CarShapeEnum shape) {
        this.shape = shape;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "CAR_TYPE_MEDIA",
            joinColumns = {@JoinColumn(name = "CAR_TYPE_ID", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "MEDIA_ID", nullable = false)}
    )
    public Set<Media> getMediaSet() {
        return mediaSet;
    }

    public void setMediaSet(Set<Media> mediaSet) {
        this.mediaSet = mediaSet;
    }

    @Override
    public String toString() {
        return code + ": " + name;
    }
}
