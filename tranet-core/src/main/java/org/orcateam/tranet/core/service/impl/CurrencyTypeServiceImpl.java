package org.orcateam.tranet.core.service.impl;

import org.orcateam.tranet.core.dao.CurrencyTypeDao;
import org.orcateam.tranet.core.dao.GenericDao;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CurrencyTypeServiceImpl extends GenericServiceImpl<CurrencyType, Integer> implements CurrencyTypeService {

    @Autowired
    CurrencyTypeDao entityDao;

    @Override
    protected GenericDao<CurrencyType, Integer> getDao() {
        return entityDao;
    }

    @Override
    public CurrencyType getTL() {
        return getCurrencyTypeByCode("TRY");
    }

    @Override
    public CurrencyType getCurrencyTypeByCode(String code) {
        return entityDao.getCurrencyTypeByCode(code);
    }
}
