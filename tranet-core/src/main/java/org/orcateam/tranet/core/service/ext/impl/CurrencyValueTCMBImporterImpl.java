package org.orcateam.tranet.core.service.ext.impl;

import org.orcateam.tcbmce.engine.TCMBCurrencyEngine;
import org.orcateam.tcbmce.exception.TCMBCurrencyExporterException;
import org.orcateam.tcbmce.modal.Currency;
import org.orcateam.tranet.core.model.currency.CurrencyType;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.service.CurrencyTypeService;
import org.orcateam.tranet.core.service.ext.CurrencyValueTCMBImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CurrencyValueTCMBImporterImpl implements CurrencyValueTCMBImporter {

    @Autowired
    transient CurrencyTypeService currencyTypeService;

    public Map<String, CurrencyValue> getCurrencyValueMap() throws TCMBCurrencyExporterException {
        Map<String, CurrencyValue> resultMap = new HashMap<String, CurrencyValue>();
        List<CurrencyType> currencyTypes = currencyTypeService.getAll();
        TCMBCurrencyEngine engine = new TCMBCurrencyEngine();
        CurrencyType tl = currencyTypeService.getTL();
        Map<String, Currency> currencyMap = engine.getCurrencyValuesFromTCMB();
        for (CurrencyType currencyType : currencyTypes) {
            Currency currency = currencyMap.get(currencyType.getCode().toString());
            if (currency == null || currency.getForexSellPrice().equals(0)) {
                continue;
            }
            CurrencyValue currencyValue = new CurrencyValue(currencyType, tl, currency.getUnit().doubleValue(), currency.getForexSellPrice());
            resultMap.put(currencyType.getCode().toString(), currencyValue);
        }
        return resultMap;
    }
}
