package org.orcateam.tranet.core.model.currency;

import org.orcateam.tranet.core.model.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CURRENCY_VALUE", uniqueConstraints = {
        @UniqueConstraint(name = "CURR_TYPE_UNQ", columnNames = {"FROM_ID", "TO_ID"})
})
public class CurrencyValue extends BaseEntity {

    private CurrencyType from;
    private CurrencyType to;

    private Double fromUnitCoefficient;
    private Double value;

    public CurrencyValue() {
    }

    public CurrencyValue(CurrencyType from, CurrencyType to, Double fromUnitCoefficient, Double value) {
        this.from = from;
        this.to = to;
        this.fromUnitCoefficient = fromUnitCoefficient;
        this.value = value;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FROM_ID", nullable = false)
    public CurrencyType getFrom() {
        return from;
    }

    public void setFrom(CurrencyType from) {
        this.from = from;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TO_ID", nullable = false)
    public CurrencyType getTo() {
        return to;
    }

    public void setTo(CurrencyType to) {
        this.to = to;
    }

    @NotNull
    @Column(name = "FROM_COEFFICIENT", nullable = false, precision = 22, scale = 4, columnDefinition = "DECIMAL(22,4)")
    public Double getFromUnitCoefficient() {
        return fromUnitCoefficient;
    }

    public void setFromUnitCoefficient(Double fromUnitCoefficient) {
        this.fromUnitCoefficient = fromUnitCoefficient;
    }

    @NotNull
    @Column(name = "VALUE", nullable = false, precision = 22, scale = 4, columnDefinition = "DECIMAL(22,4)")
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Transient
    public Double getConvertedValue() {
        return value / fromUnitCoefficient;
    }
}
