package org.orcateam.tranet.core.model.invoice;

import org.orcateam.tranet.core.model.auth.CustomerUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "INDIVIDUAL_INVOICE")
public class IndividualInvoice extends Invoice {

    private CustomerUser customerUser;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_USER_ID", nullable = false)
    public CustomerUser getCustomerUser() {
        return customerUser;
    }

    public void setCustomerUser(CustomerUser customerUser) {
        this.customerUser = customerUser;
    }

}
