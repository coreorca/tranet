package org.orcateam.tranet.core.service;


import org.orcateam.tranet.core.model.i18n.Language;

public interface LanguageService extends GenericService<Language, Integer> {

    public Language getByCode(String code);

}
