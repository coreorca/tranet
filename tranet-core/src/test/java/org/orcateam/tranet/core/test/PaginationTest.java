package org.orcateam.tranet.core.test;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.orcateam.tranet.core.config.CoreContextConfig;
import org.orcateam.tranet.core.dao.parameter.PrivilegeParameter;
import org.orcateam.tranet.core.dao.parameter.RoleParameter;
import org.orcateam.tranet.core.model.auth.Privilege;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.service.PrivilegeService;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.core.util.OrderBy;
import org.orcateam.tranet.core.util.Pager;
import org.orcateam.tranet.core.util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CoreContextConfig.class)
@Transactional
public class PaginationTest {

    @Autowired
    RoleService roleService;

    @Autowired
    PrivilegeService privilegeService;

    @Test
    public void testPrivilegeList() {
        insertPrivilege();

        Pager pager = new Pager(0, 5, new OrderBy(null));
        PrivilegeParameter privilegeParameter = new PrivilegeParameter();
        privilegeParameter.setName("name1");
        Pagination<Privilege> privilegePagination = privilegeService.findWithPagination(pager, privilegeParameter);

        Assert.assertNotNull(privilegePagination);
    }

    @Test
    public void testRoleList() {
        insertPrivilege();
        insertRole();

        Pager pager = new Pager(0, 5, new OrderBy(null));
        RoleParameter roleParameter = new RoleParameter();
        roleParameter.setPrivilegeName("name0");
        Pagination<Role> rolePagination = roleService.findWithPagination(pager, roleParameter);

        Assert.assertNotNull(rolePagination);
    }

    private void insertRole() {
        for (int i = 0; i < 10; i++) {
            Role role = new Role();
            role.setName("role" + i);
            role.setDescription("descc");
            if (i == 5) {
                List<Privilege> privilegeList = privilegeService.findAll();
                role.setPrivilegeSet(Sets.newHashSet(privilegeList));
            }
            roleService.save(role);
        }
    }

    private void insertPrivilege() {
        for (int i = 0; i < 10; i++) {
            Privilege privilege = new Privilege();
            privilege.setName("name" + i);
            privilege.setDescription("desc");
            privilegeService.save(privilege);
        }
    }
}
