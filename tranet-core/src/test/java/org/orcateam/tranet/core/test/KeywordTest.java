package org.orcateam.tranet.core.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.orcateam.tranet.core.config.CoreContextConfig;
import org.orcateam.tranet.core.model.i18n.Keyword;
import org.orcateam.tranet.core.model.i18n.KeywordFK;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CoreContextConfig.class)
public class KeywordTest {

    @Test
    public void test() {
        try {
            KeywordFK keywordFK = new KeywordFK(1);
            System.out.print(keywordFK.getTranslated());
            Thread.sleep(1000);
            KeywordFK keywordFK2 = new KeywordFK(1);
            System.out.print(keywordFK2.getTranslated());
            Thread.sleep(1000);
            KeywordFK keywordFK23 = new KeywordFK(1);
            System.out.print(keywordFK23.getTranslated());
            Thread.sleep(1000);
            KeywordFK keywordFK24 = new KeywordFK(1);
            System.out.print(keywordFK24.getTranslated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
