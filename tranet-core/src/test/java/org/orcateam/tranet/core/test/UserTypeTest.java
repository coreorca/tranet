package org.orcateam.tranet.core.test;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.orcateam.tranet.core.config.CoreContextConfig;
import org.orcateam.tranet.core.enumeration.GenderEnum;
import org.orcateam.tranet.core.model.auth.Role;
import org.orcateam.tranet.core.model.auth.SystemUser;
import org.orcateam.tranet.core.service.RoleService;
import org.orcateam.tranet.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CoreContextConfig.class)
public class UserTypeTest {

    @Autowired
    RoleService roleService;

    @Autowired
    UserService userService;


    @Test
    public void systemUserTest() {
        Role role = new Role();
        role.setName("ORCA");
        role.setDescription("ORCA TEAM");
        Role persisted = roleService.save(role);


        SystemUser user = new SystemUser();
        user.setName("Orca");
        user.setEmail("orca@orcateam.org");
        user.setDescription("orca team");
        user.setRole(persisted);
        user.setGender(GenderEnum.MALE);
        user.setPassword("fds");
        user.setSurname("TEAM");

        userService.save(user);


    }


}
