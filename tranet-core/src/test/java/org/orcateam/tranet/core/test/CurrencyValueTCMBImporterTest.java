package org.orcateam.tranet.core.test;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.orcateam.tcbmce.exception.TCMBCurrencyExporterException;
import org.orcateam.tranet.core.config.CoreContextConfig;
import org.orcateam.tranet.core.model.currency.CurrencyValue;
import org.orcateam.tranet.core.service.ext.CurrencyValueTCMBImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CoreContextConfig.class)
public class CurrencyValueTCMBImporterTest {

    @Autowired
    transient CurrencyValueTCMBImporter currencyValueTCMBImporter;

    @Test
    public void testCurrencyValuesFromTCMB() {
        Map<String, CurrencyValue> currencyValueMap = null;
        try {
            currencyValueMap = currencyValueTCMBImporter.getCurrencyValueMap();
        } catch (TCMBCurrencyExporterException e) {
            Assert.assertFalse(true);
        }
        for (Map.Entry<String, CurrencyValue> entry : currencyValueMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().getConvertedValue());
        }
    }


}
